proc resetInboxTasks {sDate sFormat {sWhere ""}} {
    set sSequenceOffset 0
    set cNewDate [clock scan $sDate]
    if {$sWhere == ""} {
	set sTaskList [mql temp query bus "Inbox Task" * * select id attribute\[Route Node ID\] attribute\[Due Date Offset\] attribute\[Actual Completion Date\] attribute\[TCETEEOrder\] format dump |]
    } else {
	set sTaskList [mql temp query bus "Inbox Task" * * where $sWhere select id attribute\[Route Node ID\] attribute\[Due Date Offset\] attribute\[Actual Completion Date\] attribute\[TCETEEOrder\] format dump |]
    }
    set lTaskList [split $sTaskList "\n"]
    foreach sTask $lTaskList {
	set lBusAttrs [split $sTask "|"]
	set sBusType [string trim [lindex $lBusAttrs 0] ]
	set sBusName [string trim [lindex $lBusAttrs 1] ]
	set sBusRev [string trim [lindex $lBusAttrs 2] ]
	set sBusID [string trim [lindex $lBusAttrs 3] ]
	set sRouteNodeID [string trim [lindex $lBusAttrs 4] ]
	set sDueDateOffset [string trim [lindex $lBusAttrs 5] ]
	set sActualCompletionDate [string trim [lindex $lBusAttrs 6] ]
	set sTCETEEOrder [string trim [lindex $lBusAttrs 7] ]
	    
	set sSequence [mql print connection $sRouteNodeID select attribute\[Route Sequence\] dump |]
	if {$sSequence == 2} {
	    set sSequenceOffset 2
	} else {
	    set sSequenceOffset 0
	}

	if {$sDueDateOffset == ""} {
	    set sDueDateOffset "0d"
	}
	    
    # repair Due Date Offset format
    if {[string is integer -strict $sDueDateOffset]} {
        set sDueDateOffset [sDueDateOffset + "d"]
    }
    
	puts "Reseting \"$sBusType\" $sBusName $sBusRev and RouteNode $sRouteNodeID according to date: $sDate"

	set sStartDate [clock format [expr {$cNewDate + $sSequenceOffset * 86400}] -format $sFormat]
	set sDueDate [clock format [expr {$cNewDate + $sSequenceOffset * 86400 + [string trimright $sDueDateOffset d] * 86400}] -format $sFormat]

	puts " Originated/Modified: $sStartDate"
	puts " Scheduled Completion Date/Actual Completion Date: $sDueDate"

	if {$sActualCompletionDate == ""} {
	    set sMqlRet [mql mod bus $sBusID originated $sStartDate modified $sStartDate "Scheduled Completion Date" $sDueDate "Due Date Offset" $sDueDateOffset]
	    set sMqlRet [mql mod connection $sRouteNodeID "Scheduled Completion Date" $sDueDate "TCETEEOrder" $sTCETEEOrder "TCETEEUserPriority" $sTCETEEOrder "Due Date Offset" $sDueDateOffset]
	} else {
	    puts " setting startDate"
	    set sMqlRet [mql mod bus $sBusID originated $sStartDate TCETEEStartDate $sStartDate modified $sStartDate "Scheduled Completion Date" $sDueDate "Actual Completion Date" $sDueDate "Due Date Offset" $sDueDateOffset]
	    set sMqlRet [mql mod connection $sRouteNodeID TCETEEStartDate $sStartDate "Scheduled Completion Date" $sDueDate "Actual Completion Date" $sDueDate "TCETEEOrder" $sTCETEEOrder "TCETEEUserPriority" $sTCETEEOrder "Due Date Offset" $sDueDateOffset]
	}
	puts "OK."
    }
}

proc resetRoutes {sDate sFormat {sWhere ""}} {
    if {$sWhere == ""} {
	set sRouteList [mql temp query bus "Route" * * select id format dump |]
    } else {
	set sRouteList [mql temp query bus "Route" * * where $sWhere select id format dump |]
    }
    set lRouteList [split $sRouteList "\n"]
    foreach sRoute $lRouteList {
	set lBusAttrs [split $sRoute "|"]
	set sBusType [string trim [lindex $lBusAttrs 0] ]
	set sBusName [string trim [lindex $lBusAttrs 1] ]
	set sBusRev [string trim [lindex $lBusAttrs 2] ]
	set sBusID [string trim [lindex $lBusAttrs 3] ]

	puts "Reseting \"$sBusType\" $sBusName $sBusRev according to date: $sDate"

	set sMqlRet [mql mod bus $sBusID originated $sDate modified $sDate state "In Process" start $sDate state "In Process" actual $sDate]
	puts "OK."
    }

}

proc difftimes { s1 s2 } {
    set d [expr { ( ( $s2 - $s1 ) / 86400 ) - 1 }]

    return [$d]
}

proc resetAll {} {
    set sRet [mql start transaction]
    set sFormat "%m/%d/%Y %I:%M:%S %p"
    set sDate [clock format [clock seconds] -format $sFormat]
    set sWhere ""
    if {[catch {
		resetRoutes $sDate $sFormat $sWhere
		resetInboxTasks $sDate $sFormat $sWhere

		set sRet [mql commit transaction]
	    } sMsg ] != 0} {
	puts " FAILED in resetDate! (err: $sMsg)"
	set sRet [mql abort transaction]
    }
}

proc formatDueDateOffset {} {
	set sTaskList [mql temp query bus "Inbox Task" * * select id attribute\[Route Node ID\] attribute\[Due Date Offset\] format dump |]
	set lTaskList [split $sTaskList "\n"]
	foreach sTask $lTaskList {
		set sBusID [string trim [lindex $lBusAttrs 3] ]
		set sRouteNodeID [string trim [lindex $lBusAttrs 4] ]
		set sDueDateOffset [string trim [lindex $lBusAttrs 5] ]
		
		if {![string is integer -strict $sDueDateOffset]} {
			set sDueDateOffset [string trimright sDueDateOffset d]
		}
		
		set sMqlRet [mql mod bus $sBusID "Due Date Offset" $sDueDateOffset]
		set sMqlRet [mql mod connection $sRouteNodeID "Due Date Offset" $sDueDateOffset]
	}
}
