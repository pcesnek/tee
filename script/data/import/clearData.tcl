set context user creator;
verb off;
trigger off;

tcl;

proc deleteAllBOs {sType sName sRevision sWhere} {
    set sBusList [mql temp query bus $sType $sName $sRevision where $sWhere select id format dump |]
    set lBusList [split $sBusList "\n"]
    foreach sBus $lBusList {
	set lBusAttrs [split $sBus "|"]
	set sBusType [string trim [lindex $lBusAttrs 0] ]
	set sBusName [string trim [lindex $lBusAttrs 1] ]
	set sBusRev [string trim [lindex $lBusAttrs 2] ]
	set sBusID [string trim [lindex $lBusAttrs 3] ]

	puts -nonewline "Deleting \"$sBusType\" $sBusName $sBusRev, busID: $sBusID..."

	set tokensCount [llength $lBusAttrs]
	if {$tokensCount > 4} {
	    puts -nonewline "files..."
	    for {set i 4} {$i < $tokensCount} {incr i} {
		set sFormat [string trim [lindex $lBusAttrs $i] ]
		set sRes [mql delete bus $sBusID format $sFormat file all]
	    }
	    puts -nonewline "OK. ..."
	}

	set sRes [mql delete bus $sBusID]
	puts "OK."
    }
}

eval {

    if {[catch {
		deleteAllBOs "Workspace,Workspace Vault,Route,Inbox Task" * * "Modified > 3/27/2015"
		deleteAllBOs "ThumbnailViewable,Document,CATProduct For Team,Versioned CATProduct For Team,CATPart For Team,Versioned CATPart For Team,CATDrawing For Team,Versioned CATDrawing For Team,CgrViewable" * * "Modified > 3/27/2015"
	    } sMsg ] != 0} {
	puts " FAILED in clearData! (err: $sMsg)"
    }
}
