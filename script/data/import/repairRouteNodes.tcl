proc repairRouteNodes {sType sName sRevision {sWhere ""}} {
    if {$sWhere == ""} {
	set sBusList [mql query connection type $sType select id format dump |]
    } else {
	set sBusList [mql query connection type $sType where $sWhere select id format dump |]
    }
    set lBusList [split $sBusList "\n"]
    foreach sBus $lBusList {
	set lBusAttrs [split $sBus "|"]
	set sBusType [string trim [lindex $lBusAttrs 0] ]
	set sBusID [string trim [lindex $lBusAttrs 1] ]

	puts -nonewline "Repairing \"$sBusType\", busID: $sBusID..."

	set sRes [mql mod connection $sBusID 'Route Node ID' $sBusID 'Review Task' 'Yes']
	puts "OK."
    }
}

proc repairRouteScope {sType sName sRevision {sWhere ""}} {
    if {$sWhere == ""} {
	set sBusList [mql temp query bus $sType $sName $sRevision  select id to\[Route Scope\].from.id format dump |]
    } else	{
	set sBusList [mql temp query bus $sType $sName $sRevision where $sWhere select id to\[Route Scope\].from.id format dump |]
    }
    set lBusList [split $sBusList "\n"]
    foreach sBus $lBusList {
	set lBusAttrs [split $sBus "|"]
	set sBusType [string trim [lindex $lBusAttrs 0] ]
	set sBusName [string trim [lindex $lBusAttrs 1] ]
	set sBusRev [string trim [lindex $lBusAttrs 2] ]
	set sBusID [string trim [lindex $lBusAttrs 3] ]
	set sWorkspaceID [string trim [lindex $lBusAttrs 4] ]

	puts -nonewline "Repairing \"$sBusType\" $sBusName $sBusRev, busID: $sBusID..."

	set sRes [mql mod bus $sBusID 'Restrict Members' $sWorkspaceID]
	puts "OK."
    }
}

#proc repairInboxTask {fromT fromN fromR toT toN toR title T N R} {
#    set sBusList [mql query connection from $fromT $fromN $fromR to $toT $toN $toR rel "Route Node" where "attribute\[Title\] == '$title']" select id format dump |]
#    set lBusAttrs [split $sBusList "|"]
#    set sBusType [string trim [lindex $lBusAttrs 0] ]
#    set sBusID [string trim [lindex $lBusAttrs 1] ]
#
#    puts -nonewline "Repairing \"$T\" $N $R, for 'Route Node ID: $sBusID..."
#
#    set sRes [mql mod bus $T $N $R 'Route Node ID' $sBusID]
#    puts "OK."
#}

proc repairInboxTasks {sBusList} {
    set lBusList [split $sBusList "\n"]
    set lBusAttrs [split $sBusList "|"]
    foreach sBus $lBusList {
	set lBusAttrs [split $sBus "|"]
	set fromT [string trim [lindex $lBusAttrs 0] ]
	set fromN [string trim [lindex $lBusAttrs 1] ]
	set fromR [string trim [lindex $lBusAttrs 2] ]
	set toT [string trim [lindex $lBusAttrs 3] ]
	set toN [string trim [lindex $lBusAttrs 4] ]
	set toR [string trim [lindex $lBusAttrs 5] ]
	set title [string trim [lindex $lBusAttrs 6] ]
	set T [string trim [lindex $lBusAttrs 7] ]
	set N [string trim [lindex $lBusAttrs 8] ]
	set R [string trim [lindex $lBusAttrs 9] ]

	set sConn [mql query connection from $fromT $fromN $fromR to $toT $toN $toR rel "Route Node" where "attribute\[Title\] == '$title']" select id format dump |]
	set lConnAttrs [split $sConn "|"]
	set ssConnType [string trim [lindex $lConnAttrs 0] ]
	set ssConnID [string trim [lindex $lConnAttrs 1] ]

	puts -nonewline "Repairing \"$T\" $N $R, for 'Route Node ID: $ssConnID..."

	set sRes [mql mod bus $T $N $R 'Route Node ID' $ssConnID 'Review Task' 'Yes']
	puts "OK."
    }
}

proc repairAll {} {
    set sRet [mql start transaction]

    if {[catch {
		#repairRouteNodes "Route Node" * * "Originated > 1/22/2015"
		#repairRouteScope "Route" * * "Originated > 1/22/2015"
		repairRouteNodes "Route Node" * *
		repairRouteScope "Route" * *

		set sBusList "Route|Complete Motor Design||Person|mlojka|-|Review1|Inbox Task|IT-0000100|1\n\
		Route|Complete Motor Design||Person|ssevcik|-|Synchronize|Inbox Task|IT-0000101|1\n\
		Route|Complete Motor Design||Person|mlojka|-|Approval1|Inbox Task|IT-0000102|1\n\
		Route|Complete Motor Design||Person|ssevcik|-|Notification1|Inbox Task|IT-0000103|1\n\
		Route|Complete Motor Design||Person|ssevcik|-|Designer Notification|Inbox Task|IT-0000108|1\n\
		Route|Complete Motor Design||Person|jguran|-|Leader Approval|Inbox Task|IT-0000109|1\n\
		Route|Complete Handlebar Design||Person|mlojka|-|Owner Review|Inbox Task|IT-0000104|1\n\
		Route|Complete Handlebar Design||Person|ssevcik|-|Designer Synchronization|Inbox Task|IT-0000105|1\n\
		Route|Complete Handlebar Design||Person|mlojka|-|Owner Approval|Inbox Task|IT-0000106|1\n\
		Route|Complete Handlebar Design||Person|ssevcik|-|Designer Review 1|Inbox Task|IT-0000107|1\n\
		Route|Complete Handlebar Design||Person|jguran|-|Leader Approval|Inbox Task|IT-0000110|1\n\
		Route|Complete Handlebar Design||Person|ssevcik|-|Designer Review 2|Inbox Task|IT-0000111|1"

		repairInboxTasks $sBusList

		#		repairInboxTask "Route" "Complete Motor Design" "" "Person" "mlojka" "-" "Review1" "Inbox Task" "IT-0000100" "1"
		#		repairInboxTask "Route" "Complete Motor Design" "" "Person" "ssevcik" "-" "Synchronize" "Inbox Task" "IT-0000101" "1"
		#		repairInboxTask "Route" "Complete Motor Design" "" "Person" "mlojka" "-" "Approval1" "Inbox Task" "IT-0000102" "1"
		#		repairInboxTask "Route" "Complete Motor Design" "" "Person" "ssevcik" "-" "Notification1" "Inbox Task" "IT-0000103" "1"
		#		repairInboxTask "Route" "Complete Handlebar Design" "" "Person" "mlojka" "-" "Owner Review" "Inbox Task" "IT-0000104" "1"
		#		repairInboxTask "Route" "Complete Handlebar Design" "" "Person" "ssevcik" "-" "Designer Synchronization" "Inbox Task" "IT-0000105" "1"
		#		repairInboxTask "Route" "Complete Handlebar Design" "" "Person" "mlojka" "-" "Owner Approval" "Inbox Task" "IT-0000106" "1"
		#		repairInboxTask "Route" "Complete Handlebar Design" "" "Person" "ssevcik" "-" "Designer Review 1" "Inbox Task" "IT-0000107" "1"

		set sRet [mql commit transaction]

	    } sMsg ] != 0} {
	puts " FAILED in repairRouteNodes! (err: $sMsg)"
	set sRet [mql abort transaction]
    }
}
