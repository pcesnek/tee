set context user creator;
verb off;
trigger off;

tcl;

eval {
	
	set home [pwd]

	set sImportTclFileName "repairRouteNodes.tcl"
	puts -nonewline "Sourcing file: $sImportTclFileName..."
	if [catch {source $sImportTclFileName} sErrorMessage] {
		puts "problems in sourcing '$sImportTclFileName'"
		puts stdout $sErrorMessage
		mql quit -1
	}
	puts "OK."
	
	set sImportResetDate "resetDate.tcl"
	puts -nonewline "Sourcing file: $sImportResetDate..."
	if [catch {source $sImportResetDate} sErrorMessage] {
		puts "problems in sourcing '$sImportResetDate'"
		puts stdout $sErrorMessage
		mql quit -1
	}
	puts "OK."

	puts "-- Start of spinner data import...."
	if [catch {mql exec prog emxSpinnerAgent.tcl "$home/../spinner/" "emxSpinnerSettings_TriggersOff.tcl" force} sErrorMessage] {
		puts "problems in importing spinner data!"
		puts stdout $sErrorMessage
		mql quit -1
	}
	puts "-- End of spinner data import."
	
	puts "-- Start of correct spinner-imported data..."
	if [catch {repairAll} sErrorMessage] {
		puts "problems in correcting spinner-imported data"
		puts stdout $sErrorMessage
		mql quit -1
	}
	puts "-- End of correct spinner-imported data."
	
	puts "-- Start of reset spinner-imported dates..."
	if [catch {resetAll} sErrorMessage] {
		puts "problems in reseting spinner-imported dates"
		puts stdout $sErrorMessage
		mql quit -1
	}
	puts "-- End of reset spinner-imported dates."
}