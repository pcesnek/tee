set context user creator;
verb off;
trigger off;

tcl;

proc deleteAllBOs {sType sName sRevision {sSkipIfTypeNameContains ""}} {
	set sBusList [mql temp query bus $sType $sName $sRevision vault 'eService Production' select id format dump |]
	set lBusList [split $sBusList "\n"]
	foreach sBus $lBusList {
		set lBusAttrs [split $sBus "|"]
		set sBusType [string trim [lindex $lBusAttrs 0] ]
		set sBusName [string trim [lindex $lBusAttrs 1] ]
		set sBusRev [string trim [lindex $lBusAttrs 2] ]
		set sBusID [string trim [lindex $lBusAttrs 3] ]
		
		if {$sSkipIfTypeNameContains != "" } {
			set iStartIndex [string first $sSkipIfTypeNameContains $sBusType]

			if {$iStartIndex != -1} {
				puts "Skipping deletion of  \"$sBusType\" $sBusName $sBusRev, busID: $sBusID..."
				continue;
			}
		}
		
		puts -nonewline "Deleting \"$sBusType\" $sBusName $sBusRev, busID: $sBusID..."
		
		set tokensCount [llength $lBusAttrs]
		if {$tokensCount > 4} {
			puts -nonewline "files..."
			for {set i 4} {$i < $tokensCount} {incr i} {
				set sFormat [string trim [lindex $lBusAttrs $i] ]
				set sRes [mql delete bus $sBusID format $sFormat file all]
			}
			puts -nonewline "OK. ..." 
		}
		
		set sRes [mql delete bus $sBusID]
		puts "OK."
	}
}

#deletes all TEE Workspace along with Folder structure
proc deleteAllTeeWorkspaces {} {
	set sBusList [mql temp query bus 'Workspace' * * vault 'eService Production' select id attribute\[TCETEEManaged\].value dump |]
	set lBusList [split $sBusList "\n"]
	foreach sBus $lBusList {
		set lBusAttrs [split $sBus "|"]
		set sBusType [string trim [lindex $lBusAttrs 0] ]
		set sBusName [string trim [lindex $lBusAttrs 1] ]
		set sBusRev [string trim [lindex $lBusAttrs 2] ]
		set sBusID [string trim [lindex $lBusAttrs 3] ]
		set sTEEManaged [string trim [lindex $lBusAttrs 4] ]
		
		
		if {$sTEEManaged != "TRUE" } {
			puts "Skipping deletion of  \"$sBusType\" $sBusName $sBusRev, busID: $sBusID..."
			continue;
		}
		
		puts  "About to delete folders for \"$sBusType\" $sBusName $sBusRev, busID: $sBusID: "
		
		set sFoldListStr [mql expand bus $sBusID recurse to all from relationship  'Data Vaults,Sub Vaults' select businessobject id dump |]
		set lFoldList [split $sFoldListStr "\n"]
		foreach sFold $lFoldList {
			set lFoldAttrs [split $sFold "|"]
			set sLevel [string trim [lindex $lFoldAttrs 0] ]
			set sLinkType [string trim [lindex $lFoldAttrs 1] ]
			set sLinkDir [string trim [lindex $lFoldAttrs 2] ]
			set sFoldType [string trim [lindex $lFoldAttrs 3] ]
			set sFoldName [string trim [lindex $lFoldAttrs 4] ]
			set sFoldRev [string trim [lindex $lFoldAttrs 5] ]
			set sFoldID [string trim [lindex $lFoldAttrs 6] ]
		
			puts -nonewline " Deleting Folder \"$sFoldName\" level:$sLevel, busID: $sFoldID..."		
			set sRes [mql delete bus $sFoldID]
			puts "OK."
		}
		
		puts -nonewline "Deleting \"$sBusType\" $sBusName $sBusRev, busID: $sBusID..."
		
		set sRes [mql delete bus $sBusID]
		puts "OK."
	}
}

eval {

	puts -nonewline "Starting transaction..."
	if {[catch {mql start transaction } sMsg ] != 0 } {
		puts "Failed."
		exit -1;
	} else {
		puts "OK."
	}

	if {[catch {
		deleteAllBOs "*CAT*" * * "Template"
		deleteAllBOs "*ProE*" * * "Template"
		deleteAllBOs CgrViewable * *
		deleteAllBOs ThumbnailViewable * *
		deleteAllBOs "Document" TEE* *
		deleteAllBOs "Inbox Task" * *
		# deleteAllBOs "Workspace Vault" Analysis *
		# deleteAllBOs "Workspace Vault" Design *
		# deleteAllBOs "Workspace Vault" Drawings *
		# deleteAllBOs "Workspace Vault" Manufacturing *
		# deleteAllBOs "Workspace Vault" "Mechanical Design" *
		# deleteAllBOs "Workspace Vault" "Mechanical Design" *
		# deleteAllBOs "Workspace Vault" References *
		deleteAllBOs "Route" * *
		
		deleteAllTeeWorkspaces
	
		set sMqlRet [mql mod bus 'eService Number Generator' type_Part 'A Size' 'eService Next Number' 0000001]
		set sMqlRet [mql mod bus 'eService Number Generator' type_Part 'B Size' 'eService Next Number' 0000001]
		set sMqlRet [mql mod bus 'eService Number Generator' type_Part 'C Size' 'eService Next Number' 0000001]
		set sMqlRet [mql mod bus 'eService Number Generator' type_Part 'D Size' 'eService Next Number' 0000001]
		
		puts -nonewline "Committing transaction..."
		set sMqlRet [mql commit transaction]
		puts "OK."
	} sMsg ] != 0} {
			puts " FAILED in TearDownAll! (err: $sMsg)"
			puts -nonewline "Aborting transaction..."
			set sMqlRet [mql abort transaction]
			puts "OK."
	}
	
	
}

