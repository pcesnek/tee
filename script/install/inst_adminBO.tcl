
proc installAllAdminBusObjects {} {
	
	global sAppDefinitionTypeName
	global sProductName
	puts -nonewline " - Installing admin BO  $sAppDefinitionTypeName $sProductName ..."
	if [catch {set sRet [installTEEAppDefinitionBO] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts  "Ok."
	
}

proc installTEEAppDefinitionBO {} {
	global sAppDefinitionTypeName
	global sProductName	
	global sMyApps_PolicyPolicyName
	global eServiceAdministrationVaultName
	
	set sRevision "-"
	
	set sRet "SUCCESS"
	
	set busID [mql temp query bus $sAppDefinitionTypeName $sProductName $sRevision select id dump] 
	if {$busID == ""} {
		if [catch { mql add bus $sAppDefinitionTypeName $sProductName $sRevision vault $eServiceAdministrationVaultName policy $sMyApps_PolicyPolicyName \
			description "Lean Engineering Experience Application" \
			"App Service" "CollaborativePlatform" \
			"App Type" "Web" \
			"App URL" "appName=TCE-TEE" \
			"App Brand" "ENOVIA" \
			"App Quadrant" "North" \
			"App Display Name" "TCE-TEE" \
			"App Name" "TCE-TEE" \
			"App Icon" "ENOUNBO_AP_AppIcon.png" \
			} sErrorMessage] {
				puts "Failed to add BO  $sAppDefinitionTypeName  $sProductName $sRevision : $sErrorMessage"
				return -code 1
			}
	} 
	
	return $sRet
}



