proc installAllRelationships {} {
	set sRet "SUCCESS"
	
	global sRouteNodeRelationshipName
	puts -nonewline " - Installing relationship $sRouteNodeRelationshipName..."
	if [catch {set sRet [installRelationshipRouteNode] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts "OK."
	
	global sTaskDocumentRelationshipName
	puts -nonewline " - Installing relationship $sTaskDocumentRelationshipName..."
	if [catch {set sRet [installRelationshipTaskDocument] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts "OK."	
	
	return $sRet
}

proc installRelationshipRouteNode {} {
	set sRet "SUCCESS"
	global sRouteNodeRelationshipName
	global iTEEOrderAttrName
	global sTEEStartDateAttrName
	global sTEEUserPriorityAttrName
	global sTEEMilestoneDateAttrName
	
	
	set sRet [addAttrToRelationship $sRouteNodeRelationshipName $iTEEOrderAttrName]
	set sRet [addAttrToRelationship $sRouteNodeRelationshipName $sTEEStartDateAttrName]
	set sRet [addAttrToRelationship $sRouteNodeRelationshipName $sTEEUserPriorityAttrName]
	set sRet [addAttrToRelationship $sRouteNodeRelationshipName $sTEEMilestoneDateAttrName]
	
	return $sRet
}

proc installRelationshipTaskDocument {} {
	set sRet "SUCCESS"
	global sTaskDocumentRelationshipName
	global sInboxTaskTypeName
	global sAllObjectsTypeName
	
	set sRet [installRelationship $sTaskDocumentRelationshipName "TEE Task Relations to Parts and Documents"]
	
	set sRet [addRelationshipFromType $sTaskDocumentRelationshipName $sInboxTaskTypeName]
	set sRet [addRelationshipToType	$sTaskDocumentRelationshipName $sAllObjectsTypeName]
	

	set sRet [setRelationshipFromCardinality $sTaskDocumentRelationshipName many]
	set sRet [setRelationshipToCardinality $sTaskDocumentRelationshipName many]
	set sRet [setRelationshipBehavior_FromRevision $sTaskDocumentRelationshipName replicate]
	set sRet [setRelationshipBehavior_FromClone $sTaskDocumentRelationshipName replicate]
	set sRet [setRelationshipBehavior_ToRevision $sTaskDocumentRelationshipName replicate]
	set sRet [setRelationshipBehavior_ToClone $sTaskDocumentRelationshipName replicate]
	
	return $sRet
}