proc installFiles {} {
	#set sSourceDir $::env(WORKSPACE)/../../copy
	
	
	set sSourceDir [pwd]
	set sSourceDir $sSourceDir/..
	
	global sEnoviaHome

	#backup files that will be updated
	set file_exist [file exists "$sEnoviaHome/STAGING/ematrix/properties/emxSystem_backup.properties"]
	if {$file_exist == 0} {file copy -force "$sEnoviaHome/STAGING/ematrix/properties/emxSystem.properties" "$sEnoviaHome/STAGING/ematrix/properties/emxSystem_backup.properties"}
	
	# --- licensing ??? --- 
	#set file_exist [file exists "$sEnoviaHome/STAGING/ematrix/properties/emxLogin_backup.jsp"]
	#if {$file_exist == 0} {file copy -force "$sEnoviaHome/STAGING/ematrix/emxLogin.jsp" "$sEnoviaHome/STAGING/ematrix/emxLogin_backup.jsp"}
	
	#copy property files
	file copy -force "$sSourceDir/server/WEB-INF/classes/teeStringResource.properties" "$sEnoviaHome/STAGING/ematrix/properties/"
	file copy -force "$sSourceDir/server/WEB-INF/classes/tee.properties" "$sEnoviaHome/STAGING/ematrix/properties/"
	file copy -force "$sSourceDir/server/WEB-INF/classes/TCE-TEE_en.properties" "$sEnoviaHome/STAGING/ematrix/properties/"
	
	#copy jar files
	file copy -force "$sSourceDir/jar/tctee.jar" "$sEnoviaHome/win_b64/docs/javaserver/"
	file copy -force "$sSourceDir/jar/mnglicenseclient.jar" "$sEnoviaHome/win_b64/docs/javaserver/"
	file copy -force "$sSourceDir/jar/json-simple-1.1.1.jar" "$sEnoviaHome/win_b64/docs/javaserver/"

	#copy jsp files
	file delete -force "$sEnoviaHome/STAGING/ematrix/tee"
	file copy -force  "$sSourceDir/server/tee" "$sEnoviaHome/STAGING/ematrix"
	
	#copy part of web.xml
	set sFrom "$sSourceDir/server/WEB-INF"
	set sTo "$sEnoviaHome/win_b64/resources/warutil/fragment"

	if {[catch {file copy -force $sFrom/TEE.web.xml.part $sTo} sError]} {
	   puts "web.xml.parts file copy failed: err:$sError to:$sTo"
	}   	
}