proc installAllTypes {} {
	set sRet "SUCCESS"
	
	global sWorkspaceTypeName
	puts -nonewline " - Installing type $sWorkspaceTypeName..."
	if [catch {set sRet [installTypeWorkspace] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts "OK."
	
	global sInboxTaskTypeName
	puts -nonewline " - Installing type $sInboxTaskTypeName..."
	if [catch {set sRet [installTypeInboxTask] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts "OK."
	
	return $sRet
}

proc installTypeWorkspace {} {
	set sRet "SUCCESS"
	global sWorkspaceTypeName
	global iTEEMamagedAttrName

	set sRet [addAttrToType	$sWorkspaceTypeName $iTEEMamagedAttrName]

	return $sRet
}

proc installTypeInboxTask {} {
	set sRet "SUCCESS"
	global sInboxTaskTypeName
	global iTEEOrderAttrName
	global sTEEStartDateAttrName
	global sTEEUserPriorityAttrName
	global sTEEMilestoneDateAttrName

	set sRet [addAttrToType	$sInboxTaskTypeName $iTEEOrderAttrName]
	set sRet [addAttrToType	$sInboxTaskTypeName $sTEEStartDateAttrName]
	set sRet [addAttrToType	$sInboxTaskTypeName $sTEEUserPriorityAttrName]
	set sRet [addAttrToType	$sInboxTaskTypeName $sTEEMilestoneDateAttrName]

	return $sRet
}