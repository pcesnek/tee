proc installAllTables {} {
	set sRet "SUCCESS"
	# global sMPPOperationsTableName
	# puts -nonewline " - Installing table $sMPPOperationsTableName..."
	# if [catch {set sRet [installProcessPlanOperationsTable] } sErrorMessage] {
		# puts $sErrorMessage
		# mql quit -1
		# exit
	# }
	# puts  "Ok."
	
	return $sRet
}

# proc installProcessPlanOperationsTable {} {
	# # Name | Operation | Section separator | Title | Tact | Work Place | Resource Category | Customer Number | Description | State
	# set sRet "SUCCESS"
	# global sMPPOperationsTableName
	# global sChannelMPPRelatedObjectsChannelName
	# global sTCMPP_UIAccess_JPO_Name
	# global sTCMPP_UIAccess_JPO_canEditMPPOperationsSBTableCellsMethodName

	
	# set sRet [installTable $sMPPOperationsTableName]
	
	# set sColumnName Name
	# set sRet [addColumnToTable $sMPPOperationsTableName $sColumnName emxFramework.Basic.Name]
	# set sRet [setTableColumnBusinessObjectExpression $sMPPOperationsTableName $sColumnName "\$<name>"]
	# set sRet [setTableColumnHref $sMPPOperationsTableName $sColumnName "\${COMMON_DIR}/emxRefreshChannel.jsp?channel=$sChannelMPPRelatedObjectsChannelName"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Editable" false]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Registered Suite" Components]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Show Type Icon" true]
	
	# global sOrderNumberLinkAttrName
	# global 	sMPPOperationsTableOrderNumberColumnName
	# global sTCMPP_MPPOperations_JPO_Name
	# global sTCMPP_MPPOperations_JPO_setMPPOperationsPreferenceOrderNumberMethodName
	# global sTCMPP_MPPOperationsUI_JPO_Name
	# global sTCMPP_MPPOperationsUI_JPO_getColumnData
	# set symbAttrName [makeSymbName attribute $sOrderNumberLinkAttrName]
	# set sColumnName $sMPPOperationsTableOrderNumberColumnName
	# set sRet [addColumnToTable $sMPPOperationsTableName $sColumnName TCMPP.Table.Column.OrderNumber.Label]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Editable" true]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Column Type" programHTMLOutput]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Registered Suite" TCMPP]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Width" 50]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Edit Access Program" $sTCMPP_UIAccess_JPO_Name]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Edit Access Function" $sTCMPP_UIAccess_JPO_canEditMPPOperationsSBTableCellsMethodName]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Update Program" $sTCMPP_MPPOperations_JPO_Name]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Update Function" $sTCMPP_MPPOperations_JPO_setMPPOperationsPreferenceOrderNumberMethodName]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "program" $sTCMPP_MPPOperationsUI_JPO_Name]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "function" $sTCMPP_MPPOperationsUI_JPO_getColumnData]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Sort Type" integer]
	
	
	# #set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Style Column" MeasurementCalculateColumnBaseSizeColor]
	
	# set sColumnName IconNewWindow
	# set sRet [addColumnToTable $sMPPOperationsTableName $sColumnName ""]
	# set sRet [setTableColumnHref $sMPPOperationsTableName $sColumnName "\${COMMON_DIR}/emxTree.jsp"]
	# set sRet [setTableColumnAlt $sMPPOperationsTableName $sColumnName TCMPP.Table.Column.IconNewWindow.Tooltip]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Registered Suite" TCMPP]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Column Type" icon]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Column Icon" "images/iconNewWindow.gif"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Width" 10]
	

	# global sTitleAttrName
	# set symbAttrName [makeSymbName attribute $sTitleAttrName]
	# set sColumnName Title
	# set sRet [addColumnToTable $sMPPOperationsTableName $sColumnName  emxFramework.Attribute.Title]
	# set sRet [setTableColumnBusinessObjectExpression $sMPPOperationsTableName $sColumnName "\$<attribute\[$symbAttrName\].value>"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Editable" true]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Required" true]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Registered Suite" Components]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Admin Type" $symbAttrName]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Field Type" attribute]	
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Width" 150]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Edit Access Program" $sTCMPP_UIAccess_JPO_Name]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Edit Access Function" $sTCMPP_UIAccess_JPO_canEditMPPOperationsSBTableCellsMethodName]
	
	# set sColumnName Description
	# set sRet [addColumnToTable $sMPPOperationsTableName $sColumnName emxFramework.Basic.Description]
	# set sRet [setTableColumnBusinessObjectExpression $sMPPOperationsTableName $sColumnName "\$<description>"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Editable" true]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Registered Suite" Components]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Input Type" textarea]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Admin Type" description]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Field Type" basic]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Width" 300]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Edit Access Program" $sTCMPP_UIAccess_JPO_Name]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Edit Access Function" $sTCMPP_UIAccess_JPO_canEditMPPOperationsSBTableCellsMethodName]
	
	# global iOperationTimeAttrName 
	# set symbAttrName [makeSymbName attribute $iOperationTimeAttrName]
	# set sColumnName SetupTime
	# set sRet [addColumnToTable $sMPPOperationsTableName $sColumnName TCMPP.Table.Column.SetupTime.Label]
	# set sRet [setTableColumnRelationshipExpression $sMPPOperationsTableName $sColumnName "\$<attribute\[$symbAttrName\].value>"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Editable" true]	
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Registered Suite" TCMPP]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Admin Type" $symbAttrName]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Field Type" attribute]	
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Width" 50]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Edit Access Program" $sTCMPP_UIAccess_JPO_Name]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Edit Access Function" $sTCMPP_UIAccess_JPO_canEditMPPOperationsSBTableCellsMethodName]

	# global iBatchTimeAttrName 
	# set symbAttrName [makeSymbName attribute $iBatchTimeAttrName]
	# set sColumnName ExecutionTime
	# set sRet [addColumnToTable $sMPPOperationsTableName $sColumnName TCMPP.Table.Column.ExecutionTime.Label]
	# set sRet [setTableColumnRelationshipExpression $sMPPOperationsTableName $sColumnName "\$<attribute\[$symbAttrName\].value>"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Editable" true]	
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Registered Suite" TCMPP]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Admin Type" $symbAttrName]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Field Type" attribute]	
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Width" 60]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Edit Access Program" $sTCMPP_UIAccess_JPO_Name]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Edit Access Function" $sTCMPP_UIAccess_JPO_canEditMPPOperationsSBTableCellsMethodName]
	
	# global sMPPWorkPlaceOperationRelationshipName
	# set symbLinkName [makeSymbName relationship $sMPPWorkPlaceOperationRelationshipName]
	# set sColumnName WorkPlace
	# set sRet [addColumnToTable $sMPPOperationsTableName $sColumnName TCMPP.Table.Column.OperationWorkPlace.Label]
	# set sRet [setTableColumnBusinessObjectExpression $sMPPOperationsTableName $sColumnName "\$<from\[$symbLinkName\].to.name>"]
	# set sRet [setTableColumnHref $sMPPOperationsTableName $sColumnName "\${COMMON_DIR}/emxTree.jsp"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Registered Suite" TCMPP]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Alternate OID expression" "\$<from\[$symbLinkName\].to.id>"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Alternate Type expression" "\$<from\[$symbLinkName\].to.type>"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Show Alternate Icon" true]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Width" 100]
	
	# global sMPPResourceCategoryOperationRelationshipName
	# set symbLinkName [makeSymbName relationship $sMPPResourceCategoryOperationRelationshipName]
	# set sColumnName ResourceCategory
	# set sRet [addColumnToTable $sMPPOperationsTableName $sColumnName TCMPP.Table.Column.OperationResourceCategory.Label]
	# set sRet [setTableColumnBusinessObjectExpression $sMPPOperationsTableName $sColumnName "\$<from\[$symbLinkName\].to.name>"]
	# set sRet [setTableColumnHref $sMPPOperationsTableName $sColumnName "\${COMMON_DIR}/emxTree.jsp"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Registered Suite" TCMPP]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Alternate OID expression" "\$<from\[$symbLinkName\].to.id>"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Alternate Type expression" "\$<from\[$symbLinkName\].to.type>"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Show Alternate Icon" true]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Width" 100]
	
	# global sMPPToolRelationshipName
	# set symbLinkName [makeSymbName relationship $sMPPToolRelationshipName]
	# set sColumnName Tool
	# set sRet [addColumnToTable $sMPPOperationsTableName $sColumnName TCMPP.Table.Column.OperationTool.Label]
	# set sRet [setTableColumnBusinessObjectExpression $sMPPOperationsTableName $sColumnName "\$<from\[$symbLinkName\].to.name>"]
	# set sRet [setTableColumnHref $sMPPOperationsTableName $sColumnName "\${COMMON_DIR}/emxTree.jsp"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Registered Suite" TCMPP]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Alternate OID expression" "\$<from\[$symbLinkName\].to.id>"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Alternate Type expression" "\$<from\[$symbLinkName\].to.type>"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Show Alternate Icon" true]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Width" 100]
	
	# global sMPPInputMaterialRelationshipName
	# set symbLinkName [makeSymbName relationship $sMPPInputMaterialRelationshipName]
	# set sColumnName InputMaterial
	# set sRet [addColumnToTable $sMPPOperationsTableName $sColumnName TCMPP.Table.Column.OperationInputMaterial.Label]
	# set sRet [setTableColumnBusinessObjectExpression $sMPPOperationsTableName $sColumnName "\$<from\[$symbLinkName\].to.name>"]
	# set sRet [setTableColumnHref $sMPPOperationsTableName $sColumnName "\${COMMON_DIR}/emxTree.jsp"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Registered Suite" TCMPP]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Alternate OID expression" "\$<from\[$symbLinkName\].to.id>"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Alternate Type expression" "\$<from\[$symbLinkName\].to.type>"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Show Alternate Icon" true]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Width" 100]
	
	# global sMPPOutputMaterialRelationshipName
	# set symbLinkName [makeSymbName relationship $sMPPOutputMaterialRelationshipName]
	# set sColumnName OutputMaterial
	# set sRet [addColumnToTable $sMPPOperationsTableName $sColumnName TCMPP.Table.Column.OperationOutputMaterial.Label]
	# set sRet [setTableColumnBusinessObjectExpression $sMPPOperationsTableName $sColumnName "\$<from\[$symbLinkName\].to.name>"]
	# set sRet [setTableColumnHref $sMPPOperationsTableName $sColumnName "\${COMMON_DIR}/emxTree.jsp"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Registered Suite" TCMPP]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Alternate OID expression" "\$<from\[$symbLinkName\].to.id>"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Alternate Type expression" "\$<from\[$symbLinkName\].to.type>"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Show Alternate Icon" true]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Width" 100]

	# global sMPPImageManagerToolbarMenuName
	# set sColumnName Image
	# set sRet [addColumnToTable $sMPPOperationsTableName $sColumnName ""]
	# set sRet [setTableColumnHref $sMPPOperationsTableName $sColumnName "\${SUITE_DIR}/TCMPP_PreventCachingForURLProcess.jsp?mppAction=showImageManager&toolbar=$sMPPImageManagerToolbarMenuName&header=TCMPP.Category.Images.Title"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Column Type" "image"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Registered Suite" TCMPP]
	
	# set sColumnName State
	# set sRet [addColumnToTable $sMPPOperationsTableName $sColumnName emxFramework.Basic.Current]
	# set sRet [setTableColumnBusinessObjectExpression $sMPPOperationsTableName $sColumnName "\$<current>"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Editable" false]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Registered Suite" Components]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Width" 80]
	
	# set sColumnName Revision
	# set sRet [addColumnToTable $sMPPOperationsTableName $sColumnName emxFramework.Basic.Revision]
	# set sRet [setTableColumnBusinessObjectExpression $sMPPOperationsTableName $sColumnName "\$<revision>"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Registered Suite" Components]
	
	# global sEffectivityStartDateAttrName
	# set sColumnName EffectivityStartDate
	# set sFieldName $sEffectivityStartDateAttrName
	# set symbAttrName [makeSymbName attribute $sEffectivityStartDateAttrName]
	# set sRet [addColumnToTable $sMPPOperationsTableName $sColumnName TCMPP.Table.Column.EffectivityStartDate.Label]
	# set sRet [setTableColumnBusinessObjectExpression $sMPPOperationsTableName $sColumnName "\$<attribute\[$symbAttrName\].value>"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Editable" false]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "format" date]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Registered Suite" TCMPP]	
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Field Type" attribute]	
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Admin Type" $symbAttrName]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Access Expression" type!=TCMPP_MPPTemplate]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Display Format" 2]
	
	
	# global sEffectivityEndDateAttrName
	# set sColumnName EffectivityEndDate
	# set sFieldName $sEffectivityEndDateAttrName
	# set symbAttrName [makeSymbName attribute $sEffectivityEndDateAttrName]
	# set sRet [addColumnToTable $sMPPOperationsTableName $sColumnName TCMPP.Table.Column.EffectivityEndDate.Label]
	# set sRet [setTableColumnBusinessObjectExpression $sMPPOperationsTableName $sColumnName "\$<attribute\[$symbAttrName\].value>"]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Editable" false]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "format" date]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Registered Suite" TCMPP]	
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Field Type" attribute]	
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Admin Type" $symbAttrName]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Access Expression" type!=TCMPP_MPPTemplate]
	# set sRet [setTableColumnSetting $sMPPOperationsTableName $sColumnName "Display Format" 2]
	
	# return $sRet
# }
