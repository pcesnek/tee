proc updateFiles {} {

	#set sSourceDir $::env(WORKSPACE)/../../copy
	set emxSystemApplicationListAddon "emxFramework.ApplicationList = TCTEE, \\
    "
	
	set emxSystemPropertiesAddon "	# Transcat Engineering Experience Begin
	emxFramework.UISuite.TCTEE = TCTEE
	TCTEE.Directory = tee
	TCTEE.StringResourceFileId = teeStringResource
	TCTEE.ApplicationPropertyFile = tee.properties
	
	emxFramework.TCE-TEE.HomePage=TCETEEApplicationHome
	
	# Transcat Engineering Experience End"	
	
	
	set emxSecurityContextSelectionImports "<%-- Manufacturing Process Plan Begin --%>
<%@ page import = \"com.transcat.mpp.utils.LanguageUtils\"%>
<%@ page import = \"com.transcat.mpp.utils.MPPLog\"%>
<%-- Manufacturing Process Plan End --%>
<%@ page import=\"com.matrixone.apps.domain.util.PropertyUtil\" %>"

	set emxSecurityContextSelectionAddon  "\{
     session.setAttribute(\"error.message\", servletException.getMessage());
\}
// Manufacturing Process Plan Begin
else
\{
    if(context != null)
    {
	   	MPPLog.openLog(context);
	   	LanguageUtils mppLang = new LanguageUtils(context);
	   	mppLang.isLanguageSupported();
    }
    else
    {
    	MPPLog.CloseLog();
    }
\} 
// Manufacturing Process Plan End"

	global sEnoviaHome
	
	#Add necessary lines at the of emxFrameworkStringResource.properties file
	# puts "Edit emxFrameworkStringResource.properties..."
	# set sPropertyFilename "emxFrameworkStringResource.properties"
	# set sPropertyFile [open "$sEnoviaHome/STAGING/ematrix/properties/$sPropertyFilename" r]
	# set fileDataNew ""
	# set delete 0
	# while {[gets $sPropertyFile line] >= 0} {
		# if {[string first "# Manufacturing Process Plan Begin" $line] >=0} {
			# set delete 1
		# }
		# if {$delete == 0} {
			
			# append fileDataNew $line
			# append fileDataNew "\n"
		# }
		# if {[string first "# Manufacturing Process Plan End" $line] >=0} {
			# set delete 0
		# }
	# }
	
	# append fileDataNew $emxFrameworkStringResourcePropertiesAddon
	
	# close $sPropertyFile
	# set sPropertyFile [open "$sEnoviaHome/STAGING/ematrix/properties/$sPropertyFilename" w+]
	# puts $sPropertyFile $fileDataNew
	# close $sPropertyFile
	# puts "Ok."
	
	#emxSystem properties ApplicationList edit part
	puts -nonewline "Edit emxSystem.properties..."
	set sPropertyFilename "emxSystem.properties"
	set sPropertyFile [open "$sEnoviaHome/STAGING/ematrix/properties/$sPropertyFilename" r]
	set fileData [read $sPropertyFile]
	if { [string first TCTEE $fileData] == -1} {
		regsub -all "emxFramework.ApplicationList =" $fileData $emxSystemApplicationListAddon changedFileData
		close $sPropertyFile

		set sPropertyFile [open "$sEnoviaHome/STAGING/ematrix/properties/$sPropertyFilename" w+]
		puts $sPropertyFile $changedFileData
	}
	close $sPropertyFile
	
	#emxSystem properties addon part
	set sPropertyFile [open "$sEnoviaHome/STAGING/ematrix/properties/$sPropertyFilename" r]
	set fileDataNew ""
	set delete 0
	while {[gets $sPropertyFile line] >= 0} {
		if {[string first "# Manufacturing Process Plan Begin" $line] >=0} {
			set delete 1
		}
		if {$delete == 0} {
			
			append fileDataNew $line
			append fileDataNew "\n"
		}
		if {[string first "# Manufacturing Process Plan End" $line] >=0} {
			set delete 0
		}
	}
	
	append fileDataNew $emxSystemPropertiesAddon
	
	close $sPropertyFile
	set sPropertyFile [open "$sEnoviaHome/STAGING/ematrix/properties/$sPropertyFilename" w+]
	puts $sPropertyFile $fileDataNew
	close $sPropertyFile
	puts "Ok."
	

	
	puts -nonewline "Update file emxLogin.jsp ..."
	if [catch {set sRet [updateLoginJSPFile] } sErrorMessage] {
		 puts "Failed."
		 puts $sErrorMessage
		 mql quit -1
		
		 exit
	 }
	 puts "Ok."
	
}

proc updateLoginJSPFile {} {
	set sRet "SUCCESS"
	global sEnoviaHome
	
    set timestamp [clock format [clock seconds] -format {%Y%m%d%H%M%S}]
	set filename $sEnoviaHome/STAGING/ematrix/emxLogin.jsp
	#set filename "C:\\TEE\\workspace\\TEE\\WebContent\\emxLogin.jsp"
	set temp     $filename.new.$timestamp
	set backup   $filename.tee_backup

	set in  [open $filename r]
	set out [open $temp     w]

	# line-by-line, read the original file
	while {[gets $in line] != -1} {
		#transform $line somehow
		set line $line
		
		set findForUpdateTEE [string  first "<!-- TEE Installation -->" $line]			
		if {$findForUpdateTEE >= 0} {
				puts "Skip line"
				continue
			}		
			
		puts $out $line	
		
		set findForTranscatLogo [string  first "<!-- /.panel-content -->" $line]
		set findForCustomerLogo [string  first "class=\"panel\"" $line]
		if {$findForCustomerLogo >= 0} {
			puts $out "<!-- TEE Installation --><img src=\"tee/images/customerlogo.png\">"
			}
		if {$findForTranscatLogo >= 0} {
			puts $out "<!-- TEE Installation --><img src=\"tee/images/transcatlogo.png\">"
			}			
	}
	close $in
	close $out


	# move the new data to the proper filename
	file rename -force $filename $backup
	file rename -force $temp $filename 
	puts "End update file emxLogin.jsp."
		
	return $sRet

}