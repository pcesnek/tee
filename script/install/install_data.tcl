proc installData {} {
	set sRet "SUCCESS"

	
	if [catch {set sRet [installAllRoles] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		exit
	}
	
	if [catch {set sRet [installAllAttributes] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		exit
	}	
	
	 if [catch {set sRet [installAllTypes] } sErrorMessage] {
		 puts "Failed."
		 puts $sErrorMessage
		 mql quit -1
		 exit
	 }
	
	if [catch {set sRet [installAllRelationships] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		exit
	}
	
	if [catch {set sRet [installAllPolicies] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		exit
	}
	
	if [catch {set sRet [installAllCommands] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		exit
	}
	
	if [catch {set sRet [installAllMenus] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		exit
	}
	
	# if [catch {set sRet [installAllChannels] } sErrorMessage] {
		# puts "Failed."
		# puts $sErrorMessage
		# mql quit -1
		# exit
	# }
	
	# if [catch {set sRet [installAllPortals] } sErrorMessage] {
		# puts "Failed."
		# puts $sErrorMessage
		# mql quit -1
		# exit
	# }
	
	if [catch {set sRet [installAllWebForms] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		exit
	}
	
	if [catch {set sRet [installAllTables] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		exit
	}

	# if [catch {set sRet [installAllPrograms] } sErrorMessage] {
		# puts "Failed."
		# puts $sErrorMessage
		# mql quit -1
		# exit
	# }
	
	if [catch {set sRet [installAllAdminBusObjects] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		exit
	}
	
	if [catch {set sRet [installProductTEE] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		exit
	}	
	
}