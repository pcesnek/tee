proc installAllAttributes {} {
	global iTEEMamagedAttrName
	puts -nonewline " - Installing attribute $iTEEMamagedAttrName..."
	if [catch {set sRet [installTEEMamaged] } sErrorMessage] {
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts  "Ok."
	
	global iTEEOrderAttrName
	puts -nonewline " - Installing attribute $iTEEOrderAttrName..."
	if [catch {set sRet [installTEEOrder] } sErrorMessage] {
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts  "Ok."

	global sTEEStartDateAttrName
	puts -nonewline " - Installing attribute $sTEEStartDateAttrName..."
	if [catch {set sRet [installTEEStartDate] } sErrorMessage] {
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts  "Ok."	
	
	global sTEEUserPriorityAttrName
	puts -nonewline " - Installing attribute $sTEEUserPriorityAttrName..."
	if [catch {set sRet [installTEEUserPriority] } sErrorMessage] {
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts  "Ok."	
	
	global sTEEMilestoneDateAttrName
	puts -nonewline " - Installing attribute $sTEEMilestoneDateAttrName..."
	if [catch {set sRet [installTEEMilestoneDate] } sErrorMessage] {
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts  "Ok."	
}



proc installTEEMamaged {} {
	set sRet "SUCCESS"
	global iTEEMamagedAttrName
	set sRet [installAttribute $iTEEMamagedAttrName boolean "Used to determine if Workspace is managed by TEE product"]
	set sRet [setDefaultValue $iTEEMamagedAttrName FALSE]
	set sRet [setAttributeHidden $iTEEMamagedAttrName FALSE]
	
	return $sRet
}

proc installTEEOrder {} {
	set sRet "SUCCESS"
	global iTEEOrderAttrName
	set sRet [installAttribute $iTEEOrderAttrName integer "Used to manage Task order in Workspace/Gantt"]
	set sRet [setDefaultValue $iTEEOrderAttrName 0]
	
	return $sRet
}

proc installTEEStartDate {} {
	set sRet "SUCCESS"
	global sTEEStartDateAttrName
	set sRet [installAttribute $sTEEStartDateAttrName date "Date when task is first moved to InWork"]
	
	return $sRet
}

proc installTEEUserPriority {} {
	set sRet "SUCCESS"
	global sTEEUserPriorityAttrName
	set sRet [installAttribute $sTEEUserPriorityAttrName integer "Used to manage Task order in Kanban"]
	set sRet [setDefaultValue $sTEEUserPriorityAttrName 0]
	return $sRet
}

proc installTEEMilestoneDate {} {
	set sRet "SUCCESS"
	global sTEEMilestoneDateAttrName
	set sRet [installAttribute $sTEEMilestoneDateAttrName date "Used to manage Task Milestone date"]
	return $sRet
}
