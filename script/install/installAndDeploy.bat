@echo off
rem environment variables
set ENOVIA_PATH=c:\enoviaV6R2014x\server
set TOMCAT_PATH=c:\Tomee-plus-1.6.0

rem Run installation
mql install_main.tcl
if %errorlevel% neq 0 exit /b %errorlevel%

rem CREATE WAR file
set INSTPATH=%ENOVIA_PATH%
set SQA=something
set WEBAPPNAME=enovia
set DOCV6PATH="trigger off;"
call %ENOVIA_PATH%\win_b64\code\command\war_setup.bat
if %errorlevel% neq 0 exit /b %errorlevel%

rem REDEPLOY Enovia
net stop tomee
echo deleting dir "%TOMCAT_PATH%\webapps\enovia"
rmdir "%TOMCAT_PATH%\webapps\enovia" /Q /S
echo deleting file "%TOMCAT_PATH%\webapps\enovia.war"
del "%TOMCAT_PATH%\webapps\enovia.war" /Q /F
copy "%ENOVIA_PATH%\distrib\enovia.war" "%TOMCAT_PATH%\webapps\enovia.war" /Y
net start tomee