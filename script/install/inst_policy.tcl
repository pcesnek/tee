proc installAllPolicies {} {
	set sRet "SUCCESS"
	
	global sTCETEETaskPolicyName
	puts -nonewline " - Installing policy $sTCETEETaskPolicyName..."
	if [catch {set sRet [installPolicyTCETEETask] } sErrorMessage] {
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts "OK."
	
	return $sRet
}

proc installPolicyTCETEETask {} {
	set sRet "SUCCESS"
	global sTCETEETaskPolicyName
	global sInboxTaskTypeName
	global sTCETEETaskPolicySequencePattern
	global sTCETEETaskPolicy_stateToDoName
	global sTCETEETaskPolicy_stateInWorkName
	global sTCETEETaskPolicy_stateDoneName
	
	set sRet [installPolicy $sTCETEETaskPolicyName "Policy to cover the life cycle of TCETEE Task"]
	set sRet [setPolicySequence $sTCETEETaskPolicyName $sTCETEETaskPolicySequencePattern]
	set sRet [addPolicyGovernedType $sTCETEETaskPolicyName $sInboxTaskTypeName]
	
	set sRet [addPolicyState $sTCETEETaskPolicyName $sTCETEETaskPolicy_stateToDoName]
	set sRet [addPolicyState $sTCETEETaskPolicyName $sTCETEETaskPolicy_stateInWorkName]
	set sRet [addPolicyState $sTCETEETaskPolicyName $sTCETEETaskPolicy_stateDoneName]

	global sOwnerRoleName 
	global sPublicRoleName
	global sVPLMCreatorRoleName
	global sVPLMProjectLeaderRoleName
	#set sRet [setPolicyAllStateUserAccess $sTCETEETaskPolicyName $sOwnerRoleName all]
	#set sRet [setPolicyAllStateUserAccess $sTCETEETaskPolicyName $sPublicRoleName read]

	set sRet [setPolicyStateSystemUserAccess $sTCETEETaskPolicyName $sTCETEETaskPolicy_stateToDoName $sOwnerRoleName all]
	set sRet [setPolicyStateSystemUserAccess $sTCETEETaskPolicyName $sTCETEETaskPolicy_stateToDoName $sPublicRoleName read]
	set sRet [setPolicyStateUserAccess $sTCETEETaskPolicyName $sTCETEETaskPolicy_stateToDoName $sVPLMCreatorRoleName read,modify,delete,create,changeowner,fromconnect,toconnect,fromdisconnect,todisconnect,show]
	set sRet [setPolicyStateUserAccess $sTCETEETaskPolicyName $sTCETEETaskPolicy_stateToDoName $sVPLMProjectLeaderRoleName read,modify,delete,create,changeowner,fromconnect,toconnect,fromdisconnect,todisconnect,show]
	set sRet [setPolicyStateSystemUserAccess $sTCETEETaskPolicyName $sTCETEETaskPolicy_stateInWorkName $sOwnerRoleName all]
	set sRet [setPolicyStateSystemUserAccess $sTCETEETaskPolicyName $sTCETEETaskPolicy_stateInWorkName $sPublicRoleName read]
	set sRet [setPolicyStateUserAccess $sTCETEETaskPolicyName $sTCETEETaskPolicy_stateInWorkName $sVPLMCreatorRoleName read,modify,delete,fromconnect,toconnect,fromdisconnect,todisconnect,show]
	set sRet [setPolicyStateUserAccess $sTCETEETaskPolicyName $sTCETEETaskPolicy_stateInWorkName $sVPLMProjectLeaderRoleName read,modify,delete,fromconnect,toconnect,fromdisconnect,todisconnect,show]
	set sRet [setPolicyStateSystemUserAccess $sTCETEETaskPolicyName $sTCETEETaskPolicy_stateDoneName $sOwnerRoleName all]
	set sRet [setPolicyStateSystemUserAccess $sTCETEETaskPolicyName $sTCETEETaskPolicy_stateDoneName $sPublicRoleName read]
	set sRet [setPolicyStateUserAccess $sTCETEETaskPolicyName $sTCETEETaskPolicy_stateDoneName $sVPLMCreatorRoleName read,delete,fromconnect,toconnect,fromdisconnect,todisconnect,show]
	set sRet [setPolicyStateUserAccess $sTCETEETaskPolicyName $sTCETEETaskPolicy_stateDoneName $sVPLMProjectLeaderRoleName read,delete,fromconnect,toconnect,fromdisconnect,todisconnect,show]
	
	return $sRet
}