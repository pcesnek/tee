



proc makeSymbName {aoType aoName} {
	#set sRet "$aoType"+"_"+"$aoName"
	set sRet [format %s%s%s $aoType "_" $aoName]
	regsub -all {[ \r\t\n]+} $sRet "" outline
	return $outline
}

proc registerAdminType {aoType aoName} {

	global APPLICATION
	global INSTALLER
	global VERSION
	global INSTALLED_DATE
	global DATECODE

	set sRet "SUCCESS"
	set aoSymbName [makeSymbName $aoType $aoName]
	
	
	
	if {$aoType == "table"}  {
	
		if [catch {mql add property $aoSymbName on prog eServiceSchemaVariableMapping.tcl to $aoType $aoName system} sErrorMessage] {
			puts "Failed to add property $aoSymbName on prog eServiceSchemaVariableMapping.tcl: $sErrorMessage"
			return -code 1
		}
	
		if [catch {mql add property "original name" on $aoType $aoName system value $aoName} sErrorMessage] {
			puts "Failed to add property original name on $aoType $aoName: $sErrorMessage"
			return -code 1
		}
		
		if [catch {mql add property application on $aoType $aoName system value $APPLICATION} sErrorMessage] {
			puts "Failed to add property application on $aoType $aoName: $sErrorMessage"
			return -code 1
		}
		
		if [catch {mql add property installer on $aoType $aoName system value $INSTALLER} sErrorMessage] {
			puts "Failed to add property installer on $aoType $aoName: $sErrorMessage"
			return -code 1
		}
		
		if [catch {mql add property version on $aoType $aoName system value $VERSION} sErrorMessage] {
			puts "Failed to add property version on $aoType $aoName: $sErrorMessage"
			return -code 1
		}
		if [catch {mql add property "installed date" on $aoType $aoName system value $INSTALLED_DATE} sErrorMessage] {
			puts "Failed to add property installed date on $aoType $aoName: $sErrorMessage"
			return -code 1
		}
		if [catch {mql add property datecode on $aoType $aoName system value $DATECODE} sErrorMessage] {
			puts "Failed to add property datecode date on $aoType $aoName: $sErrorMessage"
			return -code 1
		}
	} else {
		if [catch {mql add property $aoSymbName on prog eServiceSchemaVariableMapping.tcl to $aoType $aoName} sErrorMessage] {
			puts "Failed to add property $aoSymbName on prog eServiceSchemaVariableMapping.tcl: $sErrorMessage"
			return -code 1
		}
	
		if [catch {mql add property "original name" on $aoType $aoName value $aoName} sErrorMessage] {
			puts "Failed to add property original name on $aoType $aoName: $sErrorMessage"
			return -code 1
		}
		
		if [catch {mql add property application on $aoType $aoName value $APPLICATION} sErrorMessage] {
			puts "Failed to add property application on $aoType $aoName: $sErrorMessage"
			return -code 1
		}
		
		if [catch {mql add property installer on $aoType $aoName value $INSTALLER} sErrorMessage] {
			puts "Failed to add property installer on $aoType $aoName: $sErrorMessage"
			return -code 1
		}
		
		if [catch {mql add property version on $aoType $aoName value $VERSION} sErrorMessage] {
			puts "Failed to add property version on $aoType $aoName: $sErrorMessage"
			return -code 1
		}
		if [catch {mql add property "installed date" on $aoType $aoName value $INSTALLED_DATE} sErrorMessage] {
			puts "Failed to add property installed date on $aoType $aoName: $sErrorMessage"
			return -code 1
		}
		if [catch {mql add property datecode on $aoType $aoName value $DATECODE} sErrorMessage] {
			puts "Failed to add property datecode date on $aoType $aoName: $sErrorMessage"
			return -code 1
		}
	}
	
	return $sRet
}

#
#	--- ATTR STUFF ---
#
proc installAttribute {attrName attrType description} {
	set sRet "SUCCESS"
	set exists [AdminTypeExists attribute $attrName]
	if {$exists == 1} { 
		#create attr
		if [catch {mql add attribute $attrName type $attrType description $description} sErrorMessage] {
   	        puts "Failed to add attribute $attrName: $sErrorMessage"
			return -code 1
        }
	} else {
		#mod attr (TODO cannot change attribute type!)
		if [catch {mql mod attribute $attrName description $description} sErrorMessage] {
   	        puts "Failed to modify attribute $attrName: $sErrorMessage"
			return -code 1
		}
	}
	
	set sRet [registerAdminType attribute $attrName]
	
	
	return $sRet
}
proc setDefaultValue {attrName defValue} {
	set sRet "SUCCESS"
	if [catch {mql mod attribute $attrName default $defValue} sErrorMessage] {
   	    puts "Failed to setDefaultValue for attribute $attrName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

proc addAttributeRange {attrName rangeValue} {
	#TODO
	set sRet "SUCCESS"
	if [catch {mql mod attribute $attrName add range = $rangeValue} sErrorMessage] {
   	    puts "Failed to add range value '$rangeValue' for attribute $attrName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

proc addAttributeDimension {attrName dimensionName} {
	#TODO
	set sRet "SUCCESS"
	if [catch {mql mod attribute $attrName add dimension $dimensionName} sErrorMessage] {
   	    puts "Failed to add dimension value '$dimensionName' for attribute $attrName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

proc setAttributeHidden {attrName isHidden} {
	set sRet "SUCCESS"
	if {$isHidden == "TRUE"} {
	    if [catch {mql mod attribute $attrName hidden} sErrorMessage] {
		puts "Failed to modify hidden property for attribute $attrName: $sErrorMessage"
		    return -code 1
    	    }
	} elseif {$isHidden == "FALSE"} {
	    if [catch {mql mod attribute $attrName !hidden} sErrorMessage] {
	       puts "Failed to modify hidden property for attribute $attrName: $sErrorMessage"
	           return -code 1
	    }
	}
	return $sRet
}

proc setAttributeMultiline {attrName isMulitLine} {
	#TODO
	set sRet "SUCCESS"
	return $sRet
}
#
#	--- PRODUCT STUFF ---
#
proc installProduct {productName description title} {
	set sRet "SUCCESS"
	set exists [AdminTypeExists product $productName]
	if {$exists == 1} { 
		#create product
		if [catch {mql add product $productName description $description title $title} sErrorMessage] {
   	        puts "Failed to add product $productName: $sErrorMessage"
			return -code 1
        }        
	} else {
		#mod product
		if [catch {mql mod product $productName description $description title $title} sErrorMessage] {
   	        puts "Failed to modify product $productName: $sErrorMessage"
			return -code 1
		}
	}
	
	if [catch {mql mod product $productName app webclient public} sErrorMessage] {
   	        puts "Failed to set $productName as webclient public: $sErrorMessage"
			return -code 1
        }        

	return $sRet
}
#
#	--- TYPE STUFF ---
#
proc installType {typeName description} {
	set sRet "SUCCESS"
	set exists [AdminTypeExists type $typeName]
	if {$exists == 1} { 
		#create type
		if [catch {mql add type $typeName description $description} sErrorMessage] {
   	        puts "Failed to add type $typeName: $sErrorMessage"
			return -code 1
        }        
	} else {
		#mod type
		if [catch {mql mod type $typeName description $description } sErrorMessage] {
   	        puts "Failed to modify type $typeName: $sErrorMessage"
			return -code 1
		}
	}
		
	set sRet [registerAdminType type $typeName]	
	
	return $sRet
}

proc addAttrToType {typeName attrName} {
	set sRet "SUCCESS"
	
	set attrList [split [mql print type $typeName select attribute dump |] "|"]
	if {[lsearch -exact $attrList $attrName] == -1}	{
		if [catch {mql mod type $typeName add attribute $attrName} sErrorMessage] {
			puts "Failed to add attribute $attrName to type $typeName: $sErrorMessage"
			return -code 1
		}
	}
	return $sRet
}

proc setTypeDerivedFromType {typeName derrivedFromTypeName} {
#TODO
}

#
#	--- RELATIONSHIP STUFF ---
#
proc installRelationship {relName relDescription} {
	set sRet "SUCCESS"
	set exists [AdminTypeExists relationship $relName]
	if {$exists == 1} { 
		#create type
		if [catch {mql add relationship $relName description $relDescription} sErrorMessage] {
   	        puts "Failed to add relationship $relName: $sErrorMessage"
			return -code 1
        }        
	} else {
		#mod type
		if [catch {mql mod relationship $relName description $relDescription } sErrorMessage] {
   	        puts "Failed to mod relationship $relName: $sErrorMessage"
			return -code 1
		}
	}
		
	set sRet [registerAdminType relationship $relName]	
}

proc addAttrToRelationship {relName attrName} {
	set sRet "SUCCESS"
	
	set attrList [split [mql print relationship $relName select attribute dump |] "|"]
	if {[lsearch -exact $attrList $attrName] == -1}	{
		if [catch {mql mod relationship $relName add attribute $attrName} sErrorMessage] {
			puts "Failed to add attribute $attrName to relationship $relName: $sErrorMessage"
			return -code 1
		}
	}
	return $sRet
}

proc addRelationshipFromType {relName fromTypeName} {
	set sRet "SUCCESS"
	
	set fromList [split [mql print relationship $relName select fromtype\[$fromTypeName\] dump |] "|"]
	if {[lsearch -exact $fromList $fromTypeName] == -1}	{
		if [catch {mql mod relationship $relName from add type $fromTypeName} sErrorMessage] {
			puts "Failed to add FROM type $fromTypeName to relationship $relName: $sErrorMessage"
			return -code 1
		}
	}
	return $sRet
}

proc addRelationshipToType {relName toTypeName} {
	set sRet "SUCCESS"
	
	set toList [split [mql print relationship $relName select totype\[$toTypeName\] dump |] "|"]
	if {[lsearch -exact $toList $toTypeName] == -1}	{
		if [catch {mql mod relationship $relName to add type $toTypeName} sErrorMessage] {
			puts "Failed to add TO type $toTypeName to relationship $relName: $sErrorMessage"
			return -code 1
		}
	}
	return $sRet
}

#cardinality can be: 1(=one) | n(=many)
proc setRelationshipFromCardinality {relName cardinality} {
	set sRet "SUCCESS"

	if [catch {mql mod relationship $relName from cardinality $cardinality} sErrorMessage] {
		puts "Failed to set FROM cardinality $cardinality to relationship $relName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

#cardinality can be: 1(=one) | n(=many)
proc setRelationshipToCardinality {relName cardinality} {
	set sRet "SUCCESS"

	if [catch {mql mod relationship $relName to cardinality $cardinality} sErrorMessage] {
		puts "Failed to set TO cardinality $cardinality to relationship $relName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

#behavior can be: none | float | replicate
proc setRelationshipBehavior_FromRevision {relName behavior} {
	set sRet "SUCCESS"

	if [catch {mql mod relationship $relName from revision $behavior} sErrorMessage] {
		puts "Failed to set FROM RevisionBehavior $behavior to relationship $relName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}
#behavior can be: none | float | replicate
proc setRelationshipBehavior_FromClone {relName behavior} {
	set sRet "SUCCESS"

	if [catch {mql mod relationship $relName from clone $behavior} sErrorMessage] {
		puts "Failed to set FROM CloneBehavior $behavior to relationship $relName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}
#behavior can be: none | float | replicate
proc setRelationshipBehavior_ToRevision {relName behavior} {
	set sRet "SUCCESS"

	if [catch {mql mod relationship $relName to revision $behavior} sErrorMessage] {
		puts "Failed to set TO RevisionBehavior $behavior to relationship $relName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}
#behavior can be: none | float | replicate
proc setRelationshipBehavior_ToClone {relName behavior} {
	set sRet "SUCCESS"

	if [catch {mql mod relationship $relName to clone $behavior} sErrorMessage] {
		puts "Failed to set TO CloneBehavior $behavior to relationship $relName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

proc setRelationshipDerivedFromRelationship {relName derrivedFromRelName} {
#TODO
}

#
#	--- POLICY STUFF ---
#
proc installPolicy {policyName policyDescription} {
	set sRet "SUCCESS"
	
	set exists [AdminTypeExists policy $policyName]
	if {$exists == 1} { 
		#create policy
		if [catch {mql add policy $policyName description $policyDescription store STORE} sErrorMessage] {
   	        puts "Failed to add policy $policyName: $sErrorMessage"
			return -code 1
        }
	} else {
		#mod policy
		if [catch {mql mod policy $policyName description $policyDescription store STORE} sErrorMessage] {
   	        puts "Failed to modify policy $policyName: $sErrorMessage"
			return -code 1
		}
	}

			
	set sRet [registerAdminType policy $policyName]	
	
	return $sRet
}

proc setPolicySequence {policyName sequence} {
	set sRet "SUCCESS"
	if [catch {mql mod policy $policyName minorsequence $sequence} sErrorMessage] {
   	    puts "Failed to set policy sequence $sequence to policy $policyName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}


proc addPolicyGovernedType {policyName typeName} {
	set sRet "SUCCESS"
	
	set govTypeList [split [mql print policy $policyName select type dump |] "|"]
	if {[lsearch -exact $govTypeList $typeName] == -1}	{
		if [catch {mql mod policy $policyName add type $typeName} sErrorMessage] {
			puts "Failed to add governed type $typeName to policy $policyName: $sErrorMessage"
			return -code 1
		}
	}
	return $sRet
}

proc addPolicyFormat {policyName formatName} {
	set sRet "SUCCESS"
	
	set defFormatList [split [mql print policy $policyName select format dump |] "|"]
	if {[lsearch -exact $defFormatList $formatName] == -1}	{
		if [catch {mql mod policy $policyName add format $formatName} sErrorMessage] {
   	        puts "Failed to add format $formatName to policy $policyName: $sErrorMessage"
			return -code 1
		}
	}
	return $sRet
}

proc addPolicyState {policyName stateName} {
	set sRet "SUCCESS"
	
	set stateList [split [mql print policy $policyName select state dump |] "|"]
	if {[lsearch -exact $stateList $stateName] == -1}	{
		if [catch {mql mod policy $policyName add state $stateName public none owner none} sErrorMessage] {
   	        puts "Failed to add state $stateName to policy $policyName: $sErrorMessage"
			return -code 1
		}
		
		set stateSymbName [format %s%s%s state "_" $stateName]
		if [catch {mql mod policy $policyName property $stateSymbName value $stateName} sErrorMessage] {
   	        puts "Failed to register $stateName in policy $policyName: $sErrorMessage"
			return -code 1
		}
	}
	return $sRet
}

proc setPolicyStateUserAccess {policyName stateName userName accessNames} {
	set sRet "SUCCESS"
	
	if [catch {mql mod policy $policyName state $stateName add user $userName $accessNames} sErrorMessage] {
		puts "Failed to set access '$accessNames' for user $userName to state $stateName in policy $policyName: $sErrorMessage"
		return -code 1
	}
		
	return $sRet
}

proc setPolicyStateSystemUserAccess {policyName stateName userName accessNames} {
	set sRet "SUCCESS"
	
	if [catch {mql mod policy $policyName state $stateName add $userName $accessNames} sErrorMessage] {
		puts "Failed to set access '$accessNames' for user $userName to state $stateName in policy $policyName: $sErrorMessage"
		return -code 1
	}
		
	return $sRet
}

proc setPolicyAllStateUserAccess {policyName userName accessNames} {
	set sRet "SUCCESS"
	
	if [catch {mql mod policy $policyName add allstate $userName $accessNames} sErrorMessage] {
		puts "Failed to set access '$accessNames' for user $userName to All State Access in policy $policyName: $sErrorMessage"
		return -code 1
	}
		
	return $sRet
}

proc setPolicyStateUserAccessExpression {policyName stateName userName expression} {
	set sRet "SUCCESS"
	
	if [catch {mql mod policy $policyName state $stateName add user $userName filter $expression} sErrorMessage] {
		puts "Failed to set filter expression '$expression' for user $userName to state $stateName in policy $policyName: $sErrorMessage"
		return -code 1
	}
		
	return $sRet
}

#actionName: promote|demote|...
#triggerTimign: check|action|override
#eServiceTriggerProgParamsName: name of the BO (type=eService Trigger Program Parameters)
proc addTriggerToPolicyState {policyName stateName actionName triggerTiming eServiceTriggerProgParamsName} {
	set sRet "SUCCESS"
	
	set sTriggersForPolicyState [mql print policy $policyName select state\[$stateName\].trigger dump]

	set actionTitle [string totitle $actionName]
	set triggerTimingTitle [string totitle $triggerTiming]
	
	append sSearchStr1 $actionTitle $triggerTimingTitle ":"
	append sSearchStr2 $actionTitle $triggerTimingTitle ":emxTriggerManager("
	set iStartIndex [string first $sSearchStr1 $sTriggersForPolicyState]
	
	set eServiceTriggerProgParamsNamesToSet ""
	
	if {$iStartIndex != -1} {
		set iStartIndex [string first $sSearchStr2 $sTriggersForPolicyState]
		
		if {$iStartIndex == -1} {
			#error - the trigger is defined using different program than emxTriggerManager - cannot add trigger!!
			error "Cannot add trigger for action $actionTitle (timing:$triggerTimingTitle) for policy:$policyName and state:$stateName\n=>  there's already registered trigger using different program than emxTriggerManager!!!"
		}
		
		incr iStartIndex [string length $sSearchStr2]
		set iEndIndex [string first ")" $sTriggersForPolicyState $iStartIndex]
		incr iEndIndex -1
		set sExistingEServiceTriggerProgParamsNames [string range $sTriggersForPolicyState $iStartIndex $iEndIndex]
		set iExist [string first $eServiceTriggerProgParamsName $sExistingEServiceTriggerProgParamsNames]
		if {$iExist == -1} {
			append eServiceTriggerProgParamsNamesToSet $sExistingEServiceTriggerProgParamsNames " " $eServiceTriggerProgParamsName
		}
		
	} else {
		set eServiceTriggerProgParamsNamesToSet $eServiceTriggerProgParamsName
	}
	
	if {$eServiceTriggerProgParamsNamesToSet != ""} {
		if [catch {mql modify policy $policyName state $stateName add trigger $actionName $triggerTiming emxTriggerManager input $eServiceTriggerProgParamsNamesToSet} sErrorMessage] {
			puts "Failed to add trigger for action $actionName (timing:$triggerTiming) for policy $policyName and state $stateName: $sErrorMessage"
			return -code 1
		}

	}
	return $sRet
}

#triggerTimign: check|action|override
#eServiceTriggerProgParamsName: name of the BO (type=eService Trigger Program Parameters)
proc addTriggerToRelationship {relationshipName actionName triggerTiming eServiceTriggerProgParamsName} {
	set sRet "SUCCESS"
	
	set sTriggersForRelationship [mql print relationship $relationshipName select trigger dump]
	
	set actionTitle [string totitle $actionName]
	set triggerTimingTitle [string totitle $triggerTiming]
	
	append sSearchStr1 $actionTitle $triggerTimingTitle ":"
	append sSearchStr2 $actionTitle $triggerTimingTitle ":emxTriggerManager("
	set iStartIndex [string first $sSearchStr1 $sTriggersForRelationship]
	
	set eServiceTriggerProgParamsNamesToSet ""
	
	if {$iStartIndex != -1} {
		set iStartIndex [string first $sSearchStr2 $sTriggersForRelationship]
		if {$iStartIndex == -1} {
			#error - the trigger is defined using different program than emxTriggerManager - cannot add trigger!!
			error "Cannot add trigger for action $actionTitle (timing:$triggerTimingTitle) for relationship:$relationshipName \n=>  there's already registered trigger using different program than emxTriggerManager!!!"
		}
		incr iStartIndex [string length $sSearchStr2]
		set iEndIndex [string first ")" $sTriggersForRelationship $iStartIndex]
		incr iEndIndex -1
		set sExistingEServiceTriggerProgParamsNames [string range $sTriggersForRelationship $iStartIndex $iEndIndex]
		set iExist [string first $eServiceTriggerProgParamsName $sExistingEServiceTriggerProgParamsNames]
		if {$iExist == -1} {
			append eServiceTriggerProgParamsNamesToSet $sExistingEServiceTriggerProgParamsNames " " $eServiceTriggerProgParamsName
		}
		
	} else {
		set eServiceTriggerProgParamsNamesToSet $eServiceTriggerProgParamsName
	}
	
	if {$eServiceTriggerProgParamsNamesToSet != ""} {
		if [catch {mql modify relationship $relationshipName add trigger $actionName $triggerTiming emxTriggerManager input $eServiceTriggerProgParamsNamesToSet} sErrorMessage] {
			puts "Failed to add trigger for action $actionName (timing:$triggerTiming) for relationship $relationshipName: $sErrorMessage"
			return -code 1
		}

	}
	return $sRet
}


#
#	--- WEB FORM STUFF ---
#
proc installWebForm {webFormName description } {
	set sRet "SUCCESS"
	set exists [AdminTypeExists form $webFormName]

	if {$exists == 1} { 
		#create command
		if [catch {mql add form $webFormName web description $description} sErrorMessage] {
   	        puts "Failed to add webform $webFormName: $sErrorMessage"
			return -code 1
        }        
	} else {
		#mod command
		if [catch {mql mod form $webFormName description $description} sErrorMessage] {
   	        puts "Failed to modify webform $webFormName: $sErrorMessage"
			return -code 1
		}
	}
		
	set sRet [registerAdminType form $webFormName]
	
	return $sRet
}

# add field to WebForm
proc addFieldToWebForm {webFormName fieldName label} {
	set sRet "SUCCESS"
	
	#check field already exists
	set existName [mql print form $webFormName select field\[$fieldName\].name dump]
	if {$existName == ""} {
		#doesn't exist - create field
		if [catch {mql modify form $webFormName field name $fieldName label $label} sErrorMessage] {
			puts "Failed to add field $fieldName to webform $webFormName: $sErrorMessage"
			return -code 1
		}
	} else {
		#does exist - modify field's label
		if [catch {mql modify form $webFormName field modify name $fieldName label $label} sErrorMessage] {
			puts "Failed to modify field $fieldName for webform $webFormName: $sErrorMessage"
			return -code 1
		}
	}
	
	return $sRet
}

#set an expresssion to field in a Web Form
proc setBusinessObjectExpressionForFieldInWebForm {webFormName fieldName expression} {
	set sRet "SUCCESS"
	if [catch {mql modify form $webFormName field modify name $fieldName businessobject $expression} sErrorMessage] {
   	    puts "Failed to set businessobject expression $expression to field $fieldName in webform $webFormName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

#set a setting to field in a Web Form
proc setSettingForFieldInWebForm {webFormName fieldName setName setValue} {
	set sRet "SUCCESS"
	if [catch {mql modify form $webFormName field modify name $fieldName setting $setName  $setValue} sErrorMessage] {
   	    puts "Failed to set setting $setName(=$setValue) to field $fieldName in webform $webFormName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

#set href to field in a Web Form
proc setHrefForFieldInWebForm {webFormName fieldName url} {
	set sRet "SUCCESS"
	if [catch {mql modify form $webFormName field modify name $fieldName href $url} sErrorMessage] {
   	    puts "Failed to set setting $setName(=$setValue) to field $fieldName in webform $webFormName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

#set user to field in a Web Form
proc setUserForFieldInWebForm {webFormName fieldName user} {
	set sRet "SUCCESS"
	if [catch {mql modify form $webFormName field modify name $fieldName user $user} sErrorMessage] {
   	    puts "Failed to set setting $setName(=$setValue) to field $fieldName in webform $webFormName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

#delete user to field in a Web Form
proc deleteUserForFieldInWebForm {webFormName fieldName user} {
	set sRet "SUCCESS"
	if [catch {mql modify form $webFormName field modify name $fieldName remove user $user} sErrorMessage] {
   	    puts "Failed to delete setting $setName(=$setValue) to field $fieldName in webform $webFormName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}


#set rangeHref to field in a Web Form
proc setRangeHrefForFieldInWebForm {webFormName fieldName url} {
	set sRet "SUCCESS"
	if [catch {mql modify form $webFormName field modify name $fieldName range $url} sErrorMessage] {
   	    puts "Failed to set setting $setName(=$setValue) to field $fieldName in webform $webFormName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

#
#	--- TABLE STUFF ---
#
proc installTable {tableName} {
	set sRet "SUCCESS"
	set exists [AdminTypeExists table $tableName]

	if {$exists == 1} { 
		#create command
		if [catch {mql add table $tableName system} sErrorMessage] {
			puts "Failed to add table $tableName: $sErrorMessage"
			return -code 1
        }        
	} 
		
	set sRet [registerAdminType table $tableName]
	return $sRet
}

proc addColumnToTable {tableName columnName label} {
	set sRet "SUCCESS"
	
	#check field already exists
	set existName [mql print table $tableName system select column\[$columnName\].name dump]
	if {$existName == ""} {
		#doesn't exist - create field
		if [catch {mql modify table $tableName system column name $columnName label $label} sErrorMessage] {
			puts "Failed to add column $columnName to table $tableName: $sErrorMessage"
			return -code 1
		}
	} else {
		#does exist - modify field's label
		if [catch {mql modify table $tableName system column modify name $columnName label $label} sErrorMessage] {
			puts "Failed to modify column $columnName in table $tableName: $sErrorMessage"
			return -code 1
		}
	}
	return $sRet
}

proc setTableColumnHref {tableName columnName href} {
	set sRet "SUCCESS"
	if [catch {mql modify table $tableName system column modify name $columnName href $href} sErrorMessage] {
   	    puts "Failed to set href to column $columnName in table $tableName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

proc setTableColumnAlt {tableName columnName alt} {
	set sRet "SUCCESS"
	if [catch {mql modify table $tableName system column modify name $columnName alt $alt} sErrorMessage] {
   	    puts "Failed to set alt to column $columnName in table $tableName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

proc setTableColumnBusinessObjectExpression {tableName columnName expression} {
	set sRet "SUCCESS"
	if [catch {mql modify table $tableName system column modify name $columnName businessobject $expression} sErrorMessage] {
   	    puts "Failed to set businessobject expression $expression to column $columnName in table $tableName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

proc setTableColumnOrderNumber {tableName columnName orderNumber} {
	set sRet "SUCCESS"
	if [catch {mql modify table $tableName system column modify name $columnName order $orderNumber} sErrorMessage] {
   	    puts "Failed to set order number $orderNumber to column $columnName in table $tableName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

proc setTableColumnRelationshipExpression {tableName columnName expression} {
	set sRet "SUCCESS"
	if [catch {mql modify table $tableName system column modify name $columnName relationship $expression} sErrorMessage] {
   	    puts "Failed to set relationship expression $expression to column $columnName in table $tableName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

proc setTableColumnSetting {tableName columnName settingName settingValue} {
	set sRet "SUCCESS"
	if [catch {mql modify table $tableName system column modify name $columnName setting $settingName  $settingValue} sErrorMessage] {
   	    puts "Failed to set setting  $setName(=$setValue) to column $columnName in table $tableName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

#set user to field in a Web Form
proc setTableUserForField {tableName columnName user} {
	set sRet "SUCCESS"
	if [catch {mql modify table $tableName system column modify name $columnName user $user} sErrorMessage] {
   	    puts "Failed to set setting user(=$user) to field $columnName in table $tableName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

#delete user to field in a Web Form
proc deleteTableUserForField {tableName columnName user} {
	set sRet "SUCCESS"
	if [catch {mql modify table $tableName system column modify name $columnName remove user $user} sErrorMessage] {
   	    puts "Failed to delete setting user(=$user) for field $columnName in table $tableName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}


#
#	--- MENU STUFF ---
#
proc installMenu {menuName description label} {
	set sRet "SUCCESS"
	set exists [AdminTypeExists menu $menuName]

	if {$exists == 1} { 
		#create command
		if [catch {mql add menu $menuName description $description label $label} sErrorMessage] {
   	        puts "Failed to add menu $menuName: $sErrorMessage"
			return -code 1
        }        
	} else {
		#mod command
		if [catch {mql mod menu $menuName description $description label $label} sErrorMessage] {
   	        puts "Failed to modify menu $menuName: $sErrorMessage"
			return -code 1
		}
	}
		
	set sRet [registerAdminType menu $menuName]
	
	return $sRet
}

proc setMenuSetting {menuName setName setValue} {
	set sRet "SUCCESS"
	if [catch {mql mod menu $menuName add setting $setName $setValue} sErrorMessage] {
   	    puts "Failed to set setting  $setName(=$setValue) to menu $menuName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

proc addCommandToMenu {cmdNameToBeAdded menuName} {
	set sRet "SUCCESS"
	
	set cmdList [split [mql print menu $menuName select command dump |] "|"]
	if {[lsearch -exact $cmdList $cmdNameToBeAdded] == -1}	{
		if [catch {mql mod menu $menuName add command $cmdNameToBeAdded} sErrorMessage] {
   	        puts "Failed to add command $cmdNameToBeAdded to menu $menuName: $sErrorMessage"
			return -code 1
		}
	}
	return $sRet
}

proc addMenuToMenu {menuNameToBeAdded menuName} {
	set sRet "SUCCESS"
	
	set menuList [split [mql print menu $menuName select menu dump |] "|"]
	if {[lsearch -exact $menuList $menuNameToBeAdded] == -1}	{
		if [catch {mql mod menu $menuName add menu $menuNameToBeAdded} sErrorMessage] {
   	        puts "Failed to add menu $menuNameToBeAdded to menu $menuName: $sErrorMessage"
			return -code 1
		}
	}
	return $sRet
}

proc setMenuHref {menuName href} {
	set sRet "SUCCESS"
	if [catch {mql mod menu $menuName href $href} sErrorMessage] {
   	    puts "Failed to set href to menu $menuName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

#
#	--- COMMAND STUFF ---
#
proc installCommand {cmdName description label} {
	set sRet "SUCCESS"
	set exists [AdminTypeExists command $cmdName]

	if {$exists == 1} { 
		#create command
		if [catch {mql add command $cmdName description $description label $label} sErrorMessage] {
   	        puts "Failed to add command $cmdName: $sErrorMessage"
			return -code 1
        }        
	} else {
		#mod command
		if [catch {mql mod command $cmdName description $description label $label} sErrorMessage] {
   	        puts "Failed to modify command $cmdName: $sErrorMessage"
			return -code 1
		}
	}
		
	set sRet [registerAdminType command $cmdName]	
	
	return $sRet
}

proc setCommandHref {cmdName href} {
	set sRet "SUCCESS"
	if [catch {mql mod command $cmdName href $href} sErrorMessage] {
   	    puts "Failed to set href to command $cmdName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

proc setCommandAlt {cmdName alt} {
	set sRet "SUCCESS"
	if [catch {mql mod command $cmdName alt $alt} sErrorMessage] {
   	    puts "Failed to alt $alt to command $cmdName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

proc setCommandSetting {cmdName setName setValue} {
	set sRet "SUCCESS"
	if [catch {mql mod command $cmdName add setting $setName $setValue} sErrorMessage] {
   	    puts "Failed to set setting  $setName(=$setValue) command $cmdName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

proc modifyCommandObjectAccessAddUser {cmdName userName} {
	set sRet "SUCCESS"
	if [catch {mql mod command $cmdName add user $userName} sErrorMessage] {
   	    puts "Failed to add user $userName to Object Access for command $cmdName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

#
#	--- PROGRAM STUFF ---
#
proc installJPOProgram {programName description fileName} {
	set sRet "SUCCESS"
	
	set exists [AdminTypeExists program $programName]
	if {$exists == 1} { 
		#create program
		if [catch {mql add program $programName description $description java file $fileName} sErrorMessage] {
			 puts "Failed to add program $programName: $sErrorMessage"
			 return -code 1
			 
		}
	} else {
		#modify program
		if [catch {mql modify program $programName description $description file $fileName} sErrorMessage] {
			 puts "Failed to modify program $programName:  $sErrorMessage"
			 return -code 1
		}
	}
	puts -nonewline "compiling..."
	if [catch {mql compile program $programName} sErrorMessage] {
			puts "Failed to compile program $programName :$sErrorMessage"
			return -code 1
	}
	return $sRet
	
}

#
#	--- PORTAL STUFF ---
#

proc installPortal {portalName description label} {
	set sRet "SUCCESS"
	set exists [AdminTypeExists portal $portalName]

	if {$exists == 1} { 
		#create portal
		if [catch {mql add portal $portalName description $description label $label } sErrorMessage] {
   	        puts "Failed to add portal $portalName: $sErrorMessage"
			return -code 1
        }        
	} else {
		#mod portal
		if [catch {mql mod portal $portalName description $description label $label } sErrorMessage] {
   	        puts "Failed to modify portal $portalName: $sErrorMessage"
			return -code 1
		}
	}
		
	set sRet [registerAdminType portal $portalName]	
	
	return $sRet
}

proc setPortalSetting {portalName setName setValue} {
	set sRet "SUCCESS"
	if [catch {mql mod portal $portalName add setting $setName $setValue} sErrorMessage] {
   	    puts "Failed to set setting  $setName(=$setValue) portal $portalName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

proc addChannelToPortal {portalName channelName afterBefore channelNameBefore} {
	set sRet "SUCCESS"
	
	if [catch {mql modify portal $portalName place $channelName $afterBefore $channelNameBefore} sErrorMessage] {
   	    puts "Failed to add channel $channelName to portal $portalName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

proc addChannelToPortalNewRow {portalName channelName afterBefore channelNameBefore} {
	set sRet "SUCCESS"
	
	if [catch {mql modify portal $portalName place $channelName newrow $afterBefore $channelNameBefore} sErrorMessage] {
   	    puts "Failed to add channel $channelName to portal $portalName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

#
#	--- CHANNEL STUFF ---
#

proc installChannel {channelName description label} {
	set sRet "SUCCESS"
	set exists [AdminTypeExists channel $channelName]

	if {$exists == 1} { 
		#create channel
		if [catch {mql add channel $channelName description $description label $label} sErrorMessage] {
   	        puts "Failed to add channel $channelName: $sErrorMessage"
			return -code 1
        }        
	} else {
		#mod channel
		if [catch {mql mod channel $channelName description $description label $label} sErrorMessage] {
   	        puts "Failed to modify channel $channelName: $sErrorMessage"
			return -code 1
		}
	}
		
	set sRet [registerAdminType channel $channelName]	
	
	return $sRet
}

proc setChannelSetting {channelName setName setValue} {
	set sRet "SUCCESS"
	if [catch {mql mod channel $channelName add setting $setName $setValue} sErrorMessage] {
   	    puts "Failed to set setting  $setName(=$channelName) channel $channelName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

proc addCommandToChannel {channelName commendName afterBefore commandNameAfterBefore} {
	set sRet "SUCCESS"
	
	if [catch {mql modify channel $channelName place $commendName $afterBefore $commandNameAfterBefore} sErrorMessage] {
   	    puts "Failed to add commmand $commendName to channel $channelName: $sErrorMessage"
		return -code 1
	}
	return $sRet
}

#
#	--- ROLE STUFF ---
#
proc installRole {roleName description} {
	set sRet "SUCCESS"
	set exists [AdminTypeExists role $roleName]

	if {$exists == 1} { 
		#create channel
		if [catch {mql add role $roleName description $description} sErrorMessage] {
   	        puts "Failed to add role $roleName: $sErrorMessage"
			return -code 1
        }        
	} else {
		#mod channel
		if [catch {mql mod role $roleName description $description } sErrorMessage] {
   	        puts "Failed to modify role $roleName: $sErrorMessage"
			return -code 1
		}
	}
		
	set sRet [registerAdminType role $roleName]	
	
	return $sRet
}

proc AdminTypeExists {adminType  objName } {

	global existingAdminEntitiesMap
	
	set mappedEntitiesList [array names existingAdminEntitiesMap]	

	if {[lsearch -exact $mappedEntitiesList $adminType] == -1} {
		
		if {$adminType == "table"}  {
			set existingAdminList [split [mql list $adminType system] "\n"]
		} else {
			set existingAdminList [split [mql list $adminType] "\n"]
		}
		set existingAdminEntitiesMap($adminType) $existingAdminList
	} else {
		
		set existingAdminList $existingAdminEntitiesMap($adminType)
	}
	
	set iRet 0
	if {[lsearch -exact $existingAdminList $objName] == -1} {

		# Admin type does not exist
		set iRet 1		
	} else {
		
		# Admin type does exists
		set iRet 0		
	}
	
	return $iRet
}



