set  context user creator vault "eService Production";
trigger off;


tcl;

proc sourceInstallFiles {} {
	#------------------------------------------
	# SOURCE the necessary util files.
	#------------------------------------------
	set sInstallFiles_FileName install_files.tcl
	set sInstallDefines_FileName install_defines.tcl
	set sInstallData_FileName install_data.tcl
	set sInstallUtils_FileName install_utils.tcl
	set sInstallAttribute_FileName inst_attribute.tcl
	set sInstallType_FileName inst_type.tcl	
	set sInstallRelationship_FileName inst_relation.tcl	
	set sInstallPolicy_FileName inst_policy.tcl
	set sInstallCommand_FileName inst_command.tcl	
	set sInstallMenu_FileName inst_menu.tcl	
	set sInstallAdminBO_FileName inst_adminBO.tcl	
	set sInstallWebForm_FileName inst_webform.tcl
	set sInstallTable_FileName inst_table.tcl
	set sInstallProgram_FileName inst_program.tcl
	set sInstallChannel_FileName inst_channel.tcl	
	set sInstallPortal_FileName inst_portal.tcl
	set sInstallProduct_FileName inst_product.tcl
	set sUpdateFiles_FileName update_files.tcl
	set sInstallRoles_FileName inst_role.tcl

	
	if [catch {source $sInstallFiles_FileName} sErrorMessage] {
		puts "problems in sourcing... $sInstallFiles_FileName"
		puts stdout $sErrorMessage
		return
	}
	
	if [catch {source $sInstallProduct_FileName} sErrorMessage] {
		puts "problems in sourcing... $sInstallProduct_FileName"
		puts stdout $sErrorMessage
		return
	}
	
	if [catch {source $sUpdateFiles_FileName} sErrorMessage] {
		puts "problems in sourcing... $sUpdateFiles_FileName"
		puts stdout $sErrorMessage
		return
	}
	
	if [catch {source $sInstallDefines_FileName} sErrorMessage] {
		puts "problems in sourcing... $sInstallDefines_FileName"
		puts stdout $sErrorMessage
		return
	}
	
	if [catch {source $sInstallData_FileName} sErrorMessage] {
		puts "problems in sourcing... $sInstallData_FileName"
		puts stdout $sErrorMessage
		return
	}
	
	if [catch {source $sInstallUtils_FileName} sErrorMessage] {
		puts "problems in sourcing... $sInstallUtils_FileName"
		puts stdout $sErrorMessage
		return
	}

	if [catch {source $sInstallRoles_FileName} sErrorMessage] {
		puts "problems in sourcing... $sInstallRoles_FileName"
		puts stdout $sErrorMessage
		return
	}
	
	if [catch {source $sInstallAttribute_FileName} sErrorMessage] {
		puts "problems in sourcing... $sInstallAttribute_FileName"
		puts stdout $sErrorMessage
		return
	}
	
	 if [catch {source $sInstallType_FileName} sErrorMessage] {
		 puts "problems in sourcing... $sInstallType_FileName"
		 puts stdout $sErrorMessage
		 return
	 }
	
	if [catch {source $sInstallRelationship_FileName} sErrorMessage] {
		puts "problems in sourcing... $sInstallRelationship_FileName"
		puts stdout $sErrorMessage
		return
	}
	
	if [catch {source $sInstallPolicy_FileName} sErrorMessage] {
		puts "problems in sourcing... $sInstallPolicy_FileName"
		puts stdout $sErrorMessage
		return
	}
	
	if [catch {source $sInstallCommand_FileName} sErrorMessage] {
		puts "problems in sourcing... $sInstallCommand_FileName"
		puts stdout $sErrorMessage
		return
	}
	
	if [catch {source $sInstallMenu_FileName} sErrorMessage] {
		puts "problems in sourcing... $sInstallMenu_FileName"
		puts stdout $sErrorMessage
		return
	}
	
	if [catch {source $sInstallAdminBO_FileName} sErrorMessage] {
		puts "problems in sourcing... $sInstallAdminBO_FileName"
		puts stdout $sErrorMessage
		return
	}
	
	if [catch {source $sInstallWebForm_FileName} sErrorMessage] {
		puts "problems in sourcing... $sInstallWebForm_FileName"
		puts stdout $sErrorMessage
		return
	}
	
	if [catch {source $sInstallTable_FileName} sErrorMessage] {
		puts "problems in sourcing... $sInstallTable_FileName"
		puts stdout $sErrorMessage
		return
	}
	
	# if [catch {source $sInstallChannel_FileName} sErrorMessage] {
		# puts "problems in sourcing... $sInstallChannel_FileName"
		# puts stdout $sErrorMessage
		# return
	# }

	# if [catch {source $sInstallPortal_FileName} sErrorMessage] {
		# puts "problems in sourcing... $sInstallPortal_FileName"
		# puts stdout $sErrorMessage
		# return
	# }	

	if [catch {source $sInstallProgram_FileName} sErrorMessage] {
		 puts "problems in sourcing... $sInstallProgram_FileName"
		 puts stdout $sErrorMessage
		 return
	}
}



eval {

	global sEnoviaHome
	global APPLICATION
	global INSTALLER
	global VERSION
	global INSTALLED_DATE
	global DATECODE
	global sEnoviaHome
	
	set sEnoviaHome "c:/enoviaV6R2015x/server"
	
	set APPLICATION TEE
	set INSTALLER TEE
	set VERSION 1.0
	set INSTALLED_DATE [clock format [clock seconds] -format %D]
	set DATECODE 20150215
	
	
	set home [pwd]
	puts "home=$home"
	
	puts -nonewline "Sourcing install files..."
	if [catch {set sRet [sourceInstallFiles] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		
		exit
	}
	puts "Ok."
	
	puts -nonewline "Copying files..."
	if [catch {set sRet [installFiles] } sErrorMessage] {
		 puts "Failed."
		 puts $sErrorMessage
		 mql quit -1
		
		 exit
	 }
	 puts "Ok."
	
	puts -nonewline "Update files..."
	if [catch {set sRet [updateFiles] } sErrorMessage] {
		 puts "Failed."
		 puts $sErrorMessage
		 mql quit -1
		
		 exit
	 }
	 puts "Ok."
	
	puts -nonewline "Starting transaction..."
	if [catch {set sRet [mql start transaction] } sErrorMessage] {
			puts "Data installation Failed!"
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts "Ok."
	
	puts "Installing data..."
	if [catch {set sRet [installData] } sErrorMessage] {
			puts "Data installation Failed!"
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts " Data installation Ok."
	
	puts -nonewline "Commiting transaction..."
	if [catch {set sRet [mql commit transaction]} sErrorMessage] {
			puts "Data installation Failed!"
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts "Ok."
}



