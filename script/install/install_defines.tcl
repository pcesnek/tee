# ------------
# Roles
# ------------
	# existing ones
global sEnoviaAll
set sEnoviaAll "all"
global sOwnerRoleName 
set sOwnerRoleName "Owner"
global sPublicRoleName 
set sPublicRoleName "Public"
global sVPLMCreatorRoleName 
set sVPLMCreatorRoleName "VPLMCreator"
global sVPLMProjectLeaderRoleName 
set sVPLMProjectLeaderRoleName "VPLMProjectLeader"


# ------------
# Product
# ------------
global sProductName
set sProductName "TCE-TEE"
global sProductDesc
set sProductDesc "ENOVIA Lean Engineering"
global sProductTitle
set sProductTitle "ENOVIA Lean Engineering"

# ------------
# Attributes
# ------------
global iTEEMamagedAttrName
set iTEEMamagedAttrName TCETEEManaged
global iTEEOrderAttrName
set iTEEOrderAttrName TCETEEOrder
global sTEEStartDateAttrName
set sTEEStartDateAttrName TCETEEStartDate
global sTEEUserPriorityAttrName
set sTEEUserPriorityAttrName TCETEEUserPriority
global sTEEMilestoneDateAttrName
set sTEEMilestoneDateAttrName TCETEEMilestoneDate


# ----------
# Dimensions
# ----------
global sCurrencyDimensionName 
set sCurrencyDimensionName "Currency"

	#link attributes
		#existing ones
global sQTYLinkAttrName
set sQTYLinkAttrName "Quantity"
global sUnitMeasureLinkAttrName
set sUnitMeasureLinkAttrName "Unit of Measure"
# ------------
# Types
# ------------

	#existing ones
global eServiceObjectGeneratorTypeName
set eServiceObjectGeneratorTypeName "eService Object Generator"
global eServiceNumberGeneratorTypeName
set eServiceNumberGeneratorTypeName "eService Number Generator"
global eServiceTriggerProgramParametersTypeName
set eServiceTriggerProgramParametersTypeName "eService Trigger Program Parameters"
global sPartTypeName
set sPartTypeName "Part"
global sOrganizationTypeName
set sOrganizationTypeName "Organization"
global sAppDefinitionTypeName
set sAppDefinitionTypeName "AppDefinition"
global sWorkspaceTypeName
set sWorkspaceTypeName "Workspace"
global sInboxTaskTypeName
set sInboxTaskTypeName "Inbox Task"
global sDocumentTypeName
set sDocumentTypeName "Document"
global sAllObjectsTypeName
set sAllObjectsTypeName "All"


# ------------
# Relationships
# ------------

	#existing ones
global eServiceNumberGeneratorRelationshipName
set eServiceNumberGeneratorRelationshipName "eService Number Generator"
global sReferenceDocumentRelationshipName
set sReferenceDocumentRelationshipName "Reference Document"
global sRouteNodeRelationshipName
set sRouteNodeRelationshipName "Route Node"

global sTaskDocumentRelationshipName
set sTaskDocumentRelationshipName "TEE Task Relation";

# ------------
# Policies
# ------------

global sTCETEETaskPolicyName
global sTCETEETaskPolicySequencePattern
global sTCETEETaskPolicy_stateToDoName
global sTCETEETaskPolicy_stateInWorkName
global sTCETEETaskPolicy_stateDoneName
set sTCETEETaskPolicyName "TCETEETask"
set sTCETEETaskPolicySequencePattern "1,2,3,..."
set sTCETEETaskPolicy_stateToDoName "ToDo"
set sTCETEETaskPolicy_stateInWorkName "InWork"
set sTCETEETaskPolicy_stateDoneName "Done"
	#existing ones
global sMyApps_PolicyPolicyName
set sMyApps_PolicyPolicyName "MyApps_Policy"

# ------------
# Commands
# ------------
global sTEEApplicationHomeCommandName
set sTEEApplicationHomeCommandName "TCETEEApplicationHome"
global sTranscatSupportCommandName
set sTranscatSupportCommandName "TCETEETranscatSupport"
global sTEEReportCommandName
set sTEEReportCommandName "TCETEEReport"

	#existing ones
global sAEFLifecycleCommandName
set sAEFLifecycleCommandName AEFLifecycle
global sAEFHistoryCommandName
set sAEFHistoryCommandName AEFHistory	
global sAPPWhereUsedCommandName
set sAPPWhereUsedCommandName APPWhereUsed

# ------------
# Menus
# ------------
global sTCETEEMyDeskMenuName
set sTCETEEMyDeskMenuName "TCETEEMyDesk"

	#existing ones
global sTreeMenuName
set sTreeMenuName "Tree"
global sMyDeskMenuName
set sMyDeskMenuName "My Desk"
global sActionsMenuName
set sActionsMenuName Actions
global sAEFGlobalSearchMenuName
set sAEFGlobalSearchMenuName AEFGlobalSearch
global sAEFHelpMenuName
set sAEFHelpMenuName "AEFHelpMenu"
	
# ------------
# Chanel
# ------------

	#existing Chanels


# ------------
# Portal
# ------------

	
# ------------
# Web Forms
# ------------


# ------------
# Tables
# ------------


	#table columns

	#existing tables

# ------------
# Vaults
# ------------
global eServiceAdministrationVaultName
set eServiceAdministrationVaultName "eService Administration"

# ------------
# Stores
# ------------
#???


# ------------
# programs JPO
# ------------
global sTCTEE_UIBase_JPO_Name
set sTCTEE_UIBase_JPO_Name "TC_TEEUIBase"
global sTCTEE_UI_JPO_Name
set sTCTEE_UI_JPO_Name "TC_TEEUI"


	#methods
global sTCTEE_UI_JPO_HasUserAssignedLicense_sMethodName
set sTCTEE_UI_JPO_HasUserAssignedLicense_sMethodName hasUserAssignedLicense

	#existing program names
global sEmxCommonDocumentUI_JPO_Name
set sEmxCommonDocumentUI_JPO_Name emxCommonDocumentUI

	#existing method names
global sEmxCommonDocumentUI_JPO_GetDocumentsMethodName
set sEmxCommonDocumentUI_JPO_GetDocumentsMethodName getDocuments


# ------------
# Trigger - eService Trigger Program Parameters names
# ------------



