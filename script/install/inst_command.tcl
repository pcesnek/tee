proc installAllCommands {} {
	set sRet "SUCCESS"
	
	global sTEEApplicationHomeCommandName
	puts -nonewline " - Installing command $sTEEApplicationHomeCommandName..."
	if [catch {set sRet [installCommandTeeApplicationHome] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts  "Ok."
	
	global sTranscatSupportCommandName
	puts -nonewline " - Installing command $sTranscatSupportCommandName..."
	if [catch {set sRet [installCommandTranscatSupport] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts  "Ok."
	
	global sTEEReportCommandName
	puts -nonewline " - Installing command $sTEEReportCommandName..."
	if [catch {set sRet [installCommandTeeReport] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts  "Ok."

	return sRet
}

proc installCommandTeeApplicationHome {} {
	set sRet "SUCCESS"
	global sTEEApplicationHomeCommandName
	global sTCTEE_UI_JPO_Name
	global sTCTEE_UI_JPO_HasUserAssignedLicense_sMethodName
	
	set sRet [installCommand $sTEEApplicationHomeCommandName "Lean Engineering Application Home" "Lean Enginnering" ]
	
	set sRet [setCommandHref $sTEEApplicationHomeCommandName "\${SUITE_DIR}/teeDashboard.jsp"]
	
	set sRet [setCommandSetting $sTEEApplicationHomeCommandName "Registered Suite" "TCTEE"]
	set sRet [setCommandSetting $sTEEApplicationHomeCommandName "Target Location" "content"]
	set sRet [setCommandSetting $sTEEApplicationHomeCommandName "Access Function" $sTCTEE_UI_JPO_HasUserAssignedLicense_sMethodName]
	set sRet [setCommandSetting $sTEEApplicationHomeCommandName "Access Program" $sTCTEE_UI_JPO_Name]

	
	# global sMPPEngineerRoleName	
	# set sRet [modifyCommandObjectAccessAddUser $sMPPCreateCommandName $sMPPEngineerRoleName]
	
	return $sRet
}
proc installCommandTranscatSupport {} {
	set sRet "SUCCESS"
	global sTranscatSupportCommandName
	
	set sRet [installCommand $sTranscatSupportCommandName "Transcat Support Page" "Transcat Support"]
	
	set sRet [setCommandHref $sTranscatSupportCommandName "https://www.transcat-plm.com/en/support/technical-support.html"]
	
	set sRet [setCommandSetting $sTranscatSupportCommandName "Registered Suite" "TCTEE"]
	set sRet [setCommandSetting $sTranscatSupportCommandName "Target Location" "popup"]
	set sRet [setCommandSetting $sTranscatSupportCommandName "Popup Modal" "false"]
	
	# https://www.transcat-plm.com/en/support/technical-support.html
	
	return $sRet
}

proc installCommandTeeReport {} {
	set sRet "SUCCESS"
	global sTEEReportCommandName
	global sTCTEE_UI_JPO_Name
	global sTCTEE_UI_JPO_HasUserAssignedLicense_sMethodName
	
	set sRet [installCommand $sTEEReportCommandName "Time Spent Report" "Time Spent Report" ]
	
	set sRet [setCommandHref $sTEEReportCommandName "\${SUITE_DIR}/teeReport.jsp"]
	
	set sRet [setCommandSetting $sTEEReportCommandName "Registered Suite" "TCTEE"]
	set sRet [setCommandSetting $sTEEReportCommandName "Target Location" "hiddenFrame"]
	set sRet [setCommandSetting $sTEEApplicationHomeCommandName "Access Function" $sTCTEE_UI_JPO_HasUserAssignedLicense_sMethodName]
	set sRet [setCommandSetting $sTEEApplicationHomeCommandName "Access Program" $sTCTEE_UI_JPO_Name]
	
	# global sMPPEngineerRoleName	
	# set sRet [modifyCommandObjectAccessAddUser $sMPPCreateCommandName $sMPPEngineerRoleName]
	
	return $sRet
}
