proc installAllPrograms {} {
	set sRet "SUCCESS"
	
	global sTCTEE_UIBase_JPO_Name
	puts -nonewline " - Installing program $sTCTEE_UIBase_JPO_Name..."
	if [catch {set sRet [installTCTEE_UIProgram] } sErrorMessage] {
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts  "Ok."
	
	return sRet
}

proc installTCTEE_UIProgram {} {
	set sRet "SUCCESS"
	
	global sTCTEE_UIBase_JPO_Name	
	global sTCTEE_UI_JPO_Name
	
	set sSourceDir [pwd]
	set sSourceDir $sSourceDir/../JPO
	
	append sBaseFileName $sSourceDir / $sTCTEE_UIBase_JPO_Name _mxJPO.java
	set sRet [installJPOProgram $sTCTEE_UIBase_JPO_Name "TC TEE Base Program for show commands" $sBaseFileName] 
	
	append sFileName $sSourceDir / $sTCTEE_UI_JPO_Name _mxJPO.java
	set sRet [installJPOProgram $sTCTEE_UI_JPO_Name "TC TEE Program for show commands" $sFileName] 	
	
	return sRet
}