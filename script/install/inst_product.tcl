proc installProductTEE {} {
	set sRet "SUCCESS"
		
	global sProductName
	global sProductDesc
	global sProductTitle
	
	puts -nonewline " - Installing product $sProductName..."
	if [catch {set sRet [installProduct $sProductName $sProductDesc $sProductTitle] } sErrorMessage] {
		puts "Failed."
		puts $sErrorMessage
		mql quit -1
		exit
	}	
	puts "OK."
	
	return $sRet
}