proc installAllWebForms {} {
	set sRet "SUCCESS"

	# global sCreateMPPWebFormName
	# puts -nonewline " - Installing webform $sCreateMPPWebFormName..."
	# if [catch {set sRet [installCreateProcessPlanWebform] } sErrorMessage] {
		# puts $sErrorMessage
		# mql quit -1
		# exit
	# }
	# puts  "Ok."

}

# proc installCreateProcessPlanWebform {} {
	# set sRet "SUCCESS"
	# global sCreateMPPWebFormName
	# global sTitleAttrName 
	
	# set sRet [installWebForm $sCreateMPPWebFormName "Webform for creating Mfg Process Plan"]
	
	# set symbAttrName [makeSymbName attribute $sTitleAttrName]
	# set sFieldName Title
	# set sRet [addFieldToWebForm $sCreateMPPWebFormName $sFieldName emxFramework.Attribute.Title]
	# set sRet [setBusinessObjectExpressionForFieldInWebForm $sCreateMPPWebFormName $sFieldName "\$<attribute\[$symbAttrName\].value>"]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Editable" true]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Field Type" attribute]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Registered Suite" Components]	
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Required" true]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Admin Type" $symbAttrName]
	
	# global sTCMPP_MPPUI_JPO_Name
	# global sTCMPP_MPPUI_JPO_isTemplateFieldAccessibleInCreateMPPFormMethodName
	# global sSearchMPPTemplatesListTableName
	# global sMPPTemplateTypeName
	# global sMPPTemplatePolicyName
	# global sMPPTemplatePolicy_stateActive
	# set symbTypeName [makeSymbName type $sMPPTemplateTypeName]
	# set symbPolicyName [makeSymbName policy $sMPPTemplatePolicyName]
	# set symbStateName [makeSymbName state $sMPPTemplatePolicy_stateActive]
	# set sFieldName Template
	# set sRet [addFieldToWebForm $sCreateMPPWebFormName $sFieldName TCMPP.WebFrom.Field.MPPTemplate.Label]
	# set sRet [setRangeHrefForFieldInWebForm $sCreateMPPWebFormName $sFieldName "\${COMMON_DIR}/emxFullSearch.jsp?field=TYPES=$symbTypeName:CURRENT=$symbPolicyName.$symbStateName&table=$sSearchMPPTemplatesListTableName&selection=single&submitAction=refreshCaller&showInitialResults=true&massPromoteDemote=false"]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Required" false]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Show Clear Button" true]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Registered Suite" TCMPP]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Access Program" $sTCMPP_MPPUI_JPO_Name]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Access Function" $sTCMPP_MPPUI_JPO_isTemplateFieldAccessibleInCreateMPPFormMethodName]

	# set sFieldName TemplateAvailability
	# global sTemplateAvailabilityAttrName
	# global sTCMPP_MPPUI_JPO_isTemplateAvailabilityFieldAccessibleInCreateMPPFormMethodName
	# set symbAttrName [makeSymbName attribute $sTemplateAvailabilityAttrName]
	# set sRet [addFieldToWebForm $sCreateMPPWebFormName $sFieldName TCMPP.WebFrom.Field.MPPTemplate.TemplateAvailability]
	# set sRet [setBusinessObjectExpressionForFieldInWebForm $sCreateMPPWebFormName $sFieldName "\$<attribute\[$symbAttrName\].value>"]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Editable" true]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Field Type" attribute]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Input Type" radiobutton]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Admin Type" $symbAttrName]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Registered Suite" TCMPP]	
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Access Program" $sTCMPP_MPPUI_JPO_Name]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Access Function" $sTCMPP_MPPUI_JPO_isTemplateAvailabilityFieldAccessibleInCreateMPPFormMethodName]
	
	# set sFieldName Description
	# set sRet [addFieldToWebForm $sCreateMPPWebFormName $sFieldName emxFramework.Basic.Description]
	# set sRet [setBusinessObjectExpressionForFieldInWebForm $sCreateMPPWebFormName $sFieldName "description"]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Editable" true]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Field Type" basic]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Input Type" textarea]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Registered Suite" Components]	
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Admin Type" description]
	
	# global sEffectivityStartDateAttrName
	# global sTCMPP_MPPUI_JPO_isEffectivityDatesFieldAccessibleInCreateMPPFormMethodName
	# set sFieldName EffectivityStartDate
	# set symbAttrName [makeSymbName attribute $sEffectivityStartDateAttrName]
	# set sRet [addFieldToWebForm $sCreateMPPWebFormName $sFieldName TCMPP.WebForm.Field.MPPCreate.EffectivityStartDate.Label]
	# set sRet [setBusinessObjectExpressionForFieldInWebForm $sCreateMPPWebFormName $sFieldName "\$<attribute\[$symbAttrName\].value>"]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Editable" true]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "format" date]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Registered Suite" TCMPP]	
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Field Type" attribute]	
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Admin Type" $symbAttrName]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Required" false]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Access Program" $sTCMPP_MPPUI_JPO_Name]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Access Function" $sTCMPP_MPPUI_JPO_isEffectivityDatesFieldAccessibleInCreateMPPFormMethodName]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Show Clear Button" true]
	
	# global sEffectivityEndDateAttrName
	# set sFieldName EffectivityEndDate
	# set symbAttrName [makeSymbName attribute $sEffectivityEndDateAttrName]
	# set sRet [addFieldToWebForm $sCreateMPPWebFormName $sFieldName TCMPP.WebForm.Field.MPPCreate.EffectivityEndDate.Label]
	# set sRet [setBusinessObjectExpressionForFieldInWebForm $sCreateMPPWebFormName $sFieldName "\$<attribute\[$symbAttrName\].value>"]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Editable" true]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "format" date]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Registered Suite" TCMPP]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Field Type" attribute]	
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Admin Type" $symbAttrName]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Required" false]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Access Program" $sTCMPP_MPPUI_JPO_Name]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Access Function" $sTCMPP_MPPUI_JPO_isEffectivityDatesFieldAccessibleInCreateMPPFormMethodName]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Show Clear Button" true]
	
	# set sFieldName Policy
	# set sRet [addFieldToWebForm $sCreateMPPWebFormName $sFieldName emxEngineeringCentral.Common.Policy]
	# set sRet [setBusinessObjectExpressionForFieldInWebForm $sCreateMPPWebFormName $sFieldName "policy"]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Required" true]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Field Type" basic]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Input Type" combobox]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Admin Type" Policy]
	# set sRet [setSettingForFieldInWebForm $sCreateMPPWebFormName $sFieldName "Registered Suite" EngineeringCentral]	
	
	# return $sRet
# }
