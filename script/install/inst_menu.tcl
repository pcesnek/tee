proc installAllMenus {} {
	set sRet "SUCCESS"
		
	global sAEFHelpMenuName
	puts -nonewline " - Updating menu $sAEFHelpMenuName..."
	if [catch {set sRet [updateHelpMenuForTEE] } sErrorMessage] {
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts  "Ok."
	
	global sTCETEEMyDeskMenuName
	puts -nonewline " - Installing menu $sTCETEEMyDeskMenuName..."
	if [catch {set sRet [installTEEMyDeskMenu] } sErrorMessage] {
		puts $sErrorMessage
		mql quit -1
		exit
	}
	puts  "Ok."

	
	return $sRet
}

proc updateHelpMenuForTEE {} {
	set sRet "SUCCESS"
	global sAEFHelpMenuName
	global sTranscatSupportCommandName

	set sRet [addCommandToMenu $sTranscatSupportCommandName $sAEFHelpMenuName]
	
	return $sRet
}

proc installTEEMyDeskMenu {} {
	set sRet "SUCCESS"
	global sTCETEEMyDeskMenuName
	global sTEEApplicationHomeCommandName
	global sTEEReportCommandName
	global sMyDeskMenuName
	
	set sRet [installMenu $sTCETEEMyDeskMenuName "TEE My Desk Menu" "ENOVIA Lean Enginnering"]
	
	set sRet [setMenuSetting $sTCETEEMyDeskMenuName "Registered Suite" "TCTEE"]
	set sRet [setMenuSetting $sTCETEEMyDeskMenuName "Licensed Product" "ENO_AEF_TP,TCE-TEE"]
	
	set sRet [addCommandToMenu $sTEEApplicationHomeCommandName $sTCETEEMyDeskMenuName]
	set sRet [addCommandToMenu $sTEEReportCommandName $sTCETEEMyDeskMenuName]
	set sRet [addMenuToMenu $sTCETEEMyDeskMenuName $sMyDeskMenuName]
	
	return $sRet
}
