set context user creator;
trigger off;

tcl;

eval {

	# puts -nonewline "Assigning roles to user..."
	# set sRet [mql mod role TCMPP_MPPViewer add assign person ssevcik]
	# set sRet [mql mod role TCMPP_MPPEngineer add assign person mlojka]
	# set sRet [mql mod role TCMPP_MPPEngineer add assign person jguran]
	# set sRet [mql mod role TCMPP_MPPEngineer add assign person 'Test Everything']
	
	# set sRet [mql mod person mlojka assign role 'Senior Manufacturing Engineer']
	# set sRet [mql mod person mlojka assign role 'Manufacturing Engineer']
	# set sRet [mql mod person mlojka assign role 'Organization Manager']
	
	# set sRet [mql mod person jguran assign role 'Senior Manufacturing Engineer']
	# set sRet [mql mod person jguran assign role 'Manufacturing Engineer']
	# set sRet [mql mod person jguran assign role 'Organization Manager']
	# puts "Ok."
	
	puts -nonewline "Adding users to product TCE-TEE..."
	set sRet [mql mod prod "TCE-TEE" add person mlojka]
	set sRet [mql mod prod "TCE-TEE" add person ssevcik]
	set sRet [mql mod prod "TCE-TEE" add person jguran]
	set sRet [mql mod prod "TCE-TEE" add person 'Test Everything']
	puts "Ok."
}