﻿<%@page import="com.matrixone.apps.domain.util.FrameworkException"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@include file = "../emxUICommonAppInclude.inc"%>
<%@page import="org.json.simple.*"%>
<%@page import="java.util.*" %>
<%@page import = "java.util.Map" %>
<%@page import="org.apache.log4j.*"%>
<%@page import="com.transcat.tee.data.*"%>
<%@page import="com.transcat.tee.enovia.*"%>
<%@page import="com.transcat.tee.utils.*"%>
<%@include file = "teeProcess.inc"%>

<%!

private JSONObject convertTaskToJSON(TeeDataTask task, String sRealURL)
{
   	JSONObject jOB = task.getJSONDataObject();
   	
   	//check GANTT dates
   	if (jOB.containsKey("GANTT_START") == false)
   	{
   		System.err.println (" !! WARNING GANTT start date is not set for task "+jOB);
   		Date startDate = new Date();
   	   	Date endDate = new Date(); endDate.setDate(startDate.getDate()+1);
	   	jOB.put ("GANTT_START","/Date("+startDate.getTime()+")/");
	   	jOB.put ("GANTT_END","/Date("+endDate.getTime()+")/");
		jOB.put ("GANTT_PERCENT_COMPLETE",0);
   	}
   	
  	//hardcode for now
   	jOB.put ("GANTT_PARENT_ID",null);
   	jOB.put ("GANTT_SUMMARY",false);
   	jOB.put ("GANTT_EXPANDED",true);
   	
   	if (jOB.containsKey("SELECT_TASK_ICON"))
   	{
   		String selIcon = (String)jOB.get("SELECT_TASK_ICON");
   		//String iconWithPath = "<image src='"+sRealURL+"/common/images/"+selIcon+"'/>";
   		if (selIcon.contains(sRealURL) == false)
   		{
	   		String iconWithPath = sRealURL+"/tee/images/"+selIcon;
	   		jOB.put("SELECT_TASK_ICON",iconWithPath);
   		}
   	}
   	
     return jOB;
}



%>

<%
Logger logger = TEELogger.getUserLogger(context.getUser());
logger.debug("!!Call teeGanttProcess.jsp");


String sRealURL = getRealURL(request);

Enumeration en = request.getParameterNames();
while (en.hasMoreElements()) 
{
 String parameterName = (String) en.nextElement();
 String parameterValue = request.getParameter(parameterName);
 logger.debug("  - "+parameterName+"="+parameterValue);
}


String action = request.getParameter("action");
if(action == null)
	action = "";

if(action.equalsIgnoreCase("readproject"))
{
	DBProject dbProjects = new DBProject(sRealURL);
	 
	JSONArray jarr=new JSONArray();
 
            
    ArrayList<TeeDataProject> listProjects =  dbProjects.getAllProjects(context);
    for(int t=0;t<listProjects.size();t++)
    {
 	   jarr.add(listProjects.get(t).getJSONDataObject());
    }
     
     
     String jString = JSONArray.toJSONString(jarr);
     logger.debug("readproject " + jString);
     PrintWriter output = response.getWriter();
     logger.debug(jString);
     output.println( jString );
     output.close();
}
if(action.equalsIgnoreCase("createproject"))
{
	String models = request.getParameter("models");
	JSONArray inputArray=(JSONArray)JSONValue.parse(models);
	ArrayList<TeeDataProject> input =  new ArrayList<TeeDataProject>();
	for (int i = 0; i < inputArray.size(); i++)
	{
		JSONObject jOB = (JSONObject)inputArray.get(i);	
		TeeDataProject project = TeeDataProject.createFromJSON(jOB);
		input.add(project);
	}
	
	try
	{
	DBProject dbProject = new DBProject(sRealURL);
	TeeDataProject project = dbProject.create(context, request.getSession(), input);
	
	JSONArray jarr = new JSONArray();
	jarr.add(project.getJSONDataObject());
	
    String jString = JSONArray.toJSONString(jarr);
    PrintWriter output = response.getWriter();
    output.println( jString );
    output.close();
	}
    catch (FrameworkException ex)
	{
		JSONObject outputErrorObj= new JSONObject();
		outputErrorObj.put("errors",ex.getMessage());
		String jString = outputErrorObj.toJSONString();
	
		PrintWriter output = response.getWriter();
		output.println( jString );
		output.close();
	}
}
if(action.equalsIgnoreCase("update"))
{
	
	String modelsReqParam = request.getParameter("models");
	String projectId = request.getParameter("projectId");
	
	
	JSONArray inputArray=(JSONArray)JSONValue.parse(modelsReqParam);
	ArrayList<TeeDataTask> inputList =  new ArrayList<TeeDataTask>();
	for (int i = 0; i < inputArray.size(); i++)
	{
		JSONObject jOB = (JSONObject)inputArray.get(i);	
		TeeDataTask task = TeeDataTask.createFromJSON(jOB);
		inputList.add(task);
	}
	
	DBTask dbTask = new DBTask();
	try
	{
		ArrayList<TeeDataTask> updatedTasks = dbTask.updateTasks(context, inputList, projectId);
		
		JSONArray outputArray= new JSONArray();
		for (TeeDataTask updatedTask : updatedTasks)
		{
			JSONObject updatedJObj = convertTaskToJSON(updatedTask, sRealURL);
			outputArray.add(updatedJObj);
		}
		String jString = JSONArray.toJSONString(outputArray);
	
		 PrintWriter output = response.getWriter();
		 output.println( jString );
		 output.close();
	}
	catch (FrameworkException ex)
	{
		JSONObject outputErrorObj= new JSONObject();
		outputErrorObj.put("errors",ex.getMessage());
		String jString = outputErrorObj.toJSONString();
	
		PrintWriter output = response.getWriter();
		output.println( jString );
		output.close();
	}
}
if(action.equalsIgnoreCase("create"))
{
	
	String modelsReqParam = request.getParameter("models");
	String rowSelect = request.getParameter("rowSelect");
	String projectId = request.getParameter("projectId");
	
	
	JSONArray inputArray=(JSONArray)JSONValue.parse(modelsReqParam);
	JSONObject jOB = (JSONObject)inputArray.get(0);
   
	Long orderIDObj = (Long)jOB.get("GANTT_ORDER_ID");
	DBTask dbTask = new DBTask();
	TeeDataTask newTask = dbTask.createNewTaskForProject(context, projectId, orderIDObj.intValue());
	
	
	JSONArray outputArray= new JSONArray();
	JSONObject newJObj = convertTaskToJSON(newTask, sRealURL);
	outputArray.add(newJObj);
	String jString = JSONArray.toJSONString(outputArray);

	 PrintWriter output = response.getWriter();
	 output.println( jString );
	 output.close();
}
if(action.equalsIgnoreCase("destroy"))
{
	
	String modelsReqParam = request.getParameter("models");
	String projectId = request.getParameter("projectId");
	
	
	JSONArray inputArray=(JSONArray)JSONValue.parse(modelsReqParam);
	JSONObject jOB = (JSONObject)inputArray.get(0);
	TeeDataTask task = TeeDataTask.createFromJSON(jOB);
   
	try
	{
		DBTask dbTask = new DBTask();
		dbTask.destroyTask(context, task);
		
		//return empty array - idicate that task was deleted (error handling???)
		JSONArray outputArray= new JSONArray();
		String jString = JSONArray.toJSONString(outputArray);
	
		PrintWriter output = response.getWriter();
		output.println( jString );
		output.close();
	}
	catch (FrameworkException ex)
	{
		JSONObject outputErrorObj= new JSONObject();
		outputErrorObj.put("errors",ex.getMessage());
		String jString = outputErrorObj.toJSONString();
	
		PrintWriter output = response.getWriter();
		output.println( jString );
		output.close();
	}
}

if (action.equalsIgnoreCase("getProjectUsers"))
{
	String projectId = request.getParameter("projectId");
	DBProject dbProject = new DBProject(sRealURL);
	MapList projectMemberList =  dbProject.getProjectMembers(context,projectId);

	String jString = JSONArray.toJSONString(projectMemberList);
    PrintWriter output = response.getWriter();
    logger.debug("getProjectUsers:" + jString);
    output.println( jString );
    output.close();
}

if(action.equalsIgnoreCase("readTasksForProject"))
{
	String projectId = request.getParameter("projectId");	
	
	DBTask dbTask = new DBTask();
	 
	JSONArray jarr=new JSONArray();
 

    ArrayList<TeeDataTask> listTask =  dbTask.getAllTasksForProject(context,projectId);
    for(int t=0;t<listTask.size();t++)
    {
    	TeeDataTask task = listTask.get(t);
    	JSONObject jOB = convertTaskToJSON(task, sRealURL);
    	
 	   	jarr.add(jOB);
    }
     
     
     String jString = JSONArray.toJSONString(jarr);

     PrintWriter output = response.getWriter();
     logger.debug("readGantt:" + jString);
     output.println( jString );
     output.close();

} 

if(action.equalsIgnoreCase("getGanttTableColumns"))
{
	getSchema(GanttTableSettings.class, action, response);
}
if(action.equalsIgnoreCase("getGanttChartSettings"))
{
	getSchema(GanttChartSettings.class, action, response);
}

if(action.equalsIgnoreCase("READDEP"))
{
JSONArray jarr=new JSONArray();

//{"ID":535,"PredecessorID":24,"SuccessorID":26,"Type":1}
		/*JSONObject json = new JSONObject();
        json.put("ID","535");
        json.put("PredecessorID","18");
        json.put("SuccessorID","19");
        json.put("Type","1");           
        jarr.add(json);    */
            
        
        String jString = JSONArray.toJSONString(jarr);
        PrintWriter output = response.getWriter();
        output.println( jString );
        output.close();
}
            
%>

