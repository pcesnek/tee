﻿<%@page contentType="text/html; charset=UTF-8"%>
<%@include file = "../emxUICommonAppInclude.inc"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="java.util.*" %>
<%@page import = "java.util.Map" %>
<%@page import="com.transcat.tee.data.*"%>
<%@page import="com.transcat.tee.enovia.*"%>
<%@page import="com.matrixone.apps.domain.*"%>
<%@page import="com.matrixone.apps.domain.util.*"%>
<%@page import="com.transcat.tee.utils.*"%>
<%@page import="org.apache.log4j.*"%>
<%@include file = "teeProcess.inc"%>

<!DOCTYPE html>
<head>
    <!-- <title>TEE Add Content</title> -->
    <!-- Common Kendo UI CSS -->
    <link href="./kendo/styles/kendo.common.min.css" rel="stylesheet" />

    <!-- Default Kendo UI theme CSS -->
    <link href="./kendo/styles/kendo.uniform.min.css" rel="stylesheet" />

    <!-- (optional) Kendo UI DataViz CSS, include only if you will use the data visualisation features -->
    <link href="./kendo/styles/kendo.dataviz.uniform.min.css" rel="stylesheet" />
    
    <!-- TEE Custom CSS -->
    <link href="./styles/tee_main.css" rel="stylesheet" />

    <!-- jQuery JavaScript -->
    <script src="./kendo/scripts/jquery.min.js"></script>
    
    <!-- jQuery cookie JavaScript -->
    <script src="./kendo/scripts/jquery.cookie.js"></script>

    <!-- Kendo UI combined JavaScript -->
    <script src="./kendo/scripts/kendo.all.min.js"></script>
		
</head>
<body>
<%
Logger logger = TEELogger.getUserLogger(context.getUser());
logger.debug("Call teeAddContentDlg.jsp");

String folderName = request.getParameter("foldername");	
String filesName = request.getParameter("filesname");

logger.debug("Call teeAddContentDlg.jsp");
logger.debug("Folder Name:" + folderName);
logger.debug("Files name:" + filesName);

//BusinessType.getBusinessTypes(context);
BusinessTypeList listChildres = new BusinessTypeList();
BusinessTypeList listAllTypes = new BusinessTypeList();

//BusinessType typeDocs = new BusinessType("DOCUMENTS");
//typeDocs.open(context);
//logger.debug("Type name:" + typeDocs.getChildren(context,""));
try
{
	Vault vault = new Vault("eService Production");
	BusinessType typeDocs = new BusinessType("DOCUMENTS",vault );
	typeDocs.open(context);
	listAllTypes = typeDocs.getChildren(context,"");
	
	ArrayList<String> supportedTypes = TeeSettings.getProjectContentSettings().getDragSupportedtypes();
	for(int st=0;st < supportedTypes.size();st++)
	{	
		for(int i=0;i<listAllTypes.size();i++)
		{
			BusinessType type = (BusinessType)listAllTypes.get(i);
			String typeName = type.getName();
			
			if(supportedTypes.get(st).equalsIgnoreCase(typeName))
			{
				listChildres.add(type);
				PolicyList listPolicies = type.getPolicies(context);
				for(int j=0;j<listPolicies.size();j++)
				{
					Policy policy = (Policy)listPolicies.get(j);
					logger.debug("Policies name:" + policy.getName());
				}
				
		    	StringList listAutonames = EnoviaUtil.getAutoNameSeries(context, type.getName());
		    	for(int n=0;n<listAutonames.size();n++)	    		
		    	{	    		
		    		logger.debug("Auto names:"+listAutonames.get(n));
		    	}
			}
		}		
	}	
}
catch(Exception exc)
{
}

%>
</br>
<div id="checkboxonedocwraper" style="margin: 10px;"> <input type="checkbox" id="checkboxonedoc" > Add all files to one document</div>
</br>
<hr>
<table id="addContentTable" style="width:100%;margin: 10px;">
    <tbody id="addContentTableBody">    
    <!--  <tr><th>File</th><th>Auto Name</th><th>name</th><th>Use</th><th>Type</th><th>Policy</th><tr> -->
    </tbody>
</table>
</br>
<div style="width: 25%; margin: 0px auto;">
<button type="button" id="okbtn" onclick="addFilesToContent()" >Add content to folder <%= folderName %></button>
<div id="progressBar" style="height: 20px;width: 180px;"></div>
<!-- <button type="button" id="cancelbtn" onclick="cancel()" >Close</button> -->
</div>

<script>

	var filesStr = "<%= filesName %>";
	var files = filesStr.split(",");
	console.log("Files:"+filesStr);
	var filesCount = 0;
	
	var allFilesStr = "<label class=\"k-label\" id=\"file-all\" >";
	for(var i=0;i<files.length;i++)
  	{
		var file = files[i];
		if(file != "")
		{	
			filesCount ++;			
			if(i == files.length-1)
				allFilesStr += getImageForFile(file) + file;
			else
				allFilesStr +=  getImageForFile(file) +  file+ "</br>";
		}
  	}
	allFilesStr += "</label>";
	
	
	for(var i=0;i<files.length;i++)
  	{
		var file = files[i];
		if(file != "")
		{
			if(i == 0)				
				$("#addContentTableBody").append("<tr id=\"row-"+i+"\"><td>"+allFilesStr+"<label class=\"k-label\" id=\"file-"+i+"\">" + getImageForFile(file) + file + "</label></td><td>use AutoName  <input type=\"checkbox\" id=\"checkbox-"+i+"\" checked=\"checked\"></td><td><input id=\"autoname-"+i+"\" class=\"contentAutoName\"/><input id=\"nametextbox-"+i+"\" /></td><td><input id=\"type-"+i+"\" class=\"contentType\" /></td><td><input id=\"policy-"+i+"\" class=\"contentPolicy\"/></td></tr>");
			else
				$("#addContentTableBody").append("<tr id=\"row-"+i+"\"><td><label class=\"k-label\" id=\"file-"+i+"\">" + getImageForFile(file) + file + "</label></td><td>use AutoName  <input type=\"checkbox\" id=\"checkbox-"+i+"\" checked=\"checked\"></td><td><input id=\"autoname-"+i+"\" class=\"contentAutoName\"/><input id=\"nametextbox-"+i+"\" /></td><td><input id=\"type-"+i+"\" class=\"contentType\" /></td><td><input id=\"policy-"+i+"\" class=\"contentPolicy\"/></td></tr>");
		}
  	}
	for(var i=0;i<files.length;i++)
  	{
		var file = files[i];	
		if(file != "")
		{
            $("#nametextbox-"+i).kendoMaskedTextBox({
            	value: file
            });
            $("#nametextbox-"+i).data("kendoMaskedTextBox").wrapper.hide();
			
			$("#type-"+i).kendoComboBox({
			    dataTextField: "parentName",
			    dataValueField: "parentId",
			    dataSource: [
			        <%
			    	for(int i=0;i<listChildres.size();i++)
			    	{
			    		BusinessType type = (BusinessType)listChildres.get(i);    		
			    		if(type.isAbstract(context))
			    			continue;
			
			    		String typeName = type.getName();
			    		%>
			    		{ parentName:"<%= typeName %>", parentId: <%= i %> } ,
			    		<%
			    	}
			        %>
			    ]
			});
			$("#policy-"+i).kendoComboBox({
			    cascadeFrom: "type-"+i,
			    dataTextField: "policyName",
			    dataValueField: "policyId",
			    dataSource: [
			                 <%
			              	for(int i=0;i<listChildres.size();i++)
			              	{
			              		BusinessType type = (BusinessType)listChildres.get(i);    		
			              		if(type.isAbstract(context))
			              			continue;
			            		
			            		PolicyList listPolicies = type.getPolicies(context);
			            		for(int j=0;j<listPolicies.size();j++)
			            		{
			            			Policy policy = (Policy)listPolicies.get(j);
			            			logger.debug("Policies name:" + policy.getName());
			            			String policyName = policy.getName();
			                		%>
			                		{ policyName:"<%= policyName %>", policyId: <%= j %> , parentId: <%= i %> } ,
			                		<%            			
			            		}            		
			              	}
			                  %>
			    ]
			});			
			
			$("#autoname-"+i).kendoComboBox({
				cascadeFrom: "type-"+i,
			    dataTextField: "autoName",
			    dataValueField: "autoId",
			    dataSource: [
			                 <%
			              	for(int i=0;i<listChildres.size();i++)
			              	{
			              		BusinessType type = (BusinessType)listChildres.get(i);    		
			              		if(type.isAbstract(context))
			              			continue;

			            		logger.debug("AUTO NAMES");
			                	StringList listAutonames = EnoviaUtil.getAutoNameSeries(context, type.getName());
			                	for(int n=0;n<listAutonames.size();n++)	    		
			                	{	    		
			                		logger.debug("Auto names:"+listAutonames.get(n));
			                		String autoName = (String)listAutonames.get(n);
		                		
			                		%>
			                		{ autoName:"<%= autoName %>", autoId: <%= n %> , parentId: <%= i %>},
			                		<%  
			                	}        		
			              	}
			                  %>
			    ]
			});
			$("#file-all").hide();
			$("#file-0").show();			
			
			$("#checkboxonedoc").change(function() {
				for(var i=1;i<files.length;i++)
			  	{
					if(this.checked)
						$("#row-"+i).hide();
					else
						$("#row-"+i).show();
			  	}
				if(this.checked)
				{
					$("#file-all").show();
					$("#file-0").hide();
				}
				else
				{
					$("#file-all").hide();
					$("#file-0").show();
				}
			});
			$("#checkbox-"+i).change(function() {				
				var checkBoxID = this.id;
				var numberID = checkBoxID.substr(9); 
			    if(this.checked)
				{
			    	$("#autoname-"+numberID).data("kendoComboBox").wrapper.show("slow");
			    	$("#nametextbox-"+numberID).data("kendoMaskedTextBox").wrapper.hide("slow");
				}
				else
				{					
			    	$("#autoname-"+numberID).data("kendoComboBox").wrapper.hide("slow");
			    	$("#nametextbox-"+numberID).data("kendoMaskedTextBox").wrapper.show("slow");
				}
			});
			
			var comboboxType =  $("#type-"+i).data("kendoComboBox");
			comboboxType.select(comboboxType.ul.children().eq(0));
	
			var comboboxPolicy =  $("#policy-"+i).data("kendoComboBox");
			comboboxPolicy.select(comboboxPolicy.ul.children().eq(0));
			
			var comboboxAutoname =  $("#autoname-"+i).data("kendoComboBox");
			comboboxAutoname.select(comboboxAutoname.ul.children().eq(0));			
		}
  	}
	if(filesCount == 1)
		$("#checkboxonedocwraper").hide();
		
	
	$(".k-checkbox").css("opacity",1);

	$("#okbtn").kendoButton({
	    icon: "tick"
	});
	$("#cancelbtn").kendoButton({
	    icon: "cancel"
	});	  
	if (typeof String.prototype.endsWith !== 'function') {
	    String.prototype.endsWith = function(suffix) {
	        return this.indexOf(suffix, this.length - suffix.length) !== -1;
	    };
	}
	function getImageForFile(file)
	{
		var imageStr = "";
		var imageFile = "dragfile.png";
		
		var teeImagePath = location.protocol +"//"+ location.host + window.location.pathname.substring(0, window.location.pathname.indexOf("/",2)) +"/tee/images/";
		//return str.indexOf(suffix, str.length - suffix.length) !== -1;
		if(file.indexOf(".xls") != -1)
			imageFile = "excel-icon.png";
		if(file.indexOf(".png") != -1 || file.indexOf(".jpg") != -1 || file.indexOf(".tiff") != -1 || file.indexOf(".jpeg") != -1 || file.indexOf(".bmp") != -1)
			imageFile = "image-icon.png";
		if(file.indexOf(".dll") != -1 || file.indexOf(".exe") != -1 || file.indexOf(".obj") != -1 )
			imageFile = "file-exe-icon.png";
		if(file.indexOf(".pdf") != -1)
			imageFile = "pdf-icon.png";
		if(file.indexOf(".ppt") != -1)
			imageFile = "ppt-icon.png";
		if(file.indexOf(".txt") != -1  || file.indexOf(".ini") != -1 || file.indexOf(".cfg") != -1)
			imageFile = "text-icon.png";
		if(file.indexOf(".word") != -1)
			imageFile = "word-icon.png";
		if(file.indexOf(".prt") != -1)
			imageFile = "cad-icon.png";		
		
		
		imageStr = "<img src=\"" + teeImagePath + imageFile +  "\"> ";
		 
		 return(imageStr);
	}
	function updateProgressBar(percent)
	{
		$("#progressBar").data("kendoProgressBar").value(percent);
	}
	function showProgressBar()
	{
		var pb = $("#progressBar").kendoProgressBar({
	        min: 0,
	        value:false, //false Set setIndeterminate
	        max: 100,
	        type: "value",
	        animation: {
	            duration: 100
	        }
	    }).data("kendoProgressBar");
	   // pb.wrapper.hide();
	    
		$("#okbtn").data("kendoButton").wrapper.hide();
		$("#progressBar").data("kendoProgressBar").wrapper.show();
	}
	
	function addFilesToContent()
	{	    
		showProgressBar();
		var filesNameTypePolicy = "";
		for(var i=0;i<files.length;i++)
	  	{
			var file = files[i];	
			if(file != "")
			{
				var comboboxType = $("#type-"+i).data("kendoComboBox");
				var selectedTypeName = comboboxType.dataItem().parentName;
				
				var comboboxPolicy = $("#policy-"+i).data("kendoComboBox");
				var selectedPolicyName = comboboxPolicy.dataItem().policyName;	
				
				var comboboxAutoname = $("#autoname-"+i).data("kendoComboBox");
				var selectedAutoName = comboboxAutoname.dataItem().autoName;					

				var name = $("#nametextbox-"+i).val();
				
				var useAuto = "0"; 
				if ($("#checkbox-"+i).is(":checked"))
					useAuto = "1";
				
				if ($("#checkboxonedoc").is(":checked"))
				{
					if(i == 0)
						filesNameTypePolicy += file+","+ useAuto + "," + name + "," +selectedAutoName +"," + selectedTypeName+","+selectedPolicyName+"|";
					else
						filesNameTypePolicy = file+":"+filesNameTypePolicy;
				}
				else
				{
					filesNameTypePolicy += file+","+ useAuto + "," + name + "," +selectedAutoName +"," + selectedTypeName+","+selectedPolicyName+"|";
				}				
			}
	  	}

		//alert(filesNameTypePolicy);
		this.parent.addFilesToFolder(filesNameTypePolicy);
	}	
	function cancel()
	{
		this.parent.closeAddContentWindow();
	}
		
	
	</script>
</body>
</html>