function getDataSourceKanban(ident)
{
		var schema = {
			    data: "",
			    model: {}
			};
			var order = "";		
			if(ident == "Done")
				order = "Actual Completion Date";
			else
				order = "TCETEEUserPriority";
		    
			var ds = new kendo.data.DataSource({
			    schema: schema,
			    //filter: { field: "Title", operator: "eq", value: getStateForKanban() },kanBanColumn
			    //filter: { field: "kanBanColumn", operator: "eq", value: filter },
			    transport: {
			        read: { 
			        	url: location.protocol +"//"+ location.host + window.location.pathname.substring(0, window.location.pathname.indexOf("/",2)) +"/tee/teeKanbanProcess.jsp?action=readkanban&ident="+ident+"&order="+order,
			            dataType: "json",
			            cache: false
			        }	    
			    },
			    error: function() { console.log(arguments); }
			});
			console.log("Kanban:"+ds);
			return(ds);
}

function getKanbanSettings(action)
{
	var retVal = null;
	$.ajax({
		url : location.protocol +"//" + location.host + window.location.pathname.substring(0, window.location.pathname.indexOf("/",2)) +"/tee/teeKanbanProcess.jsp?action=" + action,
		cache : false,
		async : false,
		type : "POST",
		contentType : "application/json;charset=utf-8",
		success : function(response) 
		{
			retVal = JSON.parse(response);
		}
	});
	return retVal;
}
