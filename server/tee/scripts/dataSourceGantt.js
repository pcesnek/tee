function getContextPathGantt() {
	return window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));
}

function getGanttTableColumns()
{
	var retVal = null;
	$.ajax({
    	url: location.protocol +"//"+ location.host + getContextPathGantt() +"/tee/teeGanttProcess.jsp?action=getGanttTableColumns",
	    cache: false,
	    async: false,
	    type: "POST",
	    contentType: "application/json;charset=utf-8",
	    success: function (response) 
	    {
            retVal = response;
        }
	});
	return retVal;
}

function getGanttChartSettings()
{
	var retVal = null;
	$.ajax({
    	url: location.protocol +"//"+ location.host + getContextPathGantt() +"/tee/teeGanttProcess.jsp?action=getGanttChartSettings",
	    cache: false,
	    async: false,
	    type: "POST",
	    contentType: "application/json;charset=utf-8",
	    success: function (response) 
	    {
            retVal = response;
        }
	});
	return retVal;
}
	
function getProjectUsers(projectID)
{
	var retVal = null;
	$.ajax({
    	url: location.protocol +"//"+ location.host + getContextPathGantt() +"/tee/teeGanttProcess.jsp?action=getProjectUsers&projectId="+projectID,
	    cache: false,
	    async: false,
	    type: "POST",
	    contentType: "application/json;charset=utf-8",
	    success: function (response) 
	    {
            retVal = response;
        }
	});
	return retVal;
}
	
	
function getEmptyDataSourceForGantt()
{
	emptyDataSource = new kendo.data.GanttDataSource({
		  data: [
		  ]
		});
	return emptyDataSource;
}
	
function refreshWholeGanttForCurrentProject()
{
	var prjsCBox = $("#projectsCBox").data("kendoComboBox");
	//var selectedProjectID = prjsCBox.dataItem().SELECT_ID;
	var selectedProjectID = prjsCBox.dataItem().id;
	var ganttDataSource = getGanttTasksForSelectedProject(selectedProjectID,ganttColumnsObj);
	var kendoGanttInstance =  $("#gantt").data("kendoGantt");
	kendoGanttInstance.setDataSource(getEmptyDataSourceForGantt());
	
	kendoGanttInstance.setDataSource(ganttDataSource);
}
	
function getGanttTasksForSelectedProject(projectID, ganttTableColumns)
{
	var dataModel = {
            id: "id",
            fields: {
              id: { from: "GANTT_ID", type: "number" },
              orderId: { from: "GANTT_ORDER_ID", type: "number"},
              parentId: { from: "GANTT_PARENT_ID", type: "number"},
              start: { from: "GANTT_START", type: "date" },
              end: { from: "GANTT_END", type: "date" },
              title: { from: "GANTT_TITLE", defaultValue: "", type: "string" },
              percentComplete: { from: "GANTT_PERCENT_COMPLETE", type: "number" },
              summary: { from: "GANTT_SUMMARY" },
              expanded: { from: "GANTT_EXPANDED" }
              
            }
          };
	for (var i = 0; i < ganttTableColumns.length; i++) 
	{
	    var colObj = ganttTableColumns[i];
	    if (colObj["editable"] == true)
		{
	    	var fieldName = colObj["field"];
	    	if (fieldName == "SELECT_DUE_DATE_OFFSET")
    		{
	    		//dataModel.fields[fieldName] = {from: fieldName, type: "number", nullable: false,  validation: { min: 1, format:"n0",decimals:0 }};
	    		dataModel.fields[fieldName] = 
	    		{
	    			from: fieldName, 
	    			type: "string", 
	    			nullable: false
	    		}
    		}
	    	else
	    	{
	    		dataModel.fields[fieldName] = {from: fieldName, type: "string"};
	    	}
		}
	}
	dataModel.fields["SELECT_MILESTONE_DATE"] = {from: "SELECT_MILESTONE_DATE", type: "date", format:"yyyy/MM/dd@hh:mm:ss:tt"};
	dataModel.fields["SELECT_IS_MILESTONE"] = {from: "SELECT_IS_MILESTONE", type:"boolean"};
	dataModel.fields["SELECT_IS_IN_CRITICAL_CHAIN"] = {from: "SELECT_IS_IN_CRITICAL_CHAIN", type:"boolean"};
	
	
    var tasksDataSource = new kendo.data.GanttDataSource({
    	 schema: {
             model: dataModel,
             },
    	error: function(e)
    	{
    		console.log ("getGanttTasksForSelectedProject error e",e);
    		console.log ("e.errors",e.errors);
    		e.sender.cancelChanges();
    		if (e.errors)
    		{//custom TEE error
    			alert ("Error: "+e.errors);
    		}
    		else
    		{//unexpected server error (connection error, JSP compile error...)
    			alert (e.status+": "+e.errorThrown);
    		}
    	},
    	change: function(e)
    	{
    		//console.log ("gantt datasource change event",e);
    	},
    	sync: function(e)
    	{
    		console.log ("gantt datasource sync event",e);
    		refreshWholeGanttForCurrentProject();
    		
    		
    		if ( $.isFunction($.fn.updateKanbanList) ) 
			{
				console.log ("Call update kanban on gantt changes");
				$.fn.updateKanbanList(0);
			}
    	},
    	requestEnd: function(e)
    	{
    		//console.log ("gantt datasource requestEnd event",e);
    	},
        batch: true,
        transport: {
            read: {
                url: location.protocol +"//"+ location.host + getContextPathGantt() +"/tee/teeGanttProcess.jsp?action=readTasksForProject&projectId="+projectID,
                dataType: "json",
                type: "POST"
            },
            update: {
            	url: location.protocol +"//"+ location.host + getContextPathGantt() +"/tee/teeGanttProcess.jsp?action=update&projectId="+projectID,
                dataType: "json",
                type: "POST"
            },
            destroy: {
            	url: location.protocol +"//"+ location.host + getContextPathGantt() +"/tee/teeGanttProcess.jsp?action=destroy&projectId="+projectID,
                dataType: "json",
                type: "POST"
            },
            create: {
            	url: location.protocol +"//"+ location.host + getContextPathGantt() +"/tee/teeGanttProcess.jsp?action=create&projectId="+projectID,
                dataType: "json",
                type: "POST"
            },
            parameterMap: function(options, operation) {
                if (operation !== "read") {
                	 var selRow = $("#gantt").data("kendoGantt").select();
                	// console.log ("selRow",selRow);
                    var retVal = { models: kendo.stringify(options.models || [options]),
                    		rowSelect:selRow.length};
                   // console.log ("retVal",retVal);
                	 return retVal;
                }
            }
            
        }
       
        
    });
    return(tasksDataSource);
}
	
	
function getDataSourceGanttDependencies(projectID)
{
    var dependenciesDataSourceDep = new kendo.data.GanttDependencyDataSource({
        transport: {
            read: {
                url: location.protocol +"//"+ location.host + getContextPathGantt() +"/tee/teeGanttProcess.jsp?action=readdep&projectId="+projectID,
                dataType: "json",
                type: "POST"
            },
            update: {
            	url: location.protocol +"//"+ location.host + getContextPathGantt() +"/tee/teeGanttProcess.jsp?action=updatedep",
                dataType: "json",
                type: "POST"
            },
            destroy: {
            	url: location.protocol +"//"+ location.host + getContextPathGantt() +"/tee/teeGanttProcess.jsp?action=destroydep",
                dataType: "json",
                type: "POST"
            },
            create: {
            	url: location.protocol +"//"+ location.host + getContextPathGantt() +"/tee/teeGanttProcess.jsp?action=createdep",
                dataType: "json",
                type: "POST"
            },
            parameterMap: function(options, operation) {
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                }
            }
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { from: "ID", type: "number" },
                    predecessorId: { from: "PredecessorID", type: "number" },
                    successorId: { from: "SuccessorID", type: "number" },
                    type: { from: "Type", type: "number" }
                }
            }
        }
    });		
    return(dependenciesDataSourceDep);
}

function getDataSourceProject() {
	var dataSource = new kendo.data.DataSource({
		transport : {
			read : {
				url : location.protocol +"//" + location.host +  getContextPathGantt() +"/tee/teeGanttProcess.jsp?action=readproject",
				dataType : "json",
				type : "POST"
			},
			create : {
				url : location.protocol +"//" + location.host +  getContextPathGantt() +"/tee/teeGanttProcess.jsp?action=createproject",
				dataType : "json",
				type : "POST"
			},
			parameterMap : function(options, operation) {
				if (operation !== "read" && options.models) {
					return {
						models : kendo.stringify(options.models)
					};
				}
			}
		},
		batch : true,
		pageSize : 20,
		schema : {
			model : {
				id: "id",
				fields : {
					id: { from: "SELECT_ID", type: "string" },
					SELECT_NAME : {
						type : "string",
						validation : {
							required : true
						}
					}
				}
			}
		},
		requestEnd: function(e) {
		    if (e.type == "create" && e.response.length > 0) {
				console.log("Project Created Data RESPONSE: Id: " + e.response[0].SELECT_ID + ", Name: " + e.response[0].SELECT_NAME);
				// check for error
				if (e.response[0].error != null && e.response[0].error != "") {
					console.log("Project Created Data RESPONSE: error: " + e.response[0].error);
					// display error message
					kendo.ui.progress($('#gantt'), false);
					e.sender.cancelChanges();
					alert (e.response[0].error);
					return;
				}
					
            	responseProjectID = e.response[0].SELECT_ID;
				e.sender.read();
		    }
		},
		sync: function(e) {
       	    var item = e.sender.get(responseProjectID);
       	    
       	    if (item && item != null) {
       	    	console.log("Project Created Data SYNC: ", item);
           	    // refresh combobox
            	var projectsCombobox =  $("#projectsCBox").data("kendoComboBox");
            	projectsCombobox.setDataSource(e.sender);
       	    }
       	    
        	// close addprojectForm window
			$("#addprojectForm").data("kendoWindow").close();
		}
	});
	
	return dataSource;
}
	