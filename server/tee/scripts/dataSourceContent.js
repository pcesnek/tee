function getDataSourceContent(projectID)
{
	var ds = new kendo.data.TreeListDataSource({
		transport : {
			read : {
				async:false,
				url : location.protocol +"//" + location.host + window.location.pathname.substring(0, window.location.pathname.indexOf("/",2)) +"/tee/teeContentProcess.jsp?action=readcontent&projectId=" + projectID,
				dataType : "json"
				//jsonpCallbackString: "callback"
			}
		}/*,
		schema : {
			model : {
				id: "id",
				fields: {
					id: { field: "id", type: "string"}
					parentId: { field: "parentId", type: "string"}
				},
				expanded : true
			}
		}*/
	});
	return (ds);
}

function checkBrowser()
{
	if(navigator.userAgent.indexOf("Chrome") != -1 ) 
    {
        alert('Chrome');
    }
    else if(navigator.userAgent.indexOf("Firefox") != -1 ) 
    {
         alert('Firefox');
    }
}
function checkBrowserForPdfPlugin()
{
	var bFound = false;
	var pluginArray = navigator.plugins;
	for(var p=0;p<pluginArray.length;p++)
  	{
		var plugin = pluginArray[p];
		if(plugin.name == "Chrome PDF Viewer")
		{
			bFound = true;
		}
	}
	if(!bFound)
	{
		alert("Install PDF viewer");
	}
}

function saveExpanded(data) {
	if (data == null || expandedCookieName == "" || expandedCookieName == null)
		return;
	
    var expandedItemsIds = {};
    for (var i = 0; i < data.total(); i++) {
    	var item = data.at(i);
    	if (item.expanded == true) {
    		expandedItemsIds[item.id] = true;
    	}
    }

    $.cookie(expandedCookieName, kendo.stringify(expandedItemsIds));
    console.log("Save cookie for ProjectID " + expandedCookieName + " :" + kendo.stringify(expandedItemsIds));
    
	expandAll = true;
}

function readExpanded(data) {
	if (data == null || expandedCookieName == null || expandedCookieName == "")
		return;

	var expanded = $.cookie(expandedCookieName);
	console.log("Read cookie for ProjectID " + expandedCookieName + " :" + expanded);
	if (!expanded && expandAll) {
		expandall(data);
		return;
	}
	else if (!expanded) {
		return;
	}
	
	$.removeCookie(expandedCookieName);
	expanded = JSON.parse(expanded);
	expand(data, expanded);
}

function expand(data, expanded) {
	for (var i = 0; i < data.total(); i++) {
		if (expanded[data.at(i).id]) {
			data.at(i).expanded = true;
		} else {
			data.at(i).expanded = false;
		}
	}
	expandAll = false;
}

function expandall(data) {
	for (var i = 0; i < data.total(); i++) {
		if (data.at(i).hasChildren) {
			data.at(i).expanded = true;
		} else {
			data.at(i).expanded = false;
		}
	}
	expandAll = false;
}

function getContentSettings(action) {
	var retVal = null;
	$.ajax({
		url : location.protocol +"//" + location.host + window.location.pathname.substring(0, window.location.pathname.indexOf("/",2)) +"/tee/teeContentProcess.jsp?action=" + action,
		cache : false,
		async : false,
		type : "POST",
		contentType : "application/json;charset=utf-8",
		success : function(response) 
		{
			retVal = JSON.parse(response);
		}
	});
	return retVal;
}