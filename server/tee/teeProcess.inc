﻿<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="java.io.*"%>
<%@page import="com.transcat.tee.settings.*"%>

<%!static public <T> void getSchema(Class<T> clazz, String action, HttpServletResponse response) throws IOException {
		BaseSettings settings = TeeSettings.getSettings(clazz);
		String JSONSettings = settings.getJSONString();

		PrintWriter output = response.getWriter();
		output.println(JSONSettings);
		output.close();
	}

private String getRealURL(HttpServletRequest request)
{
	String contextPath = request.getContextPath();
	String sProtocol, sPort, sHost, sCtxPath, sRealURL;

  	sProtocol = request.getScheme();
  	//sPort = "" + request.getLocalPort();
	sPort = "" + request.getServerPort();
  	sHost = request.getServerName();

	sRealURL = sProtocol + "://" + sHost;
	if (sPort.length() > 0) 
	{
	  	sRealURL += ":"+ sPort;
	}
	sRealURL += contextPath;
	return sRealURL;
}
%>
