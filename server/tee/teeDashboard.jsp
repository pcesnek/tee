﻿<!DOCTYPE html>
<head>
    <title>TEE</title>
    <!-- Common Kendo UI CSS -->
    <link href="./kendo/styles/kendo.common.min.css" rel="stylesheet" />

    <!-- Default Kendo UI theme CSS -->
    <link href="./kendo/styles/kendo.uniform.min.css" rel="stylesheet" />

    <!-- (optional) Kendo UI DataViz CSS, include only if you will use the data visualisation features -->
    <link href="./kendo/styles/kendo.dataviz.uniform.min.css" rel="stylesheet" />
    
    <!-- TEE Custom CSS -->
    <link href="./styles/tee_main.css" rel="stylesheet" />

    <!-- jQuery JavaScript -->
    <script src="./kendo/scripts/jquery.min.js"></script>
    
    <!-- jQuery cookie JavaScript -->
    <script src="./kendo/scripts/jquery.cookie.js"></script>

    <!-- Kendo UI combined JavaScript -->
    <script src="./kendo/scripts/kendo.all.min.js"></script>
	
	<!-- Custom CSS -->
	<style>

		body {
			/*font: 400 12px Arial, Helvetica, sans-serif;*/
		}

		.splitter {
			border-width: 0;
			height: 100%;
		}
	  
/* 	  .k-grid-header, .k-gantt-toolbar, .k-window-title{
	  	font-size: 11px;
	  	padding: 0px;
	  	min-height:10px;
	  } */
	  
		#panelbar .k-content {
			min-height:250px;
			max-height:250px;
			overflow-y: scroll;
		}
		#panelbar .k-header {
			background-image: none,linear-gradient(to bottom,rgba(0,0,0,.01) 0,rgba(0,0,0,.08) 100%);
		}
		#panelbar .panelBarClose {
			float: right;
			line-height: normal;
			margin: .3em 1em 0 0;
			/*background-image: url('styles/Uniform/sprite.png');*/
		}
		
		body, html 
		{
			height: 100%;
		}
		#teeMainDivContainer 
		{
			height: 100%;
		}
		.divwindow
      	{
	        padding: 0;
    	    margin:0;
    	    height: 100%;
      	}
	  
	  	.splitter
      	{
        	border-width: 0;
        	height: 100%;
      	}
      	
      	#teeMainDivContainer .k-splitbar-vertical
      	{
      		background-color: #e30613 !important;
      		background-image: -o-linear-gradient(bottom, #d40000 0%, #550000 100%);
			background-image: -moz-linear-gradient(bottom, #d40000 0%, #550000 100%);
			background-image: -webkit-linear-gradient(bottom, #d40000 0%, #550000 100%);
			background-image: -ms-linear-gradient(bottom, #d40000 0%, #550000 100%);
			background-image: linear-gradient(to bottom, #d40000 0%, #550000 100%);
			border:#800000		
      	}      	
      	
      	#teeMainDivContainer .k-ghost-splitbar-vertical
      	{
      		background-color: #e30613 !important;
      		background-image: -o-linear-gradient(bottom, #d40000 0%, #550000 100%);
			background-image: -moz-linear-gradient(bottom, #d40000 0%, #550000 100%);
			background-image: -webkit-linear-gradient(bottom, #d40000 0%, #550000 100%);
			background-image: -ms-linear-gradient(bottom, #d40000 0%, #550000 100%);
			background-image: linear-gradient(to bottom, #d40000 0%, #550000 100%);
			border:#800000		
      	}          	
    </style>
		
</head>
<body>
	<div id="teeMainDivContainer" style="">
			<div id="teeGanttDiv" class="divwindow">
				<div id="teeGanttWindow" >
					<div id="teeGanttContent">
					Projects
					</div>
				</div>
			</div>
			<div id="teeKanbanDiv" class="divwindow">
				<div id= "teeKanbanWindow">
					<div id="teeKanbanWindowSplitter">
						<div id="teeKanbanTableDiv" class="divwindow">
							KanBan
						</div>
						<div id="teeKanbanTaskPropertiesDiv" class="divwindow">
							Task Properties
						</div>
					</div>
				</div>
			</div>
			<div id="teeProjectContentDiv" class="divwindow">
				<div id= "teeProjectContentWindow">
					<div id="teeProjectContentWindowSplitter">
						<div id = "teeProjectContentTreeViewDiv" class="divwindow">
							Project content tree view
						</div>
						<div id = "teeProjectContentPreviewDiv" class="divwindow">
							Project content preview
						</div>
						<div id="teeProjectContentPropertiesDiv" class="divwindow">
							Project content Properties
						</div>
					</div>
				</div>
			</div>
		
	</div>
	<script>
	function getContextPath() {
		return window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));
	}

	function checkContextForRelogin()
	{
 	    $.ajax({				
        	url: location.protocol +"//"+ location.host + getContextPath() + "/tee/teeKanbanProcess.jsp?action=ping",
		    cache: false,
		    async:false,
		    type: "POST",
		    success: function (response) 
		    {
		       response = response.replace(/^\s+|\s+$/g,"");//Remove NL
			   if(response != "0")
			   {
				window.location.replace(location.protocol +"//"+ location.host + getContextPath() +"/common/emxNavigator.jsp");
			   }
	        }
		});	
		
	}
	$(document).ready(function() {
			
			checkContextForRelogin();
			
			console.log("Document ready Dash Board");
			//--- Splitter settings
		$("#teeProjectContentWindowSplitter").kendoSplitter({
			panes: [{collapsible:false, contentUrl: "widget-projectcontent.html", scrollable: false},
			        {collapsible:true,  contentUrl: "widget-viewimage.html",size: "330px", scrollable: false},
			        {collapsible:true,  contentUrl: "widget-contentprop.html", size: "350px", scrollable: false}
			]
		});			
			
		$("#teeMainDivContainer").kendoSplitter({
			orientation: "vertical",
			panes: [														   
				{ collapsible: true, scrollable: false, resizable:true, contentUrl: "widget-ganttchart.html"},
				{ collapsible: true, scrollable: false, resizable:true},
				{ collapsible: true, scrollable: false, resizable:true }
			],
			resize: function (e) {
				ganttHeight = e.sender.element.context.children[0].style.height;
				kanbanHeight = e.sender.element.context.children[2].style.height;
				contentProjectHeight = e.sender.element.context.children[4].style.height;
				
				//resize ganttt				
				var ganttDiv = $("div#gantt");
				if ( ganttDiv.length > 0)
				{
					ganttDiv[0].style.height = ganttHeight;
					ganttDiv.data("kendoGantt").resize();
				}
				
				//resize TreeList Table Content (subtract 33 which is height of the table header)
				var treeListGridContent = $("div#treeList div.k-grid-content");
				
				if (treeListGridContent.length>0)
				{
					var gridHeight = (parseInt(contentProjectHeight.replace(/px/,""))-25)+"px";
					treeListGridContent[0].style.height = gridHeight;
				}
				

				//resize Kanban (subtract 22 which is height of the listViews)
				if ($("th.kanbanTH").length > 0)
				{
					var kanbanHeaderHeight = 0
					var kanbanLineHeight = ( parseInt(kanbanHeight.replace(/px/,"")) - kanbanHeaderHeight  )+"px";
					$("td.kanbanTD")[0].style.height = kanbanLineHeight;
					$("td.kanbanTD")[1].style.height = kanbanLineHeight;
					$("td.kanbanTD")[2].style.height = kanbanLineHeight;
					
					//$("th.kanbanTH")[0].style.height = "22px";
					//$("th.kanbanTH")[1].style.height = "22px";
					//$("th.kanbanTH")[2].style.height = "22px";
				}
				
			}
		});
			
		$("#teeKanbanWindowSplitter").kendoSplitter({
			panes: [ {collapsible: false, contentUrl: "widget-kanban.html", scrollable: false}, 
			         { collapsible: true, contentUrl: "widget-taskprop.html", resizable: true, size: "350px", scrollable: false}
			]
		});
		
		$("div#teeProjectContentWindow")[0].style.height = "100%";
		$("div#teeProjectContentWindowSplitter")[0].style.height = "100%";
		$("div#teeProjectContentTreeViewDiv")[0].style.height = "100%";
		$("div#teeProjectContentPreviewDiv")[0].style.height = "100%";
		$("div#teeProjectContentPropertiesDiv")[0].style.height = "100%";
		
		
		$("div#teeKanbanWindow")[0].style.height = "100%";
		$("div#teeKanbanWindowSplitter")[0].style.height = "100%";
		$("div#teeKanbanTableDiv")[0].style.height = "100%";
		$("div#teeKanbanTableDiv")[0].style.height = "100%";
		$("div#teeKanbanTaskPropertiesDiv")[0].style.height = "100%";
		
		console.log("Document ready Dash Board finished.");
		
		
		/*$("#teeKanbanWindow").kendoWindow({
			appendTo: "#teeKanbanDiv",
			title: "My Board",
			draggable: false,resizable: true,
			width: "100%",
			actions: [ "Maximize"],
			activate: function (){
				this.resize(true);
			}
		});
		
		$("#teeProjectContentWindow").kendoWindow({
			appendTo: "#teeProjectContentDiv",
			title: "Project Content",
			draggable: false,resizable: true,
			width: "100%",
			actions: [ "Maximize"],
			activate: function (){
				this.resize(true);
			}
		});
		
		$("#teeGanttWindow").kendoWindow({
			appendTo: "#teeGanttDiv",
			title: "Projects Content",
			draggable: false,resizable: true,
			width: "100%",
			actions: [ "Maximize"],
			activate: function (){
				this.resize(true);
			}
		});*/
		
		//mainSplitter.ajaxRequest("#teeGanttContent", "widget-ganttchart.html");
    });
		
	
	</script>
</body>
</html>