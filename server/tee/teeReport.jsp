﻿<%@page import="com.transcat.tee.utils.EnoviaUtil"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@include file = "../emxUICommonAppInclude.inc"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="java.util.*" %>
<%@page import = "java.util.Map" %>
<%@page import="com.transcat.tee.data.*"%>
<%@page import="com.transcat.tee.enovia.*"%>
<%@page import="com.transcat.tee.utils.*"%>
<%@page import="com.matrixone.apps.domain.*"%>
<%@page import="com.matrixone.apps.domain.util.*"%>
<%@page import="org.apache.log4j.*"%>
<%@include file = "teeProcess.inc"%>
<%
Logger logger = TEELogger.getUserLogger(context.getUser());
logger.debug("Call teeReport.jsp");


	try
	{
		DBReport dbRep = new DBReport();
		ArrayList<HashMap<String,String>> tasksHistoryInfo =  dbRep.reportAllStartedTasks(context);
	    
		//response.setContentType("application/octet-stream");
		String fileName = "teeReport_"+System.currentTimeMillis()+".csv";
		//response.setHeader("Content-Disposition", "attachment; filename=\""+fileName+"\"; size="+inFile.length());
		
		
		 response.setHeader("Content-type","application/csv");
	     response.setHeader("Content-disposition","inline; filename="+fileName);
	     //PrintWriter out = response.getWriter(); 
	     out.clear();
	     out.println("Project,Task,Assignee,Day,Hours");
	     logger.debug("Project,\tTask\tAssignee,\tDay,\tHours");
	     for(HashMap<String,String> record: tasksHistoryInfo)
	     {
	     	String taskName = record.get("taskName");
	     	String projectName = record.get("taskProject");
	     	String assignee = record.get("user");
	     	String day = record.get("day");
	     	String duration = record.get("duration");
	     	out.println(projectName+","+taskName+","+assignee+","+day+","+duration);
	     	logger.debug(projectName+",\t"+taskName+",\t"+assignee+",\t"+day+",\t"+duration);
	     }
	
	     out.flush(); 
	}
	catch (Exception ex)
	{
		ex.printStackTrace();
%>
	<script>
		alert ("Error during TEE Report creation: <%=ex.toString()%>");
	</script>
<% 		
	}
     //out.close();
	
%>

