﻿<%@page import="org.json.simple.*"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@include file = "../emxUICommonAppInclude.inc"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="java.util.*" %>
<%@ page import = "java.util.Map" %>
<%@page import="com.transcat.tee.data.*"%>
<%@page import="com.transcat.tee.enovia.*"%>
<%@page import="com.transcat.tee.utils.*"%>
<%@include file = "teeProcess.inc" %>
<%@page import="org.apache.log4j.*"%>
<%@page import="java.io.*" %>


<%
	Logger logger = TEELogger.getUserLogger(context.getUser());
	logger.debug("Call teeKanbanProcess.jsp");

	Enumeration en = request.getParameterNames();
	while (en.hasMoreElements()) 
	{
	 String parameterName = (String) en.nextElement();
	 String parameterValue = request.getParameter(parameterName);
	 logger.debug(parameterName+"="+parameterValue);
	}


String kanbanAction = request.getParameter("action");
if(kanbanAction == null)
	kanbanAction = "";

if(kanbanAction.equalsIgnoreCase("PING"))
{
	logger.debug("Ping for test:");
	String result = "";
	

	//if(context != null)
	//if(context != null && context.isConnected())		
	if(context != null)
		result = "0";
	else
		result = "1";
	
	PrintWriter output = response.getWriter();
	logger.debug("Result for ping to server:" + result);
	output.println( result );
	output.close();
}

if(kanbanAction.equalsIgnoreCase("GETSCHEMA"))
{
	KanbanSettings kambanSettings =  TeeSettings.getKanbanSettings();
	JSONObject objJSONKambanSettings = kambanSettings.getJSONDataObject(); 
	
	String jString = JSONObject.toJSONString(objJSONKambanSettings);
	 

	PrintWriter output = response.getWriter();
	logger.debug("KanbanAction GETSCHEMA:"+jString);
	output.println( jString );
	output.close();
}
if(kanbanAction.equalsIgnoreCase("getFormSchema"))
{
	getSchema(KanbanFormSettings.class, kanbanAction, response);
}
if( kanbanAction.equalsIgnoreCase("reorderTask"))
{
 	DBTask dbTask = new DBTask();	
 	
	BufferedReader br = request.getReader();
	String taskID = "";
	while((taskID = br.readLine()) != null) 
	{
		logger.debug(taskID);
		break;
	}
	JSONArray inputArray=(JSONArray)JSONValue.parse(taskID);
	ArrayList listTask = new ArrayList();
	for(int i =0;i<inputArray.size();i++)
	{
		JSONObject jOB = (JSONObject)inputArray.get(i);
		logger.debug(jOB);
		TeeDataTask task = TeeDataTask.createFromJSON(jOB);
		listTask.add(task);
	}
	
	dbTask.reorderKanbanTasksList(context,listTask);
}
if( kanbanAction.equalsIgnoreCase("updatekanban"))
{
	String target = request.getParameter("target");
	String columnStr = request.getParameter("column");
	String countStr = request.getParameter("count");	
	
	int column = Integer.parseInt(columnStr);
	int count = Integer.parseInt(countStr);
 	DBTask dbTask = new DBTask();	

	String taskID = request.getParameter("taskId");
	String routeNodeID = request.getParameter("routeNodeId");
	dbTask.updateTaskState(context,taskID, routeNodeID, target,column,count);
	
	//If target is todo move to first
	if(columnStr.equals("0"))
	{
		logger.debug("Reorder set to first.");
		ArrayList<TeeDataTask> listTaskOrdered = new ArrayList<TeeDataTask>();
		ArrayList<TeeDataTask> listTask =  dbTask.getUserActiveTasksForKanbanColumn(context, "ToDo","TCETEEUserPriority");
	    for(int t=0;t<listTask.size();t++)
	    {
	    	TeeDataTask task = listTask.get(t);
	    	if(task.getValue("SELECT_ID").equalsIgnoreCase(taskID))
	    		listTaskOrdered.add(0,task);
	    	else
	    		listTaskOrdered.add(task);
	    }
		dbTask.reorderKanbanTasksList(context,listTaskOrdered);
	}
} 
if( kanbanAction.equalsIgnoreCase("readkanban"))
{
	String kanbanColumnName = request.getParameter("ident");
	String kanbanOrderBy = request.getParameter("order");	
	if(kanbanOrderBy == null)
		kanbanOrderBy = "";
	
 	DBTask dbTask = new DBTask();
 
	JSONArray jarr=new JSONArray();
             
	logger.debug("Read kanban data for column:" + kanbanColumnName + " order by:" + kanbanOrderBy);
    ArrayList<TeeDataTask> listTask =  dbTask.getUserActiveTasksForKanbanColumn(context, kanbanColumnName,kanbanOrderBy);
    for(int t=0;t<listTask.size();t++)
    {
    	TeeDataTask task = listTask.get(t);
    	String selectPolicy = task.getValue("SELECT_POLICY");
    	String selectProjectName= task.getValue("SELECT_PROJECT");
    	JSONObject jOB = task.getJSONDataObject();
    	if (selectPolicy != null)
    	{
        	String symbolicPolicyName = FrameworkUtil.getAliasForAdmin(context, "policy", selectPolicy,true);    		
    		jOB.put ("KANBAN_POLICY",symbolicPolicyName);
    	}
    	if(selectProjectName != null)
    	{
        	String notEmptyCharProjectName = selectProjectName.replaceAll("\\s+","");    		
    		jOB.put ("PROJECT_CLASS",notEmptyCharProjectName);
    	}
 	   jarr.add(jOB);
    }     
     
    String jString = JSONArray.toJSONString(jarr);
    PrintWriter output = response.getWriter();
    logger.debug("readkanban" + jString);
    output.println( jString );
    output.close();
}     
%>

