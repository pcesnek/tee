﻿<%@page import="com.transcat.tee.utils.EnoviaUtil"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@include file = "../emxUICommonAppInclude.inc"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="java.util.*" %>
<%@page import = "java.util.Map" %>
<%@page import="com.transcat.tee.data.*"%>
<%@page import="com.transcat.tee.enovia.*"%>
<%@page import="com.transcat.tee.utils.*"%>
<%@page import="com.matrixone.apps.domain.*"%>
<%@page import="com.matrixone.apps.domain.util.*"%>
<%@page import="org.apache.log4j.*"%>
<%@include file = "teeProcess.inc"%>
<%
Logger logger = TEELogger.getUserLogger(context.getUser());
logger.debug("Call teeContentProcess.jsp");

Enumeration en = request.getParameterNames();
while (en.hasMoreElements()) 
{
 String parameterName = (String) en.nextElement();
 String parameterValue = request.getParameter(parameterName);
 logger.debug(parameterName+"="+parameterValue);
}

String realURL = getRealURL(request);
String action = request.getParameter("action");
if(action == null)
	action = "";

if(action.equalsIgnoreCase("CONTENT_TABLE_SCHEMA"))
{
    getSchema(ProjectContentTableSettings.class, action, response);
}
if(action.equalsIgnoreCase("getFormSchema"))
{
	getSchema(ProjectContentFormSettings.class, action, response);
}
if(action.equalsIgnoreCase("READCONTENT"))
{
	logger.debug("READCONTENT START");  
	JSONArray jarr=new JSONArray();
	
	String projectId = request.getParameter("projectId");	
	
	DBProject dbProject = new DBProject(realURL);

    ArrayList<TeeDataObject> listDepDoc =  dbProject.getProjectContent(context,projectId);    
    for(int t=0;t<listDepDoc.size();t++)
    {
 	   jarr.add(listDepDoc.get(t).getJSONDataObject());
    } 
     
     String jString = JSONArray.toJSONString(jarr);
     PrintWriter output = response.getWriter();
     logger.debug("READCONTENT" + jString);  

     output.println(jString );
     output.close();
}
if(action.equalsIgnoreCase("addDocumentToTask"))
{
	String targetTaskID = request.getParameter("taskid");	
	String docid = request.getParameter("docid");	
	
	 logger.debug("Add document :" + docid + " to task ID:" + targetTaskID);
     try
     {        	

	        RelationshipType linkType = new RelationshipType("TEE Task Relation");
	        //RelationshipType linkType = new RelationshipType("Task Document");
	        linkType.open(context);
	
	        BusinessObject documentBO = new BusinessObject(docid);
	        documentBO.open(context);
	        
	        BusinessObject taskBO = new BusinessObject(targetTaskID);
	        taskBO.open(context);	
	    
	        
	        Relationship ship = taskBO.connect(context, linkType, true, documentBO);
	}
	catch(Exception ex)
	{
		logger.debug(ex.toString());
	}   
    PrintWriter output = response.getWriter();
    output.println( "OK" );
    output.close();	 
}

if( action.equalsIgnoreCase("removeDocumentFromTask"))
{
	String targetTaskID = request.getParameter("taskID");	
	
	logger.debug("Remove all document from task ID:" + targetTaskID);
	
     try
     {        
		DomainObject taskBO = new DomainObject(targetTaskID);
		if(taskBO.exists(context))//neni connection 
		{
			taskBO.open(context);	     
			    
			StringList selectStmts = new StringList(1);
			selectStmts.addElement(DomainConstants.SELECT_ID);
				  
			StringList selectRelStmts = new StringList(1);
			selectRelStmts.addElement(DomainConstants.SELECT_RELATIONSHIP_ID);
			    
		    MapList mlItemsDocuments = taskBO.getRelatedObjects(context,"TEE Task Relation","*", selectStmts,selectRelStmts,false,true,(short)1, "","",0);
		    //MapList mlItemsDocuments = taskBO.getRelatedObjects(context,"Task Document","*", selectStmts,selectRelStmts,false,true,(short)1, "","",0);
		    for (int i=0; i<mlItemsDocuments.size(); i++)
		    {
			  Map docMap=(Map)mlItemsDocuments.get(i);
			  
			  String relID = docMap.get("id[connection]").toString();
			  logger.debug("Remove rel ID:" + relID);
			  DomainRelationship.disconnect(context,relID);
		    }
		}        
	}
	catch(Exception ex)
	{
		logger.debug(ex.toString());
	}   
    PrintWriter output = response.getWriter();
    output.println( "OK" );
    output.close();	 	
}
if(action.equalsIgnoreCase("getDocumentFromTask"))
{
	String retValue = "";
	String targetTaskID = request.getParameter("taskid");
	
	logger.debug("Get all objects from task ID:" + targetTaskID);
	DBTask dbTask = new DBTask();	
	ArrayList<String> listObjIDs = dbTask.gelAllRelationsObjectsIDForTask(context,targetTaskID);	
	for (int i=0; i<listObjIDs.size(); i++)
    {
	  String objID = listObjIDs.get(i);
	  retValue += objID+"|";
    }
	logger.debug("Related objects ids:" + retValue);
    PrintWriter output = response.getWriter();
    output.println( retValue );
    output.close();	 	
}
if(action.equalsIgnoreCase("getImageForDocument"))
{
	String retValue = "";
	String docID = request.getParameter("docID");		
	
	String jpoName = "emxImageManager";
	String methodName = "getImageURLs";

	//example of inputHashMap content:
	// generateHTML=false, 
	// objectList=[{id=384.46942.11509.9011}], 
	// imageData={MCSURL=http://tee-2014x-ml:8080/enovia}
	// paramList={{format=format_mxSmallImage}} 
	HashMap inputArgsMap = new HashMap(); 
	inputArgsMap.put("generateHTML", "false");

	//inputArgsMap.put("format","format_mxLargeImage");
	//inputArgsMap.put("format","format_mxSmallImage");
	inputArgsMap.put("format","format_generic");
	
	MapList idList = new MapList();
	HashMap idMap = new HashMap();
	idMap.put ("id",docID);
	idList.add(idMap);
	inputArgsMap.put("objectList", idList);
	
	HashMap ImageDataMap = new HashMap();
	ImageDataMap.put("MCSURL",getRealURL(request));
	HashMap requestMap = new HashMap();
	requestMap.put("ImageData",ImageDataMap);
	inputArgsMap.put("paramList", requestMap);
	
	// DUO
	DBDocument dbDocument = new DBDocument();
	Vector imagesInfo = dbDocument.getDocumentURLs(context, inputArgsMap);
	if (imagesInfo == null) {
		//call JPO
		/*Vector */imagesInfo = (Vector)JPO.invoke(context, jpoName, null, methodName, JPO.packArgs(inputArgsMap), Vector.class);
	}
	// DUO - end
	
	String jString = JSONArray.toJSONString(imagesInfo);
    PrintWriter output = response.getWriter();
    logger.debug("jSonRet = " + jString);  

    output.println(jString );
    output.close();
	
}
if(action.equalsIgnoreCase("addNewDocuments"))
{
    PrintWriter output = response.getWriter();
    try
    {     	
    	String dragTransportDirectory = TeeSettings.getProjectContentSettings().getDragTransportDir();

		String folderID = request.getParameter("folderid");
		String fileTypesNames = request.getParameter("typesNamesPoliciesForFiles");		
		String docName = "";
		
		ContextUtil.startTransaction(context, true);
		//file1:file2:file3,AutoName,Type,Policy|file4:file5,AutoName,Type,Policy
		String[] fileAllNamesTypesList = fileTypesNames.split("\\|");
    	for(int i=0;i<fileAllNamesTypesList.length;i++)	    		
    	{	    
    		String filesNameTypePol = (String)fileAllNamesTypesList[i];
    		String[] filesNameTypePolList = filesNameTypePol.split(",");
    		
    		String files = filesNameTypePolList[0];
    		String useAutoName = filesNameTypePolList[1];
    		String name = filesNameTypePolList[2];
    		String autoNameRev = filesNameTypePolList[3];
    		String typeName = filesNameTypePolList[4];
    		String policyName = filesNameTypePolList[5];
    		
    		if(useAutoName.equals("1"))
    		{
    			String[] nameRev = autoNameRev.split("-");
    			String autoName = nameRev[0];
    			String autoRev = "";
    			if(nameRev.length > 1)
    				autoRev = nameRev[1];
    			
    	   		ArrayList listNames = EnoviaUtil.generateAutoNames(context,autoName,autoRev,1);
    	    	for(int a=0;a<listNames.size();a++)	    		
    	    	{	    
    	    		String generatedName = (String)listNames.get(a);
    	    		logger.debug("Generated name:"+generatedName);
    	    		docName = generatedName;
    	    	}
    		}    		
    		else
    		{
    			docName = name;
    		}
    		// *************   Create basic document  ********************
		    Policy basicPolicy = new Policy(policyName);
		    basicPolicy.open(context);
		    String initialBasicRevision = basicPolicy.getFirstInSequence(context);
		   // String initialVersionedRevision = initialBasicRevision+".0";	
			logger.debug("Create new document:" + docName + " type:" + typeName + " policy:" + policyName);

			BusinessObject basicObj = new  BusinessObject (typeName,docName,initialBasicRevision,"");		
		    basicObj.create(context,policyName);  
		    
		    basicObj.setDescription(context, files.replace(":",","));
		    basicObj.update(context);
		    
    		
    		String[] fileList = files.split(":");
        	for(int f=0;f<fileList.length;f++)	    		
        	{	
        		String file = fileList[f];	
        		// *************   Create version document  ********************   
				BusinessObject basicObjVer = new  BusinessObject (typeName,docName,initialBasicRevision+"."+f,"");
        		//BusinessObject basicObjVer = new  BusinessObject (typeName,docName,""+f,"");	
				basicObjVer.create(context,"Version");   
				
				basicObjVer.setAttributeValue(context,"Title",file);
				
	    		//String vaultToDocLinkName = PropertyUtil.getSchemaProperty("relationship_VaultedDocuments");
	    		//RelationshipType linkType = new RelationshipType(vaultToDocLinkName);
	    		
	    		String docToActiveVerLinkName = PropertyUtil.getSchemaProperty("relationship_ActiveVersion");
	    		RelationshipType linkTypeActiveVer = new RelationshipType(docToActiveVerLinkName);
	    		linkTypeActiveVer.open(context);
	    		basicObj.connect(context,linkTypeActiveVer,true,basicObjVer);  	
	    		
	    		String docToLatestVerLinkName = PropertyUtil.getSchemaProperty("relationship_LatestVersion");
	    		RelationshipType linkTypeLatestVer = new RelationshipType(docToLatestVerLinkName);
	    		linkTypeLatestVer.open(context);	    			    		
	    		basicObj.connect(context,linkTypeLatestVer,true,basicObjVer); 
	    		
        		
        	    String mqlCmdForChecIn = "checkin bus '" + typeName + "' '" + docName + "' '" + initialBasicRevision + "' append '" + dragTransportDirectory + "\\" + file + "'";
        	    // MqlUtil.mqlCommand (context, "checkin bus Document emxInstall.tcl 0 append C:\TEMP\test1.txt;");
        	    //mql checkin bus Assembly Wheel 0 format ascii file C:\TEMP\text.txt;
        	    logger.debug("MQL command:" + mqlCmdForChecIn);
        	    MqlUtil.mqlCommand (context,mqlCmdForChecIn);        		
        	}
        	
    	    BusinessObject workspaceVault = new BusinessObject(folderID);
    		workspaceVault.open(context);
    		
    		String vaultToDocLinkName = PropertyUtil.getSchemaProperty("relationship_VaultedDocuments");
    		RelationshipType linkType = new RelationshipType(vaultToDocLinkName);
    		linkType.open(context);
    		workspaceVault.connect(context,linkType,true,basicObj);  	

    		logger.debug("Document created and linking succesfully.");

    		ContextUtil.commitTransaction(context);
    	}
	    output.println("OK" ); 
	}
	catch(Exception ex)
	{
		logger.debug(ex.toString());
		ContextUtil.abortTransaction(context);
	    output.println(ex.toString());
	} 
    output.close();
}
        
%>

