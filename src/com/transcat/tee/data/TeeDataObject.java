package com.transcat.tee.data;

import java.util.*;

import org.json.simple.JSONObject;


public abstract class TeeDataObject extends JSONDataBasic
{
	
	HashMap<String,String> m_ValuesMap = new HashMap<String,String>();
	
	
	public TeeDataObject(int type)
	{
		super(type);
	}
	
	public void addValue(String selectName, String attributeValue)
	{
		m_ValuesMap.put (selectName,attributeValue);
	}
	
	public HashMap<String,String> getValues()
	{
		return(m_ValuesMap);
	}
	
	public String getValue(String selectName)
	{
		return m_ValuesMap.get(selectName);
	}
	
	public JSONObject getJSONDataObject()
	{
		JSONObject retVal = new JSONObject();
		for (Map.Entry<String, String> entry : m_ValuesMap.entrySet()) 
		{
		    String key = entry.getKey();
		    Object value = entry.getValue();
		    retVal.put(key,value);
		}
		return retVal;
	}

	protected void readValuesMapFromJSONObject(JSONObject jOB)
	{
		for (Object key : jOB.keySet())
		{
			String keyStr = (String)key;
			
			Object value = jOB.get(keyStr);
			if (value != null)
			{
				if ("SELECT_DUE_DATE_OFFSET".equals(keyStr)) {
					value = formatDueDateOffset(value);
				}
				m_ValuesMap.put(keyStr,value.toString());
			}
		}
	}
	
	private Object formatDueDateOffset(Object value) {
		String strValue = value.toString();
		
		if (!strValue.matches("\\d{1,2}[.,]?\\d{0,2}?"))
			return value;
		
/*		try { 
	        Double.parseDouble(strValue); 
	    } catch(NumberFormatException e) { 
	        return value; 
	    } catch(NullPointerException e) {
	        return value;
	    }*/
		
	    return value + "h";
	}
	
	public ArrayList<HashMap<String,String>> getDifferenceFrom(TeeDataObject another)
	{
		ArrayList<HashMap<String,String>> retVal = new ArrayList<HashMap<String,String>>();
		
		HashMap<String,String> anotherValuesMap = another.getValues(); 
		for (String key:m_ValuesMap.keySet())
		{
			String origValue = m_ValuesMap.get(key);
			String anotherValue = "";
			if (anotherValuesMap.containsKey(key))
			{
				anotherValue = anotherValuesMap.get(key);
			}
			if (origValue.equals(anotherValue) == false)
			{
				HashMap<String,String> diffMap = new HashMap<String,String>();
				diffMap.put("key",key);
				diffMap.put("origVal",origValue);
				diffMap.put("newVal",anotherValue);
				retVal.add(diffMap);
			}
		}
		
		for (String key:anotherValuesMap.keySet())
		{
			if (m_ValuesMap.containsKey(key) == false)
			{
				String anotherValue = anotherValuesMap.get(key);
				HashMap<String,String> diffMap = new HashMap<String,String>();
				diffMap.put("key",key);
				diffMap.put("origVal","");
				diffMap.put("newVal",anotherValue);
				retVal.add(diffMap);
			}
		}
		return retVal;
	}
}
