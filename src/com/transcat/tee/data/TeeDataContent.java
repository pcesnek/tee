package com.transcat.tee.data;

import org.json.simple.JSONObject;

public class TeeDataContent extends TeeDataObject 
{
	public TeeDataContent()
	{
		super(TeeDataObject.TYPE_DATA);
	}
	
	public JSONObject getJSONDataObject()
	{
		JSONObject retVal = super.getJSONDataObject();
		return retVal;
	}
}
