package com.transcat.tee.data;


import java.io.*;
import java.util.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.transcat.tee.settings.TeeSettings;

import java.util.ResourceBundle;



public abstract class JSONDataBasic
{
	public static final int TYPE_PROJECT = 1;
	public static final int TYPE_TASK = 2;	
	public static final int TYPE_DATA = 3;	
		
	private int m_ObjectType = 0;
	
	public JSONDataBasic(int type)
	{
		m_ObjectType = type;

	}
	
	public abstract JSONObject getJSONDataObject();
}
