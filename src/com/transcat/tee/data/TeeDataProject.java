package com.transcat.tee.data;

import org.json.simple.JSONObject;

public class TeeDataProject extends TeeDataObject 
{
	public TeeDataProject()
	{
		super(TeeDataObject.TYPE_PROJECT);
	}
	
	public JSONObject getJSONDataObject()
	{
		JSONObject retVal = super.getJSONDataObject();
		return retVal;
	}
	
	public static TeeDataProject createFromJSON(JSONObject jOB)
	{
		TeeDataProject retVal = new TeeDataProject();
		retVal.readValuesMapFromJSONObject(jOB);
		
		return retVal;
	}
}
