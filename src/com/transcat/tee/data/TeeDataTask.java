package com.transcat.tee.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import org.json.simple.JSONObject;

import com.transcat.tee.settings.KanbanSettings;
import com.transcat.tee.settings.TeeSettings;


public class TeeDataTask extends TeeDataObject
{
	
	protected boolean m_IsTask = false;
	protected String m_RouteID = null; //used in Gantt; !! not intialized in Kanban !!
	
	public TeeDataTask()
	{
		super(TeeDataObject.TYPE_TASK);
		addValue("SELECT_TASK_ICON", "iconSmallInboxTask.png");
	}
	
	public boolean getIsTask() 
	{
		return m_IsTask;
	}

	public void setIsTask(boolean isTask)
	{
		m_IsTask = isTask;
	}
	
	public String getRouteID()
	{
		return m_RouteID;
	}
	public void setRouteID(String routeID)
	{
		m_RouteID = routeID;
	}
	
	public JSONObject getJSONDataObject()
	{
		JSONObject retVal = super.getJSONDataObject();
		retVal.put("isTask", m_IsTask);
		if (m_RouteID != null)
		{
			retVal.put("SELECT_ROUTE_ID",m_RouteID);
		}
		
		
		//duplicate select fields for gantt
	   	String selectID = getValue("SELECT_ID");
	   	if (selectID != null)
	   	{
	   		retVal.put ("GANTT_ID",selectID);
	   	}
	   	String selectTitle = getValue("SELECT_TITLE");
	   	if (selectTitle != null)
	   	{
	   		retVal.put ("GANTT_TITLE",selectTitle);
	   	}
	   	String routeOrder = getValue("SELECT_ORDER");
	   	if (routeOrder != null)
	   	{
	   		retVal.put ("GANTT_ORDER_ID",routeOrder);
	   	}
	   	
	   	String isMilestone = "false";
	   	String milestoneDate = getValue("SELECT_MILESTONE_DATE");
	   	if (milestoneDate != null && milestoneDate.length() > 0)
	   	{
	   		isMilestone = "true";
	   		retVal.put ("SELECT_TASK_ICON","iconSmallMilestone.png");
	   	}
	   	retVal.put ("SELECT_IS_MILESTONE",isMilestone);
	   	
		return retVal;
	}
	
	public static TeeDataTask createFromJSON(JSONObject jOB)
	{
		TeeDataTask retVal = new TeeDataTask();
		
		Boolean isTask = (Boolean)jOB.get("isTask");
		if (isTask != null)
		{
			retVal.setIsTask(isTask.booleanValue());
			jOB.remove("isTask");
		}
		
		String routeID = (String)jOB.get("SELECT_ROUTE_ID");
		if (routeID != null)
		{
			retVal.setRouteID(routeID);
			jOB.remove("SELECT_ROUTE_ID");
		}
		
		
		retVal.readValuesMapFromJSONObject(jOB);
		
		//duplicate select fields for gantt
	   	/*String selectID = getValue("SELECT_ID");
	   	if (selectID != null)
	   	{
	   		retVal.put ("GANTT_ID",selectID);
	   	}
	   	String selectTitle = getValue("SELECT_TITLE");
	   	if (selectTitle != null)
	   	{
	   		retVal.put ("GANTT_TITLE",selectTitle);
	   	}*/
	   	Object routeOrderObj = jOB.get("GANTT_ORDER_ID");
	   	if (routeOrderObj != null)
	   	{// to be able to check order difference
	   		retVal.addValue ("SELECT_ORDER",routeOrderObj.toString());
	   	}
	   	
	   	//format date for milestone attribute
	   	String strDateValue = retVal.getValue("SELECT_MILESTONE_DATE");
	   	if (strDateValue != null && strDateValue.isEmpty() == false)
	   	{
	   		//2015-03-27T23:00:00.000Z
	   		//to : yr4/moy/dom@h24:min:sec:tz
	   		try
	   		{
		   		SimpleDateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		   		TimeZone gmtTimeZone = TimeZone.getTimeZone("Etc/GMT");
		   		fromFormat.setTimeZone(gmtTimeZone);
		   		SimpleDateFormat enoviaFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		   		TimeZone tz = TimeZone.getDefault();
		   		enoviaFormat.setTimeZone(TimeZone.getDefault());

		   		Date dateValue = fromFormat.parse(strDateValue) ;
		   		
		   		String enoviaStrDateValue = enoviaFormat.format(dateValue);
		   		retVal.addValue("SELECT_MILESTONE_DATE", enoviaStrDateValue);
	   		}
	   		catch (ParseException pE)
	   		{
	   			pE.printStackTrace();
	   		}
	   	}
		
		return retVal;
	}
	
	
	protected HashMap<String,String> getMapFromDifferencesListByKey(ArrayList<HashMap<String,String>> diffList, String key)
	{
		HashMap<String,String> retVal = null;
		
		for (HashMap<String,String> difMap:diffList)
		{
			String keyValue = difMap.get("key");
			if (keyValue.equals(key))
			{
				retVal  =difMap;
				break;
			}
		}
		return retVal;
	}
	
	public ArrayList<HashMap<String,String>> getDifferenceFrom(TeeDataTask another)
	{
		ArrayList<HashMap<String,String>> retVal = super.getDifferenceFrom(another);
		
		

		//check milestone changes
		ArrayList<HashMap<String,String>> differencesToBeRemoved = new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String,String>> differencesToBeAdded = new ArrayList<HashMap<String,String>>();
		
		String sIsUpdatedTaskMilestone = another.getValue("SELECT_IS_MILESTONE");
		boolean bIsUpdatedTaskMilestone = "true".equalsIgnoreCase(sIsUpdatedTaskMilestone);
		
		String currentTaskMilestoneDate = getValue("SELECT_MILESTONE_DATE");
		boolean bIsCurrentTaskMilestone = (currentTaskMilestoneDate !=null && currentTaskMilestoneDate.length()>0);

		if (bIsUpdatedTaskMilestone && bIsCurrentTaskMilestone)
		{ 
			//both tasks are milestone - user shouldn't change assignee (none) nor estimation time (0)
			HashMap<String,String>assigneeDifMap = getMapFromDifferencesListByKey(retVal,"SELECT_ASSIGNEE");
			if (assigneeDifMap != null)
			{
				differencesToBeRemoved.add(assigneeDifMap);
			}
			
			HashMap<String,String> dueDateOfSetDifMap = getMapFromDifferencesListByKey(retVal,"SELECT_DUE_DATE_OFFSET");
			if (dueDateOfSetDifMap != null)
			{
				differencesToBeRemoved.add(dueDateOfSetDifMap);
			}
			
		}
		else if (bIsUpdatedTaskMilestone)
		{
			//current task isn't milestone and user is changing it to milestone
			// => change assignee to 'none' (if necessary) and estimation time to 0
			
			//first check whether there's assignee and estimation changes in the difference map - if so remove it
			HashMap<String,String> removeAssigneeDifMap = getMapFromDifferencesListByKey(retVal,"SELECT_ASSIGNEE");
			if (removeAssigneeDifMap != null)
			{
				differencesToBeRemoved.add(removeAssigneeDifMap);
			}
			
			HashMap<String,String> removeDueDateOfSetDifMap = getMapFromDifferencesListByKey(retVal,"SELECT_DUE_DATE_OFFSET");
			if (removeDueDateOfSetDifMap != null)
			{
				differencesToBeRemoved.add(removeDueDateOfSetDifMap);
			}
			
			//second add new difference Maps for assignee and estimation time
			HashMap<String,String>addAssigneeDifferenceMap = new HashMap<String,String>();
			addAssigneeDifferenceMap.put("key","SELECT_ASSIGNEE");
			addAssigneeDifferenceMap.put("origVal",getValue("SELECT_ASSIGNEE"));
			addAssigneeDifferenceMap.put("newVal","none");
			differencesToBeAdded.add(addAssigneeDifferenceMap);
			
			HashMap<String,String>addDueDateOfSetDifferenceMap = new HashMap<String,String>();
			addDueDateOfSetDifferenceMap.put("key","SELECT_DUE_DATE_OFFSET");
			addDueDateOfSetDifferenceMap.put("origVal",getValue("SELECT_DUE_DATE_OFFSET"));
			addDueDateOfSetDifferenceMap.put("newVal","0");
			differencesToBeAdded.add(addDueDateOfSetDifferenceMap);
			
			HashMap<String,String>addStartDateDifferenceMap = new HashMap<String,String>();
			addStartDateDifferenceMap.put("key","SELECT_START_DATE");
			addStartDateDifferenceMap.put("origVal",getValue("SELECT_START_DATE"));
			addStartDateDifferenceMap.put("newVal","");
			differencesToBeAdded.add(addStartDateDifferenceMap);
		}
		else if (bIsCurrentTaskMilestone)
		{
			//current task is milestone and user is changing it to regular task
			// => clear milestone date attribute
			
			//first check whether there's milestone date change in the difference map - if so remove it
			HashMap<String,String> removeMilestoneDateDifMap = getMapFromDifferencesListByKey(retVal,"SELECT_MILESTONE_DATE");
			if (removeMilestoneDateDifMap != null)
			{
				differencesToBeRemoved.add(removeMilestoneDateDifMap);
			}
			
			HashMap<String,String> addMilestoneDateDifferenceMap = new HashMap<String,String>();
			addMilestoneDateDifferenceMap.put("key","SELECT_MILESTONE_DATE");
			addMilestoneDateDifferenceMap.put("origVal",getValue("SELECT_MILESTONE_DATE"));
			addMilestoneDateDifferenceMap.put("newVal","");
			differencesToBeAdded.add(addMilestoneDateDifferenceMap);
		}
		else
		{
			//neither of tasks is milestone - milestone date shouldn't be in the differences list
			//check whether there's milestone date change in the difference map - if so remove it
			HashMap<String,String> removeMilestoneDateDifMap = getMapFromDifferencesListByKey(retVal,"SELECT_MILESTONE_DATE");
			if (removeMilestoneDateDifMap != null)
			{
				differencesToBeRemoved.add(removeMilestoneDateDifMap);
			}
		}
		
		//check whether we have change in SELECT_ORDER attribute
		HashMap<String,String>selectOrderDifMap = getMapFromDifferencesListByKey(retVal,"SELECT_ORDER");
		if (selectOrderDifMap != null)
		{
			//if changing order in gantt - change also user order
			String newGanttOrder = selectOrderDifMap.get("newVal");
			String currentUserOrder = getValue("SELECT_USER_PRIORITY");
			if (newGanttOrder.equals(currentUserOrder) == false)
			{
				HashMap<String,String> addUserPriorityDifferenceMap = new HashMap<String,String>();
				addUserPriorityDifferenceMap.put("key","SELECT_USER_PRIORITY");
				addUserPriorityDifferenceMap.put("origVal",currentUserOrder);
				addUserPriorityDifferenceMap.put("newVal",newGanttOrder);
				differencesToBeAdded.add(addUserPriorityDifferenceMap);
			}
		}
		
		
		for (HashMap<String,String> difMapToBeRemoved:differencesToBeRemoved)
		{
			retVal.remove (difMapToBeRemoved);
		}
		

		for (HashMap<String,String> difMapToBeAdded:differencesToBeAdded)
		{
			retVal.add (difMapToBeAdded);
		}
		
		return retVal;
	}
}
