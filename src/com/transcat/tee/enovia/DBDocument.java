package com.transcat.tee.enovia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import matrix.db.BusinessObjectProxy;
import matrix.db.Context;
import matrix.util.StringList;

import com.matrixone.apps.domain.DomainConstants;
import com.matrixone.apps.domain.DomainObject;
import com.matrixone.apps.domain.util.FrameworkException;
import com.matrixone.apps.domain.util.MapList;
import com.matrixone.apps.framework.ui.UIComponent;
import com.matrixone.apps.framework.ui.UIUtil;
import com.matrixone.fcs.common.ImageRequestData;

public class DBDocument {
	public static final String DBDOCUMENT_FILENAME = "filename";
	public static final String DBDOCUMENT_EXTENSION = "extension";
	public static final String DBDOCUMENT_FORMAT= "format";

	public DBDocument() {
		
	}
	
	public Vector getDocumentURLs(Context context, HashMap inputArgsMap) throws Exception {
		HashMap idMap = (HashMap) ((MapList) inputArgsMap.get("objectList")).get(0);
		String id = (String) idMap.get("id");
		HashMap imageDataMap = (HashMap) ((HashMap)inputArgsMap.get("paramList")).get("ImageData");

        // for main document object:
        DomainObject document = DomainObject.newInstance(context,id);
        // get type
		String type = (String) document.getInfo(context, DomainConstants.SELECT_TYPE);

        StringList formats = document.getInfoList(context,"format");
        if (formats == null || formats.size() == 0)
        	return null;

        // search all formats for file with PDF extension
        HashMap fileinfo = searchForExtension(context, document, formats, new String[] {"pdf", "png", "jpg", "gif"});
        // if no graphic format found return nothing
        if (fileinfo == null)
        	return null;
        
        BusinessObjectProxy bop = new BusinessObjectProxy(id, (String) fileinfo.get(DBDOCUMENT_FORMAT), (String) fileinfo.get(DBDOCUMENT_FILENAME), false, false);
        ArrayList list  = new ArrayList();
        list.add(bop);
        String[] array = ImageRequestData.getImageURLS(context, list, imageDataMap);

        // getImageColumnInfo
        HashMap imageMap = new HashMap();
        imageMap.put(UIComponent.IMAGE_MANAGER_FILE_NAME, fileinfo.get(DBDOCUMENT_FILENAME));
        imageMap.put(UIComponent.IMAGE_MANAGER_IMAGE_URL, array[0]);
        imageMap.put(DBDOCUMENT_EXTENSION, fileinfo.get(DBDOCUMENT_EXTENSION));
        imageMap.put(DomainConstants.SELECT_TYPE, type);
        
        Vector finalImageURLs = new Vector();
        finalImageURLs.add(imageMap);
        
		return finalImageURLs;
	}
	
    private HashMap searchForExtension(Context context, DomainObject document, StringList formats, String[] extensions) throws Exception {
    	for (String extension : extensions) {
	        for (int i=0; i<formats.size(); i++) {
	        	StringList files = document.getInfoList(context,"format["+formats.get(i)+"].file.name");
	        	if (files == null || files.size() == 0)
	        		continue;
	        	
	            // search all formats for file with PDF extension
	            for (Object file : files) {
	            	String fileextension = getFileExtension((String)file);
		    			if (extension.equalsIgnoreCase(fileextension)) {
		    				HashMap map = new HashMap();
		    				map.put(DBDOCUMENT_FILENAME, file);
		    				map.put(DBDOCUMENT_FORMAT, formats.get(i));
		    				map.put(DBDOCUMENT_EXTENSION, extension);
		    				return map;
	            	}
	    		}
	        }
    	}
        return null;
    }
    
    static public String getFileExtension(String strFileName) {
        int index = strFileName.lastIndexOf('.');

        if (index == -1) {
            return strFileName;
        } else {
            return strFileName.substring(index + 1, strFileName.length());
        }
    }
    
    static public String getFileBaseName(String strFileName) {
        int index = strFileName.lastIndexOf('.');

        if (index == -1) {
            return strFileName;
        } else {
            return strFileName.substring(0, index);
        }
    }
    
	public Vector getPDFDocumentURLs(Context context, HashMap inputArgsMap) throws Exception {
		HashMap idMap = (HashMap) ((MapList) inputArgsMap.get("objectList")).get(0);
		String id = (String) idMap.get("id");
		
		HashMap imageDataMap = (HashMap) ((HashMap)inputArgsMap.get("paramList")).get("ImageData");
		
        DomainObject pdfDocument = DomainObject.newInstance(context,id);
        StringList pdfFiles    = pdfDocument.getInfoList(context,"format[generic].file.name");
        if (pdfFiles == null || pdfFiles.size() == 0)
        	return null;
        
        String fileName = (String) pdfFiles.get(0);
        BusinessObjectProxy bop = new BusinessObjectProxy(id, "generic", fileName, false, false);
        ArrayList list  = new ArrayList();
        list.add(bop);
        String[] array = ImageRequestData.getImageURLS(context, list, imageDataMap);
        
        HashMap imageMap = new HashMap();
        imageMap.put(UIComponent.IMAGE_MANAGER_IMAGE_URL, array[0]);
        
        Vector finalImageURLs = new Vector();
        finalImageURLs.add(imageMap);
        
		
		return finalImageURLs;
	}
}
