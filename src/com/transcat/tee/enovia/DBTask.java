package com.transcat.tee.enovia;

import java.text.SimpleDateFormat;
import java.util.*;

import com.matrixone.apps.common.Route;
import com.matrixone.apps.domain.DomainConstants;
import com.matrixone.apps.domain.DomainObject;
import com.matrixone.apps.domain.util.*;

import matrix.db.*;
import matrix.util.*;

import com.transcat.tee.data.*;
import com.transcat.tee.settings.*;
import com.transcat.tee.utils.*;
import org.apache.log4j.*; 

public class DBTask 
{
	
	protected ArrayList<String> m_SelectablesList;
	protected HashMap<String,String> m_SelectablesToSelectableNamesMap;
	protected HashMap<String,String> m_SelectablesNamesToSelectablesMap;
	protected TaskSelectableSettings m_Settings;
	
	public DBTask()
	{
		m_Settings = TeeSettings.getTaskSelectableSettings();
	}
	
	
	protected void setTaskSelects()
	{
		m_SelectablesList = m_Settings.getTaskSelectablesList();
		m_SelectablesToSelectableNamesMap = m_Settings.getTaskSelectablesToSelectableNamesMap();
		m_SelectablesNamesToSelectablesMap = m_Settings.getTaskSelectableNamesToSelectablesMap();
	}
	
	protected void setRouteNodeSelects()
	{
		m_SelectablesList = m_Settings.getRouteNodeSelectablesList();
		m_SelectablesToSelectableNamesMap = m_Settings.getRouteNodeSelectablesToSelectableNamesMap();
		m_SelectablesNamesToSelectablesMap = m_Settings.getRouteNodeSelectableNamesToSelectablesMap();
	}
	
	protected TeeDataTask getTaskByID(Context context, String id, boolean isTask) throws FrameworkException
	{
		TeeDataTask retVal = null;
		String mqlCommand, mqlRet;
		if (isTask)
		{
			setTaskSelects();
			mqlCommand = constructMqlCommandForGetTaskInfo(id);
			mqlRet = MqlUtil.mqlCommand (context, mqlCommand);
			ArrayList<TeeDataTask> currentTaskInfoList = processTasksQueryResultString(context,mqlRet,0);
			retVal = currentTaskInfoList.get(0);
			retVal.setIsTask(true);
		}
		else
		{
			setRouteNodeSelects();
			mqlCommand = constructMqlCommandForGetRouteNodeInfo(id);
			mqlRet = MqlUtil.mqlCommand (context, mqlCommand);
			ArrayList<TeeDataTask> currentTaskInfoList = processTasksQueryResultString(context,mqlRet,0);
			retVal = currentTaskInfoList.get(0);
		}
		return retVal;
	}
	
	public ArrayList<TeeDataTask> updateTasks(Context context, ArrayList<TeeDataTask> changedTasks, String project) throws FrameworkException
	{
		Logger logger = TEELogger.getUserLogger(context.getUser());
		
		logger.debug (">>>updateTasks begin");
		ArrayList<TeeDataTask> retVal = new ArrayList<TeeDataTask>();
		
		HashMap<TeeDataTask,ArrayList<String>> commandsListByTask = new HashMap<TeeDataTask,ArrayList<String>>();
		boolean foundAnyChange = false;

		ArrayList<String> nonUpdateableSystemSelectsList = new ArrayList<>(Arrays.asList
				(
					"SELECT_ID","SELECT_CURRENT","SELECT_POLICY","SELECT_ROUTE_SEQUENCE","SELECT_ROUTE_NODE_ID",
					"SELECT_SCHEDULED_COMPLETION_DATE","SELECT_ACTUAL_COMPLETION_DATE",
					"SELECT_PROJECT","SELECT_PROJECT_ID",
					"SELECT_ASSIGNEE_ID","SELECT_ASSIGNEE_TYPE","SELECT_TASK_ASSIGNEE_RELID"
				)
				);
		
		String errorMessage = "";
		
		for (TeeDataTask changedTask:changedTasks)
		{
			String objectId = changedTask.getValue("SELECT_ID");
			String title = changedTask.getValue("SELECT_TITLE");
			logger.debug ("Processing task Title: "+title+", oid="+objectId);
			
			TeeDataTask currentTask = null;
		
			try
			{
				String mqlCommand, mqlRet;
				currentTask = getTaskByID (context, objectId, changedTask.getIsTask());

				ArrayList<String> updateMqlCommandsList = new ArrayList<String>();
				ArrayList<HashMap<String,String>> diffList = currentTask.getDifferenceFrom(changedTask);

				//clear difflist
				ArrayList<HashMap<String,String>> clearedDiffList = new ArrayList<HashMap<String,String>>();
				for (HashMap<String,String> diffMap: diffList)
				{
					String selKey = diffMap.get("key");
					String origVal = diffMap.get("origVal");
					String newValue = diffMap.get("newVal");
					if (nonUpdateableSystemSelectsList.contains(selKey)) 
					{
						logger.debug ("DBTask.updateTask will not update system attribute for selectKey: "+selKey +", origVal:'"+origVal+"', newValue:'"+newValue+"'");
						continue;
					}
					String mqlSelStr = m_SelectablesNamesToSelectablesMap.get(selKey);
					if (mqlSelStr == null)
					{
						logger.debug ("DBTask.updateTask will not update attribute for unsupported selectKey: "+selKey+"', newValue:'"+newValue+"'");
						continue;
					}
					clearedDiffList.add(diffMap);
				}
				
				logger.debug ("clearedDiffList="+clearedDiffList);
				
				String taskStatus = currentTask.getValue("SELECT_CURRENT");
				if (taskStatus != null && "ToDo".equalsIgnoreCase(taskStatus) ==false)
				{
					//only Gantt order can be changed here!!!
					logger.debug (" !! Task is in state: '"+taskStatus+"' no changes -other than gantt order- will be done!");
					
					ArrayList<HashMap<String,String>> updatedDiffList = new ArrayList<HashMap<String,String>> ();
					
					for (HashMap<String,String> diffMap: clearedDiffList)
					{
						String selKey = diffMap.get("key");
						if (selKey.equalsIgnoreCase("SELECT_ORDER") || selKey.equalsIgnoreCase("SELECT_USER_PRIORITY"))
						{
							updatedDiffList.add (diffMap);
						}
						
					}
					
					logger.debug ("updatedDiffList="+updatedDiffList);
					if (clearedDiffList.size() != updatedDiffList.size())
					{
						errorMessage = "Cannot change task '"+title+"' with state: "+taskStatus;
					}
					
					clearedDiffList = updatedDiffList;
				}
				
				if (clearedDiffList.size() == 0)
				{
					logger.debug (":: no changes done!");
					commandsListByTask.put(changedTask,updateMqlCommandsList);
					continue;
				}
				
				
				
				for (HashMap<String,String> diffMap: clearedDiffList)
				{
					String selKey = diffMap.get("key");
					String origVal = diffMap.get("origVal");
					String newValue = diffMap.get("newVal");
					
					if (selKey.equalsIgnoreCase("SELECT_ASSIGNEE"))
					{
						//update assignee
						logger.debug (" - Updating assignee: origVal:'"+origVal+"', newValue:'"+newValue+"'");
						
						String newPersonID = "";
						//evaluate Person or Route Task User id
						if (newValue.equals("none"))
						{
							String routeID = changedTask.getRouteID();
							newPersonID = getRouteTaskUserIDForRoute(context, routeID, true /*create Route Task User if there's none*/);
						}
						else
						{
							mqlCommand = "print bus Person '"+newValue+"' - select id dump |";
							mqlRet = MqlUtil.mqlCommand (context, mqlCommand);
							newPersonID = mqlRet;
						}
	
						String connectionID_RouteToPerson = currentTask.getValue("SELECT_ROUTE_NODE_ID");
						boolean errorOccured = false;
							
						mqlCommand = "mod connection "+connectionID_RouteToPerson+" to "+newPersonID;
						updateMqlCommandsList.add(mqlCommand);
						
						if (changedTask.getIsTask())
						{
							String connectionID_TaskToPerson = currentTask.getValue("SELECT_TASK_ASSIGNEE_RELID");
							mqlCommand = "mod connection "+connectionID_TaskToPerson+" to "+newPersonID;
							updateMqlCommandsList.add(mqlCommand);

							// DUO 20150330: when user is assigned task will change owner to assignee
							String newPersonName = newValue.equals("none") ? "User Agent" : newValue;
							mqlCommand = "mod bus "+currentTask.getValue("SELECT_ID")+" owner "+"'"+newPersonName+"'";
							updateMqlCommandsList.add(mqlCommand);
							// DUO 20150330: end
						}
						
					}
					else
					{
						String mqlSelStr = m_SelectablesNamesToSelectablesMap.get(selKey);
						logger.debug (" - Updating select: '"+selKey+"["+mqlSelStr+"]', origVal:'"+origVal+"', newValue:'"+newValue+"'");
						
						String mqlPropStr = m_Settings.trimSelectFromAttributePrefix(mqlSelStr);
						if (changedTask.getIsTask())
						{
							
							mqlCommand = "mod bus "+objectId+" '"+mqlPropStr+"' '"+newValue+"'";
							updateMqlCommandsList.add(mqlCommand);
						}
						
						String connectionID_RouteToPerson = currentTask.getValue("SELECT_ROUTE_NODE_ID");
						mqlCommand = "mod connection "+connectionID_RouteToPerson+" '"+mqlPropStr+"' '"+newValue+"'";
						updateMqlCommandsList.add(mqlCommand);
						
					}
				}// for browsing differences
				
				if (updateMqlCommandsList.size() > 0)
				{
					foundAnyChange = true;	
				}
				
				commandsListByTask.put(changedTask,updateMqlCommandsList);
			}
			catch (Exception ex)
			{
				logger.debug ("Error occured when collection changes and composing required mqlCommands!");
				ex.printStackTrace();
			}
		}//for browsing input tasks list
			
		
		if (!foundAnyChange)
		{
			retVal = changedTasks;
			logger.debug (" No changes found for DB found!");
		}
		else
		{
			boolean transactionStarted = false;
			String mqlCommand ="start transcation";
			try
			{
				logger.debug ("Starting transcation");
				context.start(true);
				transactionStarted=true;
				
				for (TeeDataTask inputTask:commandsListByTask.keySet())
				{
					ArrayList<String> updateMqlCommandsList = commandsListByTask.get(inputTask);
				
					if (updateMqlCommandsList.size() == 0)
					{
						retVal.add (inputTask);
					}
					else
					{
						for (String cmd: updateMqlCommandsList)
						{
							mqlCommand = cmd;
							logger.debug (" running mql command: "+mqlCommand);
							String mqlRet = MqlUtil.mqlCommand (context, mqlCommand);
							logger.debug ("  retVal: "+mqlRet);
						}
					}
				}
				mqlCommand = "commit transcation";
				logger.debug ("Commiting transcation");	
				context.commit();
					
			}
			catch (Exception e)
			{
				logger.debug ("Error occured for mqlCommand: '"+mqlCommand+"'");
				e.printStackTrace();
				
				if (transactionStarted)
				{
					logger.debug ("Aborting transaction");
					try
					{
						context.abort();
					}
					catch (Exception ex){}
				}
			}
		}//else - some changes found
			

		
		//compute start/end date for updated task
		/*try
		{
			DBTasksSorter dbTasksSorter = new DBTasksSorter();
			dbTasksSorter.evaluateGanttStartEndDateForTasks(context,retVal);
		}
		catch (FrameworkException fwE)
		{
			fwE.printStackTrace();
		}*/
	
		logger.debug ("<<<updateTasks end");
		
		if (errorMessage.length() >0)
		{
			logger.debug (" got error message ("+errorMessage+") - throwing exception");
			throw new FrameworkException(errorMessage);
		}
		
		
		return retVal;
	}
	
	public TeeDataTask createNewTaskForProject(Context context, String projectID, int orderID)
	{
		TeeDataTask retVal = null;
		
		boolean transactionStarted = false;
		try
		{
			context.start(true);
			transactionStarted = true;
			Date date = new Date();
			String timeStamp = ""+date.getTime();
			ArrayList<String> routeIDsList = getAllRoutesForProject (context,projectID);
			
			String routeID = routeIDsList.get(0);
			String routeTaskUserID = getRouteTaskUserIDForRoute(context, routeID, true /*create Route Task User if there's none*/);
			
			Route route = new Route(routeID);
			BusinessObject personObj = new BusinessObject(routeTaskUserID);

			//create Route Node connection
			Relationship relationShipRouteNode = route.connect(context, new RelationshipType(DomainObject.RELATIONSHIP_ROUTE_NODE),true,personObj);
			AttributeList attrList = getAttrListForNewTask(context, orderID, timeStamp, false);
			relationShipRouteNode.open(context);
			relationShipRouteNode.setAttributes(context,attrList);
			relationShipRouteNode.close(context);
			
			//get connection ID
			String mqlCommand = "query connection relationship 'Route Node' where \"from.id=='"+routeID+"' and attribute[Title]=='"+timeStamp+"'\" select relationship id attribute[Route Node ID] dump |";
			String mqlRet = MqlUtil.mqlCommand (context, mqlCommand);
			String relTokens[] = mqlRet.split("\\|");
			String relID = relTokens[1];

			
			MqlUtil.mqlCommand (context, "mod connection "+relID+" 'Route Node ID' "+relID);
			
			//create InboxTask
			mqlCommand = "execute program eServicecommonNumberGenerator.tcl 'type_InboxTask' '' 'policy_TCETEETask' 'Null' 'eService Production'";
            String strInboxRes = MqlUtil.mqlCommand(context, mqlCommand, false);         

            StringList slTokens = FrameworkUtil.split( strInboxRes, "|");
            String sInboxErrorCode = ((String) slTokens.get(0)).trim();

            String sInboxTaskId = ((String) slTokens.get(1)).trim();
            String sInboxTaskType = ((String) slTokens.get(2)).trim();
            String sInboxTaskName = ((String) slTokens.get(3)).trim();
            String sInboxTaskRev = ((String) slTokens.get(4)).trim();
			
			BusinessObject taskBO = new BusinessObject(sInboxTaskId);
			taskBO.open(context);
			taskBO.connect(context, new RelationshipType(DomainObject.RELATIONSHIP_PROJECT_TASK),true,personObj);
			taskBO.connect(context, new RelationshipType(DomainObject.RELATIONSHIP_ROUTE_TASK),true,route);
			attrList = getAttrListForNewTask(context, orderID, timeStamp, true);
			taskBO.setAttributes(context, attrList);
			taskBO.close(context);
			
			//set Title and 'Route Node ID' attributes for 'Route Node' and 'Inbox Task'
			MqlUtil.mqlCommand (context, "mod connection "+relID+" 'Route Node ID' "+relID+" Title NewTask");
			MqlUtil.mqlCommand (context, "mod bus "+sInboxTaskId+" 'Route Node ID' "+relID+" Title NewTask");
			
			retVal = getTaskByID(context, sInboxTaskId, true);
			
			//compute start/end date for new task
			ArrayList<TeeDataTask> inputListForDBTasksSorter = new ArrayList<TeeDataTask>();
			inputListForDBTasksSorter.add(retVal);
			/*DBTasksSorter dbTasksSorter = new DBTasksSorter();
			dbTasksSorter.evaluateGanttStartEndDateForTasks(context,inputListForDBTasksSorter);*/
			
			context.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if (transactionStarted)
			{
				try
				{
					context.abort();
				}
				catch(MatrixException mxE){}
			}
		}
		
		return retVal;
	}
	
	public void destroyTask (Context context, TeeDataTask taskToDestroy) throws FrameworkException
	{
		Logger logger = TEELogger.getUserLogger(context.getUser());
		
		String objectId = taskToDestroy.getValue("SELECT_ID");
		String title = taskToDestroy.getValue("SELECT_TITLE");
		logger.debug ("Destroying task Title: "+title+", oid="+objectId);
		
		
		TeeDataTask currentTask = null;
		String errorMessage="";
		try
		{
			String mqlCommand, mqlRet;
			currentTask = getTaskByID (context, objectId, taskToDestroy.getIsTask());
			String taskStatus = currentTask.getValue("SELECT_CURRENT");
			
			if (taskStatus != null && "ToDo".equalsIgnoreCase(taskStatus) ==false)
			{
				//only Gantt order can be changed here!!!
				logger.debug (" !! Task is in state: '"+taskStatus+"' task cannot be removed!");
				errorMessage = "Cannot delete task '"+title+"' with state: "+taskStatus;
			}
		}
		catch (Exception ex)
		{
			System.err.println (" - Error when evaluating current info about task to be removed");
			ex.printStackTrace();
		}
		
		if (errorMessage.length() > 0)
		{
			throw new FrameworkException (errorMessage);
		}
		
		String connectionID_RouteToPerson = taskToDestroy.getValue("SELECT_ROUTE_NODE_ID");
		
		String mqlCommand = "start transaction";
		boolean transactionStarted = false;
		try
		{
			logger.debug ("starting transaction");
			context.start(true);
			transactionStarted = true;
			mqlCommand = "delete connection "+connectionID_RouteToPerson;
			logger.debug ("running mqlCommand: "+mqlCommand);
			String mqlRet = MqlUtil.mqlCommand (context, mqlCommand);
			if (taskToDestroy.getIsTask())
			{
				mqlCommand = "delete bus "+objectId;
				logger.debug ("running mqlCommand: "+mqlCommand);
				mqlRet = MqlUtil.mqlCommand (context, mqlCommand);
			}
			logger.debug ("commiting transaction");
			context.commit();
		}
		catch (Exception ex)
		{
			System.err.println (" - Error when destrying task, mqlCommand:"+mqlCommand);
			ex.printStackTrace();
			if (transactionStarted)
			{
				try{context.abort();}catch(Exception e){}
			}
		}
		
		
		
	}
	
	
	public ArrayList<TeeDataTask> getUserActiveTasksForKanbanColumn(Context context, String kanbanColumnName,String kanbanOrderBy)
	{
		setTaskSelects();
		
		String mqlCommand = constructMqlCommandForGetUserActiveTasksInKanbanColumn(context, kanbanColumnName,kanbanOrderBy);
		ArrayList<TeeDataTask>  retVal = new ArrayList<TeeDataTask> ();
		try
		{
			String mqlRet = MqlUtil.mqlCommand (context, mqlCommand);
			retVal = processTasksQueryResultString(context,mqlRet,3 /*task BO type,name,revision fields are always output*/);
			
			int columnsCount = TeeSettings.getKanbanSettings().getColumnsList().size();
			int columnPos = TeeSettings.getKanbanSettings().getColumnPositionByColumnID(kanbanColumnName);
			
			if (columnPos != (columnsCount -1))
			{
				//it is not last column - check whether task(s) for this column is(are) in critical chain
				// (tasks in last column are 'done' => they are not in critical chain - there's no point in calculating critical chain for them)
				DBTasksSorter dbTasksSorter = new DBTasksSorter();
				dbTasksSorter.evaluatedWhetherKanbanTasksInCriticalChain(context, retVal);	
			}
			
		}
		catch (FrameworkException fwE)
		{
			fwE.printStackTrace();
		}
		
		return(retVal);
	}
	
	public ArrayList<TeeDataTask> getAllTasksForProject(Context context, String projectID)
	{
		ArrayList<TeeDataTask> retVal = new ArrayList<TeeDataTask>();
		ArrayList<String> routeIDsList = getAllRoutesForProject (context,projectID);
		
		for (String routeID: routeIDsList)
		{
			try
			{
				setRouteNodeSelects();
				String mqlCommand = constructMqlCommandForGetAllRouteNodesForRoute(routeID);

				//String mqlRet = MQLCommand.exec(context, mqlCommand, "");
				String mqlRet = MqlUtil.mqlCommand (context, mqlCommand);
				ArrayList<TeeDataTask> routeNodesForRoute = processTasksQueryResultString(context,mqlRet,1 /*connection type is always output*/);
				
				
				setTaskSelects();
				mqlCommand = constructMqlCommandForGetAllTasksForRoute(routeID); 
				//mqlRet = MQLCommand.exec(context, mqlCommand, "");
				mqlRet = MqlUtil.mqlCommand (context, mqlCommand);
				ArrayList<TeeDataTask> tasksForRoute = processTasksQueryResultString(context,mqlRet,6 /*skip 'extract bus' tokens*/);
				
				mergeRouteNodesAndTasksIntoResultList (routeID, routeNodesForRoute, tasksForRoute, retVal);
				
				DBTasksSorter dbTasksSorter = new DBTasksSorter();
				dbTasksSorter.evaluateGanttStartEndDateForTasks(context, retVal);
				
			}
			catch (FrameworkException fwE)
			{
				fwE.printStackTrace();
			}
		}
		
		return(retVal);
	}
	
	
	
	protected AttributeList getAttrListForNewTask(Context context, int taskOrder, String timeStamp, boolean isTask)
	{
		AttributeList attrList = new AttributeList();
		attrList.add(new Attribute(new AttributeType(DomainObject.ATTRIBUTE_TITLE),timeStamp));
		attrList.add(new Attribute(new AttributeType(DomainObject.ATTRIBUTE_ROUTE_INSTRUCTIONS),"New Task Instructions"));
		if (isTask == false)
		{//only for Route Node
			attrList.add(new Attribute(new AttributeType(DomainObject.ATTRIBUTE_ROUTE_SEQUENCE),"1"));
			attrList.add(new Attribute(new AttributeType(PropertyUtil.getSchemaProperty(context, "attribute_ParallelNodeProcessionRule")),"All"));
		}
		attrList.add(new Attribute(new AttributeType(DomainObject.ATTRIBUTE_ROUTE_ACTION),"Approve"));
		attrList.add(new Attribute(new AttributeType(DomainObject.ATTRIBUTE_DUEDATE_OFFSET),"1d"));
		attrList.add(new Attribute(new AttributeType(PropertyUtil.getSchemaProperty(context, "attribute_TemplateTask")),"Yes"));
		attrList.add(new Attribute(new AttributeType(DomainObject.ATTRIBUTE_REVIEW_TASK),"Yes"));
		
		attrList.add(new Attribute(new AttributeType(DomainObject.ATTRIBUTE_DATE_OFFSET_FROM),"Task Create Date"));
		
		attrList.add(new Attribute(new AttributeType("TCETEEOrder"),""+taskOrder));
		attrList.add(new Attribute(new AttributeType("TCETEEUserPriority"),""+taskOrder));
		
		return attrList;
	}
	
	public static String getRouteTaskUserIDForRoute(Context context, String routeID, boolean createIfDoesNotExist) throws FrameworkException
	{
		String mqlCommand = "expand bus '"+routeID+"' relationship 'Route Node' type 'Route Task User' select bus id dump |";
		String mqlRet = MqlUtil.mqlCommand (context, mqlCommand).trim();
		
		String routeTaskUserId = null;
		
		if (mqlRet != null && mqlRet.length() > 0)
		{
			//get first found 'Route Task User'
			String lines[] = mqlRet.split("\n");
			String lineTokens[] = lines[0].split ("\\|");
			routeTaskUserId = lineTokens[6];
		}
		else if(createIfDoesNotExist)
		{
			//no 'Route Task User' is linked to the Route => create a new one
			DomainObject dmoRTU = new DomainObject();
			dmoRTU.createObject(context, DomainObject.TYPE_ROUTE_TASK_USER, null, null, DomainObject.POLICY_ROUTE_TASK_USER, null);
			routeTaskUserId = dmoRTU.getObjectId();
		}
		return routeTaskUserId;
	}
	
	
	protected void mergeRouteNodesAndTasksIntoResultList(String routeID,
			ArrayList<TeeDataTask> routeNodesList, 
			ArrayList<TeeDataTask> tasksList, 
			ArrayList<TeeDataTask> resultList)
	{
		routeNodesLoop:
		for (TeeDataTask routeNode: routeNodesList)
		{
			String routeNodeId = routeNode.getValues().get("SELECT_ID");
			for (TeeDataTask task: tasksList)
			{
				String taskRouteNodeID = task.getValues().get("SELECT_ROUTE_NODE_ID");
				if (routeNodeId.equals(taskRouteNodeID))
				{
					// duo 20150330: TEE-265: Gantt will show only task with this policy (TCETEETask)
					if (!("TCETEETask".equals(task.getValues().get("SELECT_POLICY"))))
						continue routeNodesLoop;
					// duo 20150330 end
					routeNode.getValues().putAll(task.getValues());
					routeNode.setIsTask(true);
					break;
				}
			}
			
			routeNode.setRouteID(routeID);
			resultList.add(routeNode);
		}
	}
	
	public static ArrayList<String> getAllRoutesForProject (Context context, String projectID)
	{
		ArrayList<String> retVal = new ArrayList<String>();
		String mqlCommand = "expand bus "+projectID+ " from relationship 'Route Scope' select bus id dump |";
		
		try
		{
			//String mqlRet = MQLCommand.exec(context, mqlCommand, "");
			String mqlRet = MqlUtil.mqlCommand(context, mqlCommand);
			String[]lines = mqlRet.split("\n");
			for (String line: lines)
			{
				if (line == null || line.trim().equals(""))
					continue;
				String[] tokens = line.split("\\|");
				String level = tokens[0];
				String fromType = tokens[1];
				String direction = tokens[2];
				String toType = tokens[3];
				String toName = tokens[4];
				String toRevision = tokens[5];
				String toID = tokens[6];
				retVal.add(toID);
			}
		}
		catch (FrameworkException fwE)
		{
			fwE.printStackTrace();
		}
		
		return retVal;
	}

	
	// TODO: update this method - evaluate correct state and correct action (promote/demote);
	//	update Route Node link (if necessary)
	//	also check whether route is being updated correctly
	public int updateTaskState(Context context, String taskID, String routeNodeID,String action,int column,int count)
	{
		Logger logger = TEELogger.getUserLogger(context.getUser());
		int retValue = 0;
		String mqlCommand = "";
		
		int kanbanColumnsCount = TeeSettings.getKanbanSettings().getColumnsList().size();
		
		if(action.equalsIgnoreCase("PROMOTE"))
			mqlCommand = "promote bus "+taskID;
		if(action.equalsIgnoreCase("DEMOTE"))
			mqlCommand = "demote bus "+taskID;	
		if(mqlCommand.length() != 0)
		{
			for(int i=0;i<count;i++)
			{
				try
				{
					//MqlUtil.mqlCommand (context, "set context user creator;");
					//MqlUtil.mqlCommand (context, "trigger off;");
					String mqlRet = MqlUtil.mqlCommand (context, mqlCommand);
					logger.debug ("mqlRet ="+mqlRet);
					//MqlUtil.mqlCommand (context, "set context user creator;trigger on;");
				}
				catch (FrameworkException mxE)
				{
					mxE.printStackTrace();
					retValue = 1;
				}
			}
			
			//if necessary, update start/end dates
			try
			{
				if(action.equalsIgnoreCase("promote"))
				{
					//check whether start date is set, if no - set it
					String startDate = MqlUtil.mqlCommand (context, "print bus "+taskID+" select attribute[TCETEEStartDate] dump |");
					if (startDate.length() == 0)
					{
						SimpleDateFormat formatterTest   = new SimpleDateFormat (eMatrixDateFormat.getInputDateFormat(),Locale.US);
						startDate = formatterTest.format(new Date().getTime());
						
						MqlUtil.mqlCommand (context, "mod bus "+taskID+" TCETEEStartDate '"+startDate+"'");
						MqlUtil.mqlCommand (context, "mod connection "+routeNodeID+" TCETEEStartDate '"+startDate+"'");
					}
					
					if (column == kanbanColumnsCount -1)
					{
						//target column is last kanban column => set End Date
						SimpleDateFormat formatterTest   = new SimpleDateFormat (eMatrixDateFormat.getInputDateFormat(),Locale.US);
						String endDate = formatterTest.format(new Date().getTime());
						MqlUtil.mqlCommand (context, "mod bus "+taskID+" 'Actual Completion Date' '"+endDate+"'");
						MqlUtil.mqlCommand (context, "mod connection "+routeNodeID+" 'Actual Completion Date' '"+endDate+"'");
					}
				}
				if( action.equalsIgnoreCase("demote")) 
				{
					
					//if ((column==1 && count == 1) || 	//demoting from Done to InWork 
					//	  (column==0 && count == 2) 	//demoting from Done to ToDo
					//   )
					if (column == ((kanbanColumnsCount-1)-count))
					{
						//demoting from last kanban column => clear end date
						MqlUtil.mqlCommand (context, "mod bus "+taskID+" 'Actual Completion Date' ''");
						MqlUtil.mqlCommand (context, "mod connection "+routeNodeID+" 'Actual Completion Date' ''");
					}
				}
			}
			catch (FrameworkException mxE)
			{
				mxE.printStackTrace();
				retValue = 1;
			}
			
			
			//Update all Parts and Documents 
			try
			{
				DomainObject taskBO = new DomainObject(taskID);
				taskBO.open(context);	
				String policy = taskBO.getPolicy(context).getName();
	        	String symbolicPolicyName = FrameworkUtil.getAliasForAdmin(context, "policy", policy,true);    						
				
				ArrayList<String> listAllRels = gelAllRelationsObjectsIDForTask(context,taskID);
				for(int r=0;r<listAllRels.size();r++)
				{				
					KanbanSettings kanbanSettings = TeeSettings.getKanbanSettings();	
					String relObjID = listAllRels.get(r);
					try
					{
						DomainObject targetObj = new DomainObject(relObjID);
						targetObj.open(context);
						String typeName = targetObj.getTypeName();
		        		String symbolicTypeName = FrameworkUtil.getAliasForAdmin(context, "type", typeName,true);
						String targetState = kanbanSettings.getTargetStateForKanbanColumnRelatedObject(column,symbolicPolicyName,symbolicTypeName);
						if(targetState.length() != 0)
						{
							String mqlCommandForObj = "";
					    	StateList listStates = targetObj.getStates(context);
					    	int currentStateInt = 0;
					    	int targetStateInt = 0;
					    	for(int s=0;s<listStates.size();s++)
					    	{
					    		  State state =  (State)listStates.get(s);
					    		  String stateName = state.getName();
					    		  if(state.isCurrent())
					    			  currentStateInt = s;
					    			  
					    		  if(targetState.equalsIgnoreCase(stateName))
					    			  targetStateInt = s;
					    	}
				    	    if(targetStateInt > currentStateInt)
								mqlCommandForObj = "promote bus "+relObjID;
				    	    if(targetStateInt < currentStateInt)
								mqlCommandForObj = "demote bus "+relObjID;
							
			    	    	//MqlUtil.mqlCommand (context, "set context user creator;trigger off");
				    	    for(int ts=0;ts<Math.abs(targetStateInt-currentStateInt);ts++)
				    	    {
				    	    	//MqlUtil.mqlCommand (context,"set context user creator;" + mqlCommandForObj);
				    	    	String mqlRet =MqlUtil.mqlCommand (context,mqlCommandForObj);
				    	    	logger.debug ("promoted/demote relDoc, mqlRet ="+mqlRet);
				    	    }
			    	    	//MqlUtil.mqlCommand (context, "set context user creator;trigger on");
						}
					}
					catch (Exception exc)
					{
						exc.printStackTrace();
						retValue = 1;
					}				    
				}
			}
			catch (Exception exc)
			{
				exc.printStackTrace();
				retValue = 1;
			}	
		}
		else
		{
			retValue = 1;
		}
		return(retValue);
	}
	public int updatePartOrDocToState(Context context, String partOrDoc,String targetState)
	{
		int retValue = 0;
		
		return(retValue);
	}
	
	protected ArrayList<TeeDataTask> processTasksQueryResultString(Context context,String queryResult, int startIndex)
	{
		ArrayList<TeeDataTask>  retVal = new ArrayList<TeeDataTask> ();
		
		String[]lines = queryResult.split("\n");
		for (String line: lines)
		{
			if (line == null || line.trim().equals(""))
				continue;
			
			String[] tokens = line.split("\\|");
			
			TeeDataTask teeTask = new TeeDataTask();
			int strIndex = startIndex;
			for (String selectable:m_SelectablesList)
			{
				String selectableName = m_SelectablesToSelectableNamesMap.get(selectable);
				String value = "";
				if (strIndex < tokens.length)
				{
					value = tokens[strIndex];
				}
				strIndex++;

				teeTask.addValue(selectableName, value);
			}
			
			//process assignee info
		   	String assigneeType = teeTask.getValue("SELECT_ASSIGNEE_TYPE");
		   	if (assigneeType.equals("Person"))
		   	{
		   	}
		   	else
		   	{
		   		teeTask.addValue ("SELECT_ASSIGNEE","none");
		   	}
		   	ArrayList<String> listrelIDs = this.gelAllRelationsObjectsIDForTask(context, teeTask.getValue("SELECT_ID"));
		   	String relsIDStr = "";
		   	for(int rIds = 0;rIds < listrelIDs.size();rIds++)
		   	{
		   		relsIDStr += "|"+listrelIDs.get(rIds);
		   	}
		   	teeTask.addValue ("SELECT_REL_IDS",relsIDStr);
			
			retVal.add(teeTask);
		}

		return(retVal);
	}
	
	protected String constructMqlCommandForGetUserActiveTasksInKanbanColumn(Context context, String kanbanColumnName,String kanbanOrderBy)
	{
		KanbanSettings kbSettings = TeeSettings.getKanbanSettings();
		
		String currentUser = context.getUser();
		String where = "where \"(from[Project Task].to.name=='"+currentUser+"')";
		HashMap <String,ArrayList<String>> policyStatesMap = kbSettings.getPolicyStatesMapForKanbanColumn (kanbanColumnName);
		
		int policyIndex = 0;
		for (String policySymbName:policyStatesMap.keySet())
		{
			String policyName = PropertyUtil.getSchemaProperty(context,policySymbName);
			
			if (policyIndex == 0)
			{
				where += " and ((policy=='"+policyName+"' and (current=='";
			}
			else
			{
				where += ") or (policy=='"+policyName+"' and (current=='";
			}
			
			ArrayList<String> stateNamesList = policyStatesMap.get(policySymbName);
			int stateIndex =0;
			for (String stateSymbName: stateNamesList)
			{
				String stateName = PropertyUtil.getSchemaProperty(context,"policy",policyName,stateSymbName);
				if (stateIndex > 0)
				{
					where += " or current=='";
				}
				
				where += stateName+ "'";
				
				stateIndex++;
			}
			where += ")";
			
			policyIndex++;
		}
		
		if (policyIndex > 0)
		{
			where += "))";	
		}
		
		where +="\"";
		//TCETEEUserPriority
		
		String mqlCommand = "temp query bus 'Inbox Task' * * " + where;
		mqlCommand += " orderby attribute[" + kanbanOrderBy + "] select";
		for (String sel:m_SelectablesList)
		{
			mqlCommand+= " "+sel;
		}
		mqlCommand += " dump |";
		return mqlCommand;
	}
	
	
	protected String constructMqlCommandForGetAllRouteNodesForRoute(String routeID)
	{
		String mqlCommand = "query connection relationship 'Route Node' where \"from.id=='"+routeID+"'\"";
		mqlCommand += " select";
		for (String sel:m_SelectablesList)
		{
			mqlCommand+= " "+sel;
		}
		mqlCommand += " dump |";
		return mqlCommand;
	}	
	
	protected String constructMqlCommandForGetAllTasksForRoute (String routeID)
	{
		String mqlCommand = "expand bus "+routeID+ " to relationship 'Route Task' select bus";
		for (String sel:m_SelectablesList)
		{
			mqlCommand+= " "+sel;
		}
		//mqlCommand += " where 'policy==TCETEETask'";
		mqlCommand += " dump |";
		return mqlCommand;
	}

	protected String constructMqlCommandForGetTaskInfo (String taskID)
	{
		String mqlCommand = "print bus "+taskID+ " select ";
		for (String sel:m_SelectablesList)
		{
			mqlCommand+= " "+sel;
		}
		mqlCommand += " dump |";
		return mqlCommand;
	}
	
	protected String constructMqlCommandForGetRouteNodeInfo (String routeNodeID)
	{
		String mqlCommand = "print connection "+routeNodeID+ " select ";
		for (String sel:m_SelectablesList)
		{
			mqlCommand+= " "+sel;
		}
		mqlCommand += " dump |";
		return mqlCommand;
	}
	public ArrayList<String> gelAllRelationsObjectsIDForTask(Context context,String taskid)
	{
		Logger logger = TEELogger.getUserLogger(context.getUser());
		ArrayList<String> retValues = new ArrayList<String>();
		try
		{
			DomainObject taskBO = new DomainObject(taskid);
			if(taskBO.exists(context))//neni connection 
			{
				taskBO.open(context);	     
				    
				StringList selectStmts = new StringList(1);
				selectStmts.addElement(DomainConstants.SELECT_ID);
					  
				StringList selectRelStmts = new StringList(1);
				selectRelStmts.addElement(DomainConstants.SELECT_RELATIONSHIP_ID);
				    
			    MapList mlItemsDocuments = taskBO.getRelatedObjects(context,"TEE Task Relation","*", selectStmts,selectRelStmts,false,true,(short)1, "","",0);
			    //MapList mlItemsDocuments = taskBO.getRelatedObjects(context,"Task Document","*", selectStmts,selectRelStmts,false,true,(short)1, "","",0);
				for (int i=0; i<mlItemsDocuments.size(); i++)
			    {
				  Map docMap=(Map)mlItemsDocuments.get(i);
				  String objID= docMap.get("id").toString();
				  retValues.add(objID);
			    }
			}
		}
		catch(Exception exc)
		{
			  logger.error("Exception in gelAllRelationsObjectsIDForTask" + exc);
		}
		return(retValues);
	}
	  public void reorderKanbanTasksList(Context context, ArrayList<TeeDataTask> tasksList)
      {
			 Logger logger = TEELogger.getUserLogger(context.getUser());
             boolean transactionStarted = false;
             try
             {
                    context.start(true);
                    transactionStarted = true;
                    
                    int index = 0;
                    for (TeeDataTask task: tasksList)
                    {
                          String taskObjectID = task.getValue("SELECT_ID");
                          String routeNodeID = task.getValue("SELECT_ROUTE_NODE_ID");
                          String mqlRet = MqlUtil.mqlCommand (context, "mod bus "+taskObjectID+" TCETEEUserPriority "+index);
                          mqlRet = MqlUtil.mqlCommand (context, "mod connection "+routeNodeID+" TCETEEUserPriority "+index);
                          index++;
                    }
                    
                    context.commit();
             }
             catch (Exception e)
             {
                    logger.debug ("Error occured when reordering Kanban tasks list: "+tasksList);
                    e.printStackTrace();
                    
                    if (transactionStarted)
                    {
                          logger.error ("Aborting transaction");
                          try    {context.abort();} catch (MatrixException mxE) {}
                    }
             }
      }
	
}
