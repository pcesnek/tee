package com.transcat.tee.enovia;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.matrixone.apps.domain.util.ContextUtil;
import com.matrixone.apps.domain.util.FrameworkException;
import com.matrixone.apps.domain.util.MqlUtil;
import com.transcat.tee.utils.ReportData;
import com.transcat.tee.utils.TEELogger;

import org.apache.log4j.*; 

import matrix.db.*;

public class DBReport {

	public ArrayList<HashMap<String,String>> reportAllStartedTasks(Context context)
	{
		ReportData reportData = new ReportData(); 
		
		ArrayList<HashMap<String,String>> retVal = new ArrayList<HashMap<String,String>>();
		
		String mqlCommand = "temp query bus 'Inbox Task' * * where \"history.promote != '' && policy=='TCETEETask'\"";
		mqlCommand +=" select id attribute[Title] from[Route Task].to[Route].to[Route Scope].from[Workspace].name history dump |";
		
		String mqlRet = "";
		try
		{
			Logger logger = TEELogger.getUserLogger(context.getUser());
			mqlRet = MqlUtil.mqlCommand (context, mqlCommand);
			String[] lines = mqlRet.split("\n",-1);
			
			for (String line:lines)
			{
				if (line.trim().length() == 0)
				{
					continue;
				}
				logger.debug ("Got line: "+line);
				String[] lineTokens = line.split ("\\|",-1);
				
				String taskType = lineTokens[0];
				String taskName = lineTokens[1];
				String taskRev = lineTokens[2];
				String taskOID = lineTokens[3];
				String taskTitle = lineTokens[4];
				String projectNameForTask = lineTokens[5];
				
				logger.debug("Got task '"+taskTitle+"' from project '"+projectNameForTask+"'");
				int index = 1;
				boolean taskStarted = false;
				for (int i = 6; i < lineTokens.length; i++)
				{
					String historyRecord = lineTokens[i];
					HashMap<String,String> tokenized = tokenizeHistoryRecord(historyRecord);
					String historyRecordEvent = tokenized.get("event");
					String historyRecordState = tokenized.get("state");
					String historyRecordUser = tokenized.get("user");
					String historyRecordTime = tokenized.get("time");
					if ( ("promote".equals(historyRecordEvent) || "demote".equals(historyRecordEvent)) && "InWork".equals(historyRecordState) && taskStarted==false)
					{
						reportData.startWorkOnTask(taskOID, taskTitle, projectNameForTask, historyRecordUser, historyRecordTime);
						taskStarted = true;
					}
					else if (("promote".equals(historyRecordEvent) || "demote".equals(historyRecordEvent)) && "InWork".equals(historyRecordState))
					{
						reportData.reStartWorkOnTask(taskOID, historyRecordUser, historyRecordTime);
					}
					else if (("promote".equals(historyRecordEvent) || "demote".equals(historyRecordEvent)) && "InWork".equals(historyRecordState) == false)
					{
						reportData.stopWorkOnTask(taskOID, historyRecordTime);
					}
				}//for - parsing line tokens
			}// for - parsing lines
			
			retVal = reportData.getAllTasksRecords();
		}
		catch (FrameworkException fwE)
		{
			System.err.println ("Error[FrameworkException] in DBReport.reportAllStartedTasks:");
			fwE.printStackTrace();
		}
		
		return retVal;
	}
	
	public HashMap<String,String> tokenizeHistoryRecord(String historyRecord)
	{
		HashMap<String,String> retVal = new HashMap<String,String>();
		
		String delims = " - user: | time: | state: ";
		ArrayList<String> keys = new ArrayList<String>();
		keys.add("event");
		keys.add("user");
		keys.add("time");
		keys.add("state");
		
		if (historyRecord.startsWith("change owner"))
		{
			delims += "| owner: | was: ";
			keys.add("owner");
			keys.add("oldOwner");
		}
		
		if (historyRecord.startsWith("promote"))
		{
			//nothing to add
		}
		

		String[] tokens = historyRecord.split(delims,-1);
		int index = 0;
		for (String token:tokens)
		{
			String keyName;
			if (keys.size() > index)
			{
				keyName = keys.get(index);
			}
			else
			{
				keyName = "key-"+index;
			}
			retVal.put(keyName, token.trim());
			index++;
		}
		
		
		return retVal;
	}
	
	public static void main(String[] args)
	{
		try
		{			
			Context ctx = ContextUtil.getAnonymousContext();
			ContextUtil.pushContext(ctx);
			DBReport dbRep = new DBReport();
			dbRep.reportAllStartedTasks(ctx);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	
}
