package com.transcat.tee.enovia;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import matrix.db.Context;
import com.matrixone.apps.domain.util.FrameworkException;
import com.matrixone.apps.domain.util.MqlUtil;
import com.matrixone.apps.domain.util.eMatrixDateFormat;

import com.transcat.tee.data.TeeDataTask;
import com.transcat.tee.settings.*;
import com.transcat.tee.utils.*;

public class DBTasksSorter 
{
	protected static SimpleDateFormat m_DateFormat = new SimpleDateFormat (eMatrixDateFormat.getInputDateFormat(), Locale.US);
	GregorianCalendar m_Now;
	GregorianCalendar m_EarliestTaskMilestoneDate = null;
	String m_ProjectID = null;

	GanttChartSettings m_GanttSettings = (GanttChartSettings)TeeSettings.getSettings(GanttChartSettings.class);
	TEEWorkingTime m_TeeWorkTime = null;
	
	public DBTasksSorter()
	{
		m_TeeWorkTime = new TEEWorkingTime(m_GanttSettings.getStartWorkDayHour(), m_GanttSettings.getStartWorkDayMinute(), 
				m_GanttSettings.getEndWorkDayHour(), m_GanttSettings.getEndWorkDayMinute());
		m_TeeWorkTime.setWeekEndDaysList(m_GanttSettings.getWeekendDaysList());
		m_Now = new GregorianCalendar();
	}
	
	public void evaluatedWhetherKanbanTasksInCriticalChain(Context context, ArrayList<TeeDataTask> tasksList) throws FrameworkException
	{
		HashMap<String,ArrayList<TeeDataTask>> tasksByProject = groupKanbanTasksByProjects(tasksList);
		
		for (String projID: tasksByProject.keySet())
		{
			m_ProjectID = projID;
			if (m_ProjectID.length() == 0)
			{//no project ???
				//do not evaluate whether tasks are in critical chain for empty project 
				continue;
			}
			
			ArrayList<String> routesForProject = DBTask.getAllRoutesForProject(context, m_ProjectID);
			if (routesForProject.size() < 1)
			{
				//no route for project ???
				//do not evaluate whether tasks are in critical chain for project without route
				continue;
			}
			String routeID = routesForProject.get(0);
					
			String noneUserIdForRoute = DBTask.getRouteTaskUserIDForRoute(context, routeID, false);
			if (noneUserIdForRoute == null)
			{
				//this project's route has no task with user 'none' => there's no milestone there
				// => no critical chain for this project
				continue;
			}
			
			ArrayList<TeeDataTask> tasksListByProject = tasksByProject.get(m_ProjectID);
			String currentUserID = 	tasksListByProject.get(0).getValue("SELECT_ASSIGNEE_ID");//ArrayList shouldn't be empty as it is stored in grouped by Project HM
			
			String userIDsList = currentUserID+","+noneUserIdForRoute;
			
			String mqlCommand = constructMqlCommandForRetrieveAllTasksForUsers(userIDsList);

			String mqlRet = MqlUtil.mqlCommand (context, mqlCommand);
			HashMap<String,ArrayList<HashMap<String,String>>> recordListsGroupedByUser = tokenizeAndGroupReadDataByUser (mqlRet);

			
			evaluateFirstMilestoneDate (recordListsGroupedByUser);
			
			computeGanttStartEndDates (recordListsGroupedByUser);
			
			for (TeeDataTask task:tasksListByProject)
			{
				String routeNodeId = task.getValue("SELECT_ROUTE_NODE_ID");
				HashMap<String,String> recordForTask = getRecordFromDataByRouteNodeId(recordListsGroupedByUser,routeNodeId);
				if (recordForTask != null)
				{
					String taskState = task.getValue("SELECT_CURRENT");
					if ("Done".equals(taskState))
					{//if task is done - it is not in CRITICAL CHAIN
						task.addValue("SELECT_IS_IN_CRITICAL_CHAIN","false");
					}
					else
					{
						task.addValue("SELECT_IS_IN_CRITICAL_CHAIN",recordForTask.get("SELECT_IS_IN_CRITICAL_CHAIN"));
					}
				
					
				}
			}
		}// for - browsing projects 
			
	}
	
	protected HashMap<String,ArrayList<TeeDataTask>> groupKanbanTasksByProjects (ArrayList<TeeDataTask> tasksList)
	{
		HashMap<String,ArrayList<TeeDataTask>> retVal = new HashMap<String,ArrayList<TeeDataTask>>();
		
		for (TeeDataTask kanbanTask: tasksList)
		{
			String projID = kanbanTask.getValue("SELECT_PROJECT_ID");
			if (projID == null)
			{
				projID = "";
			}
			
			ArrayList<TeeDataTask> tasksListForProject = retVal.get(projID);
			if (tasksListForProject == null)
			{
				tasksListForProject = new ArrayList<TeeDataTask>();
				retVal.put(projID,tasksListForProject);
			}
			tasksListForProject.add(kanbanTask);
		}
		
		return retVal;
	}
	
	public void evaluateGanttStartEndDateForTasks(Context context, ArrayList<TeeDataTask> tasksList) throws FrameworkException
	{
		if (tasksList == null || tasksList.isEmpty())
		{
			return;
		}
		
		
		//assuming that all input tasks are in the same project - get project id of the first one...
		m_ProjectID = tasksList.get(0).getValue("SELECT_PROJECT_ID");

		/*
		//assuming that all input tasks are in the same route - get route id of the first one...
		String routeId = tasksList.get(0).getRouteID();*/
		
		String userIDsList = getRequiredUsersFromInputTasksList(tasksList);
		
		String mqlCommand = constructMqlCommandForRetrieveAllTasksForUsers(userIDsList);
		String mqlRet = MqlUtil.mqlCommand (context, mqlCommand);
		HashMap<String,ArrayList<HashMap<String,String>>> recordListsGroupedByUser = tokenizeAndGroupReadDataByUser (mqlRet);


		evaluateFirstMilestoneDate (recordListsGroupedByUser);
		
		computeGanttStartEndDates (recordListsGroupedByUser);
		
		for (TeeDataTask task:tasksList)
		{
			String routeNodeId = task.getValue("SELECT_ROUTE_NODE_ID");
			HashMap<String,String> recordForTask = getRecordFromDataByRouteNodeId(recordListsGroupedByUser,routeNodeId);
			if (recordForTask != null)
			{
				task.addValue("GANTT_START",recordForTask.get("GANTT_START"));
				task.addValue("GANTT_END",recordForTask.get("GANTT_END"));
				task.addValue("GANTT_PERCENT_COMPLETE",recordForTask.get("GANTT_PERCENT_COMPLETE"));
				
				String taskState = task.getValue("SELECT_CURRENT");
				if ("Done".equals(taskState))
				{//if task is done - it is not in CRITICAL CHAIN
					task.addValue("SELECT_IS_IN_CRITICAL_CHAIN","false");
				}
				else
				{
					task.addValue("SELECT_IS_IN_CRITICAL_CHAIN",recordForTask.get("SELECT_IS_IN_CRITICAL_CHAIN"));
				}
			
				
			}
		}
		
		
		
	}
	
	protected String getRequiredUsersFromInputTasksList(ArrayList<TeeDataTask> tasksList)
	{
		ArrayList<String> userIDsList = new ArrayList<String>();
		
		for (TeeDataTask task: tasksList)
		{
			String userID = task.getValue("SELECT_ASSIGNEE_ID");
			if (userID != null && userID.length() > 0)
			{
				if (userIDsList.contains(userID) == false)
				{
					userIDsList.add(userID);
				}
			}
		}
		
		String retVal = "";
		for (String userID:userIDsList)
		{
			if (retVal.length() > 0)
			{
				retVal+=",";
			}
			retVal+= userID;
		}
		
		return retVal;
	}
	
	protected String constructMqlCommandForRetrieveAllTasksForUsers(String strUsersList)
	{
		//get all users on the project
		
		
		//construct mql command - begin	
	//next 'where' definition line was replaced 
	// - we will collect only tasks from TEE projects, otherwise some select (e.g. from.to[Route Scope].from[Workspace].id) aren't valid, no value is returned and then the index of the select doesn't match!!
		//String where = "where \"to.id matchlist '"+strUsersList+"' ','\"";
		String where = "where \"to.id matchlist '"+strUsersList+"' ',' and from.to[Route Scope].from[Workspace].attribute[TCETEEManaged]=='TRUE'\"";
		
		String orderby =" ";
		orderby += "orderby to.id ";								//first - group by users
		orderby += "orderby attribute[TCETEEUserPriority]";			//second - sort ascending by user priority
		
		String selects = "select";								//index 0 - link name (Route Node)
		selects += " id";										//index 1 - Route Node ID
		selects += " from.to[Route Scope].from[Workspace].id";	//index 2 - Project(Workspace) ID
		selects += " to.id";									//index 3 - User ID
		selects += " attribute[TCETEEStartDate]";				//index 4 - StartDate
		selects += " attribute[Actual Completion Date]";		//index 5 - EndDate
		selects += " attribute[Due Date Offset]";				//index 6 - Estimated Duration
		selects += " attribute[TCETEEUserPriority]";			//index 7 - User Priority
		selects += " attribute[TCETEEMilestoneDate]";			//index 8 - Milestone Date
		selects += " dump |";
		String mqlCommand = "query connection relationship 'Route Node' "+where+" "+orderby+" "+selects;
		//construct mql command - end
		
		return mqlCommand;
	}

	protected HashMap<String,ArrayList<HashMap<String,String>>> tokenizeAndGroupReadDataByUser(String data)
	{
		String lines[] =  data.split("\n");
		HashMap<String,ArrayList<HashMap<String,String>>> recordListsGroupedByUser = new HashMap<String,ArrayList<HashMap<String,String>>>();
		for (String line:lines)
		{
			String tokens[] = line.split("\\|",-1);
			String routeNodeID = tokens[1];
			String projectID = tokens[2];
			String userID = tokens[3];
			String startDate = tokens[4];
			String endDate = tokens[5];
			String estimatedDuration = tokens[6];
			String userPriority = tokens[7];
			String milestoneDate = "";
			if (tokens.length == 9) //if last value is empty, then the split command will not give an empty token
			{
				milestoneDate = tokens[8];
			}
			
			HashMap <String,String> record = new HashMap <String,String>();
			record.put("routeNodeID", routeNodeID);
			record.put("projectID", projectID);
			record.put("userID", userID);
			record.put("startDate", startDate);
			record.put("endDate", endDate);
			record.put("estimatedDuration", estimatedDuration);
			record.put("userPriority", userPriority);
			record.put("milestoneDate", milestoneDate);
			
			ArrayList<HashMap<String,String>> recordListForUser = recordListsGroupedByUser.get(userID);
			if (recordListForUser == null)
			{
				recordListForUser = new ArrayList<HashMap<String,String>>();
				recordListsGroupedByUser.put (userID, recordListForUser);
			}
			
			recordListForUser.add(record);
		}//for - browsing lines
		return recordListsGroupedByUser;
	}

	protected void computeGanttStartEndDates(HashMap<String,ArrayList<HashMap<String,String>>> recordListsGroupedByUser)
	{
		
		for (String userID:recordListsGroupedByUser.keySet())
		{
			ArrayList<HashMap<String,String>> recordListForUser = recordListsGroupedByUser.get(userID);
			
			GregorianCalendar firstStartDateForUnstartedTasks = getStartDateForUserUnstartedTasks (recordListForUser);
			firstStartDateForUnstartedTasks = m_TeeWorkTime.getNearestWorkingTime(firstStartDateForUnstartedTasks);
			
			GregorianCalendar tempDate = (GregorianCalendar) firstStartDateForUnstartedTasks.clone();
			GregorianCalendar endTimeOfLastTaskForUserAndCurrentProject = null;
			for (HashMap<String,String> record: recordListForUser)
			{
				String startDate = record.get("startDate");
				String taskProjectID = record.get("projectID");
				
				GregorianCalendar taskStartDate = null; 
				
				if (startDate != null && startDate.length() > 0)
				{
					taskStartDate = parseStringDate(startDate); //if null, then parse exception
				}
				if (taskStartDate == null)
				{
					String milestoneDate = record.get("milestoneDate");
					GregorianCalendar taskMilestoneDate = null; 
					if (milestoneDate != null && milestoneDate.length() > 0)
					{
						taskMilestoneDate = parseStringDate(milestoneDate);
					}
					
					if (taskMilestoneDate != null)
					{
						record.put ("GANTT_START","/Date("+taskMilestoneDate.getTimeInMillis()+")/");
						record.put ("GANTT_END","/Date("+taskMilestoneDate.getTimeInMillis()+")/");
						record.put ("GANTT_PERCENT_COMPLETE","0");
					}
					else
					{
						String estimatedDuration = record.get("estimatedDuration");
						int dueDateOffSetinSecs = parseDueDateOffset(estimatedDuration);
						GregorianCalendar estimatedEndDate = m_TeeWorkTime.computeWorkingEndDateTime(tempDate, dueDateOffSetinSecs);
					
						record.put ("GANTT_START","/Date("+tempDate.getTimeInMillis()+")/");
						record.put ("GANTT_END","/Date("+estimatedEndDate.getTimeInMillis()+")/");
						
						if (m_ProjectID.equals(taskProjectID))
						{
							if (endTimeOfLastTaskForUserAndCurrentProject == null)
							{
								endTimeOfLastTaskForUserAndCurrentProject = estimatedEndDate;
							}
							else
							{
								if (endTimeOfLastTaskForUserAndCurrentProject.compareTo(estimatedEndDate) < 0)
								{
									endTimeOfLastTaskForUserAndCurrentProject = estimatedEndDate;
								}
							}
						}
						
						record.put ("GANTT_PERCENT_COMPLETE","0");
						tempDate = estimatedEndDate;
					}
				}//if - startDate is empty
				else
				{
					record.put ("GANTT_START","/Date("+taskStartDate.getTimeInMillis()+")/");
					String endDate = record.get("endDate");
					GregorianCalendar taskEndDate = null;
					if (endDate != null && endDate.length() > 0)
					{
						taskEndDate = parseStringDate(endDate);
					}
					
					
					if (taskEndDate == null)
					{//task is in progress
						
						record.put ("GANTT_PERCENT_COMPLETE","0");
						
						//evaluate estimated end date
						String estimatedDuration = record.get("estimatedDuration");
						
						int dueDateOffSetinSecs = parseDueDateOffset(estimatedDuration);
						GregorianCalendar estimatedEndDate = m_TeeWorkTime.computeWorkingEndDateTime(taskStartDate, dueDateOffSetinSecs);						
						if (estimatedEndDate.compareTo(m_Now) > 0)
						{
							taskEndDate = estimatedEndDate;
						}
						else
						{
							taskEndDate = m_Now;
						}
					}
					/*else
					{
					}*/
					
					record.put ("GANTT_PERCENT_COMPLETE","1");
					record.put ("GANTT_END","/Date("+taskEndDate.getTimeInMillis()+")/");
						
					if (m_ProjectID.equals(taskProjectID))
					{
						if (endTimeOfLastTaskForUserAndCurrentProject == null)
						{
							endTimeOfLastTaskForUserAndCurrentProject = taskEndDate;
						}
						else
						{
							if (endTimeOfLastTaskForUserAndCurrentProject.compareTo(taskEndDate) < 0)
							{
								endTimeOfLastTaskForUserAndCurrentProject = taskEndDate;
							}
						}
					}
				}
			}//for - browsing records in recordList
			
			
			//evaluate whether in critical chain
			if (m_EarliestTaskMilestoneDate != null && endTimeOfLastTaskForUserAndCurrentProject != null)
			{
				if (endTimeOfLastTaskForUserAndCurrentProject.compareTo(m_EarliestTaskMilestoneDate) > 0)
				{
					for (HashMap<String,String> record: recordListForUser)
					{
						record.put("SELECT_IS_IN_CRITICAL_CHAIN", "true");
					}
				}
				else
				{
					for (HashMap<String,String> record: recordListForUser)
					{
						record.put("SELECT_IS_IN_CRITICAL_CHAIN", "false");
					}
				}
			}
			
		}//for - browsing users grouped recordLists
	}
	
	protected int parseDueDateOffset(String offset) {
		String reformated = offset;
//		if (!offset.endsWith("d") && !offset.endsWith("h")){
//			reformated += "d";
//		}
		DecimalFormat format=(DecimalFormat)DecimalFormat.getInstance();
		char separator = format.getDecimalFormatSymbols().getDecimalSeparator();
		reformated = reformated.replaceAll("[, .]", String.valueOf(separator));

		final int workDayDurationInSecs = m_GanttSettings.getWorkingDayDurationInMillis()/1000;
		
		Map<String, Integer> fields = new HashMap<String, Integer>() {{
			put("", 3600);
		    put("h", 3600);
		    put("d", workDayDurationInSecs);
		}};

		Pattern p = Pattern.compile("(\\d{1,2}[.,]?\\d{0,2}?)([hd]?)");
	    Matcher m = p.matcher(reformated);
	    if (m.matches()) {
	    	double amount = Double.parseDouble(m.group(1));
	    	return (int)Math.round(fields.get(m.group(2)) * amount);
	    }
	    
	    return 0;
	    
	    //throw new NumberFormatException("For input string: \"" + offset + "\"");
	}
	
	protected GregorianCalendar getStartDateForUserUnstartedTasks(ArrayList<HashMap<String,String>> recordListForUser)
	{
		GregorianCalendar retVal = null;
		
		for (HashMap<String,String> record: recordListForUser)
		{
			String startDate = record.get("startDate");
			if (startDate == null || startDate.length() == 0)
			{
				continue;
			}
			GregorianCalendar taskStartDate = parseStringDate(startDate);

			if (taskStartDate == null)
			{//parse exception ??
				continue;
			}
			
			String endDate = record.get("endDate");
			GregorianCalendar taskEndDate = null;
			if (endDate != null && endDate.length() > 0)
			{
				taskEndDate = parseStringDate(endDate);
			}
			
			if (taskEndDate == null)
			{//task is in progress
				//evaluate estimated end date
				String estimatedDuration = record.get("estimatedDuration");
				int dueDateOffSetinSecs = parseDueDateOffset(estimatedDuration);
				GregorianCalendar estimatedEndDate = m_TeeWorkTime.computeWorkingEndDateTime(taskStartDate, dueDateOffSetinSecs);
				
				if (estimatedEndDate.compareTo(m_Now) > 0)
				{
					if (retVal == null)
					{
						retVal = estimatedEndDate;
					}
					else if (estimatedEndDate.compareTo(retVal) > 0)
					{
						retVal = estimatedEndDate;
					}
				}
			}
			else
			{
				if (taskEndDate.compareTo(m_Now) > 0)
				{
					if (retVal == null)
					{
						retVal = taskEndDate;
					}
					else if (taskEndDate.compareTo(retVal) > 0)
					{
						retVal = taskEndDate;
					}
				}
			}
		}
		
		if (retVal == null)
		{
			retVal = m_Now;
		}
		
		
		
		return retVal;
	}
	
	protected HashMap<String,String> getRecordFromDataByRouteNodeId(HashMap<String,ArrayList<HashMap<String,String>>> data, String routeNodeID)
	{
		HashMap<String,String> record = null;
		
		for (String userID: data.keySet())
		{
			ArrayList<HashMap<String,String>> userRecordList = data.get(userID);
			for (HashMap<String,String> storedRecord:userRecordList)
			{
				String storedRouteNodeId = storedRecord.get("routeNodeID");
				if (routeNodeID.equals(storedRouteNodeId))
				{
					record = storedRecord;
					break;
				}
			}
		}
		
		return record;
	}
	
	
	public GregorianCalendar parseStringDate(String strDate)
	{
		GregorianCalendar retVal = new GregorianCalendar();
		try
		{
			retVal.setTime(m_DateFormat.parse(strDate));
		}
		catch (ParseException pe)
		{
			System.err.println ("ParseException "+pe+" when parsing string date "+strDate+" using format "+eMatrixDateFormat.getInputDateFormat());
			retVal = null;
		}
		catch (Exception ex)
		{
			System.err.println ("UnexpectedException "+ex+" when parsing string date "+strDate+" using format "+eMatrixDateFormat.getInputDateFormat());
			ex.printStackTrace();
			throw ex;
		}
		return retVal;
	}
	
	protected void evaluateFirstMilestoneDate(HashMap<String,ArrayList<HashMap<String,String>>> recordListsGroupedByUser)
	{
		//get first milestonedate
		m_EarliestTaskMilestoneDate = null;
		for (String userID:recordListsGroupedByUser.keySet())
		{
			ArrayList<HashMap<String,String>> recordListForUser = recordListsGroupedByUser.get(userID);
			for (HashMap<String,String> record: recordListForUser)
			{
				String startDate = record.get("startDate");
				
				GregorianCalendar taskStartDate = null; 
				
				if (startDate != null && startDate.length() > 0)
				{
					taskStartDate = parseStringDate(startDate); //if null, then parse exception
				}
				if (taskStartDate == null)
				{
					String milestoneDate = record.get("milestoneDate");
					GregorianCalendar taskMilestoneDate = null;
					if (milestoneDate != null && milestoneDate.length() > 0)
					{
						taskMilestoneDate = parseStringDate(milestoneDate);
					}
					
					if (taskMilestoneDate != null)
					{
						if (m_EarliestTaskMilestoneDate == null)
						{
							m_EarliestTaskMilestoneDate = taskMilestoneDate;
						}
						else
						{
							if (m_EarliestTaskMilestoneDate.compareTo(taskMilestoneDate) > 0)
							{
								m_EarliestTaskMilestoneDate = taskMilestoneDate;
							}
						}
					}
				}
			}
		}
	}
}
