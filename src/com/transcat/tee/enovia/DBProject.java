package com.transcat.tee.enovia;

import java.util.*;

import javax.servlet.http.HttpSession;

import com.transcat.tee.data.*;
import com.transcat.tee.settings.*;
import com.transcat.tee.utils.*;
import com.matrixone.apps.common.Route;
import com.matrixone.apps.common.Workspace;
import com.matrixone.apps.domain.DomainAccess;
import com.matrixone.apps.domain.DomainConstants;
import com.matrixone.apps.domain.DomainObject;
import com.matrixone.apps.domain.util.ContextUtil;
import com.matrixone.apps.domain.util.FrameworkException;
import com.matrixone.apps.domain.util.FrameworkUtil;
import com.matrixone.apps.domain.util.MapList;
import com.matrixone.apps.domain.util.MqlUtil;
import com.matrixone.apps.domain.util.PersonUtil;
import com.matrixone.apps.domain.util.PropertyUtil;
import com.matrixone.apps.team.TeamUtil;
import com.matrixone.apps.team.WorkspaceTemplate;

import matrix.db.Context;
import matrix.db.JPO;
import matrix.db.RelationshipType;
import matrix.db.State;
import matrix.db.StateList;
import matrix.util.MatrixException;
import matrix.util.StringList;

import org.apache.log4j.*; 

public class DBProject {

	protected ProjectSelectableSettings m_Settings;
	protected int m_ProjectContentIDCounter = 0;
	protected String m_realUrl;
	public DBProject(String realUrl) {
		m_Settings = TeeSettings.getProjectSelectableSettings();
		m_realUrl = realUrl;
	}

	public ArrayList<TeeDataObject> getProjectContent(Context context, String projectID) {
		ArrayList<TeeDataObject> retVal = new ArrayList<TeeDataObject>();
		HashMap<String, DomainObject> temRec = new HashMap<String, DomainObject>();
		try {
			m_ProjectContentIDCounter= 0;
			// DomainObject domObj = DomainObject.newInstance(context, objectId);
			DomainObject domObj = new DomainObject(projectID);
			StringList selectStmts = new StringList(2);
			selectStmts.addElement(DomainConstants.SELECT_ID);
			selectStmts.addElement(DomainConstants.SELECT_NAME);

			StringList selectRelStmts = new StringList(1);
			selectRelStmts.addElement(DomainConstants.SELECT_RELATIONSHIP_ID);

			// @formatter:off
			MapList mlItemsFolders = domObj.getRelatedObjects(context, "Data Vaults", // relationship pattern (String)
					"*", 				// type pattern (String)
					selectStmts, 		// objectSelects (StringList)
					selectRelStmts, 	// relationshipSelects (StringList)
					false, 				// getTo (bool)
					true, 				// getFrom (bool)
					(short) 1, 			// recurseToLevel (int)
					"", 				// busWhereClause (String)
					"", 				// relWhereClause (String)
					0); 				// limit (int)
			// @formatter:on
			mlItemsFolders.sort(DomainConstants.SELECT_NAME,"ascending","string");
			for (int i = 0; i < mlItemsFolders.size(); i++) {
				Map item = (Map) mlItemsFolders.get(i);
				String FID = item.get("id").toString();
				DomainObject folderObj = new DomainObject(FID);
				folderObj.open(context);
				TeeDataContent data = new TeeDataContent();
				//String guid = new Date().getTime()+"";
				//String guid = String.format("%03d", i +1);
				//String guid = String.format("%03d", i +1);
				String guid = ""+m_ProjectContentIDCounter++;
				data.addValue("id", "" + guid);
				data.addValue("docid", FID);
				data.addValue("projectID", projectID);
				data.addValue("SELECT_NAME", folderObj.getName());
				data.addValue("SELECT_TYPE", folderObj.getTypeName());
				data.addValue("TYPE_CLASS", folderObj.getTypeName().replaceAll("\\s",""));
				data.addValue("SELECT_DESCRIPTION", folderObj.getDescription());
				data.addValue("SELECT_OWNER", folderObj.getOwner().getName());
				data.addValue("SELECT_REVISION", folderObj.getRevision());
				data.addValue("parentId", "null");
				data.addValue("icon", getIcon(folderObj.getTypeName()));
				if(folderObj.getTypeName().equalsIgnoreCase("Workspace Vault")) {
				  data.addValue("SELECT_REVISION","");
				}
		    	  
				retVal.add(data);

				getFolderContent(context, retVal, temRec, folderObj, guid, projectID);
			}
		} catch (Exception exc) {
			exc.printStackTrace();
		}
		ArrayList<TeeDataObject> listDepDoc = retVal;
		return (retVal);

	}

	private int getFolderContent(Context context, ArrayList<TeeDataObject> listDocs,
			HashMap<String, DomainObject> listTempRec, DomainObject folderObj, String parentID, String projectID) 
	{
		int retVal = 0;
		try {
			
			Logger logger = TEELogger.getUserLogger(context.getUser());

			
			StringList selectStmts = new StringList(2);
			selectStmts.addElement(DomainConstants.SELECT_ID);
			selectStmts.addElement(DomainConstants.SELECT_NAME);

			StringList selectRelStmts = new StringList(1);
			selectRelStmts.addElement(DomainConstants.SELECT_RELATIONSHIP_ID);

			// @formatter:off
		    MapList mlItemsObjects = folderObj.getRelatedObjects(context,
		    		TeeSettings.getProjectContentSettings().getRelationships(),	// relationship pattern (String)
		    		TeeSettings.getProjectContentSettings().getTypes(),			// type pattern (String) 
		    		selectStmts,												// objectSelects (StringList)
		    		selectRelStmts,												// relationshipSelects (StringList)
		    		false,														// getTo (bool)
		    		true,														// getFrom (bool)
		    		(short)1, 													// recurseToLevel (int)
		    		"",															// busWhereClause (String)
		    		"",															// relWhereClause (String)
		    		0);															// limit (int)
		    
		    mlItemsObjects.sort(DomainConstants.SELECT_NAME,"ascending","string");
		    
		    retVal = mlItemsObjects.size();
		    // @formatter:on
			for (int d = 0; d < mlItemsObjects.size(); d++) {
				Map doc = (Map) mlItemsObjects.get(d);
				String DID = doc.get("id").toString();
				DomainObject docObj = new DomainObject(DID);
				docObj.open(context);
				TeeDataContent docData = new TeeDataContent();
				//String guid = new Date().getTime()+"";
				//String guid = parentID+String.format("%03d", d +1);
				String guid = ""+m_ProjectContentIDCounter++;
				docData.addValue("id", guid);
				docData.addValue("docid", DID);
				docData.addValue("projectID", projectID);
				docData.addValue("SELECT_NAME", docObj.getName());
				docData.addValue("SELECT_TYPE", docObj.getTypeName());
				docData.addValue("TYPE_CLASS", docObj.getTypeName().replaceAll("\\s",""));
				docData.addValue("SELECT_OWNER", docObj.getOwner().getName());
				docData.addValue("SELECT_DESCRIPTION", docObj.getDescription());
				docData.addValue("SELECT_REVISION", docObj.getRevision());
				// <p><b>This text is bold</b>.</p>
				String listStatesStr = "";
				StateList listStates = docObj.getStates(context);
				for (int s = 0; s < listStates.size(); s++) {
					State state = (State) listStates.get(s);
					if (state.isCurrent())
						listStatesStr += state.getName();
				}
				docData.addValue("SELECT_STATE", listStatesStr);
				docData.addValue("parentId", parentID);
				docData.addValue("icon", getIcon(docObj.getTypeName()));
		    	if(docObj.getTypeName().equalsIgnoreCase("Workspace Vault"))
		    		docData.addValue("SELECT_REVISION","");
		    	listDocs.add(docData);
		    	logger.debug ("Adding CONT: "+docData.getJSONDataObject());
				//if (!listTempRec.containsKey(DID)) {
					listTempRec.put(DID, docObj);
					int addedFolders = getFolderContent(context, listDocs, listTempRec, docObj, guid, projectID);
					int addedAssyComponents = 0;
					if (isObjectTypeAssembly(context,docObj.getTypeName()))
					{
						addedAssyComponents = getAssemblyStructure (context, listDocs, listTempRec, docObj, guid, projectID, addedFolders);
					}
					
					if (isObjectTypeComponent(context, docObj.getTypeName()))
					{
						getAssociatedDrawings (context, listDocs, listTempRec, docObj, guid, projectID, addedFolders+addedAssyComponents);
					}
				/*}
				else
				{
				}*/
			}
		} catch (Exception exc) {
			exc.printStackTrace();
		}
		return retVal;
	}
	
	
	protected int getAssemblyStructure(Context context, ArrayList<TeeDataObject> listDocs,
			HashMap<String, DomainObject> listTempRec, DomainObject assemblyObj, String parentID, String projectID, int startIndex)
	{
		int retVal = 0;
		try {
			Logger logger = TEELogger.getUserLogger(context.getUser());
			
			DomainObject doToExpand = getCorrectDomainObjectToBeSearchedForCADConnections (context, assemblyObj);
			
			StringList selectStmts = new StringList(2);
			selectStmts.addElement(DomainConstants.SELECT_ID);
			selectStmts.addElement(DomainConstants.SELECT_NAME);

			StringList selectRelStmts = new StringList(1);
			selectRelStmts.addElement(DomainConstants.SELECT_RELATIONSHIP_ID);

			// @formatter:off
		    MapList mlItemsObjects = doToExpand.getRelatedObjects(context,
		    		"CAD SubComponent",	// relationship pattern (String)
		    		"MCAD Model",			// type pattern (String) 
		    		selectStmts,												// objectSelects (StringList)
		    		selectRelStmts,												// relationshipSelects (StringList)
		    		false,														// getTo (bool)
		    		true,														// getFrom (bool)
		    		(short)1, 													// recurseToLevel (int)
		    		"",															// busWhereClause (String)
		    		"",															// relWhereClause (String)
		    		0);															// limit (int)
		    
		    mlItemsObjects.sort(DomainConstants.SELECT_NAME,"ascending","string");
		    retVal = mlItemsObjects.size();
		    // @formatter:on
			for (int d = 0; d < mlItemsObjects.size(); d++) {
				Map doc = (Map) mlItemsObjects.get(d);
				String DID = doc.get("id").toString();
				DomainObject docObj = new DomainObject(DID);
				docObj.open(context);
				
				DomainObject majorCADObj;
				String majorObjID;
				if (isCADObjectVersionedObject(context, DID))
				{
					majorObjID = getMajorObjectForVersionedCADObject(context, DID);
					majorCADObj =  new DomainObject(majorObjID);
					majorCADObj.open(context);
				}
				else
				{
					majorObjID = DID;
					majorCADObj = docObj;
				}
				
				
				TeeDataContent docData = new TeeDataContent();
				//String guid = new Date().getTime()+"";
				//String guid = parentID+String.format("%03d",startIndex+d+1);
				String guid = ""+m_ProjectContentIDCounter++;
				docData.addValue("id", guid);
				docData.addValue("docid", majorObjID);
				docData.addValue("projectID", projectID);
				docData.addValue("SELECT_NAME", majorCADObj.getName());
				docData.addValue("SELECT_TYPE", majorCADObj.getTypeName());
				docData.addValue("TYPE_CLASS", majorCADObj.getTypeName().replaceAll("\\s",""));
				docData.addValue("SELECT_OWNER", majorCADObj.getOwner().getName());
				docData.addValue("SELECT_DESCRIPTION", majorCADObj.getDescription());
				docData.addValue("SELECT_REVISION", majorCADObj.getRevision());
				// <p><b>This text is bold</b>.</p>
				String listStatesStr = "";
				StateList listStates = majorCADObj.getStates(context);
				for (int s = 0; s < listStates.size(); s++) {
					State state = (State) listStates.get(s);
					if (state.isCurrent())
						listStatesStr += state.getName();
				}
				docData.addValue("SELECT_STATE", listStatesStr);
				docData.addValue("parentId", parentID);
				docData.addValue("icon", getIcon(majorCADObj.getTypeName()));
				logger.debug ("Adding COMP: "+docData.getJSONDataObject());
				listDocs.add(docData);
				//if (!listTempRec.containsKey(majorObjID)) {
					
					listTempRec.put(majorObjID, majorCADObj);
					int addedAssyComponents = 0;
					if (isObjectTypeAssembly(context,docObj.getTypeName()))
					{
						addedAssyComponents= getAssemblyStructure (context, listDocs, listTempRec, docObj, guid, projectID,0);
					}
					
					if (isObjectTypeComponent(context, docObj.getTypeName()))
					{
						getAssociatedDrawings (context, listDocs, listTempRec, docObj, guid, projectID,addedAssyComponents);
					}
				/*}
				else
				{
				}*/
			}
		} catch (Exception exc) {
			exc.printStackTrace();
		}
		return retVal;
	}
	
	private void getAssociatedDrawings(Context context, ArrayList<TeeDataObject> listDocs,
			HashMap<String, DomainObject> listTempRec, DomainObject componentObj, String parentID,String projectID, int startIndex)
	{
		try {
			Logger logger = TEELogger.getUserLogger(context.getUser());
			
			DomainObject doToExpand = getCorrectDomainObjectToBeSearchedForCADConnections (context, componentObj);
			
			StringList selectStmts = new StringList(1);
			selectStmts.addElement(DomainConstants.SELECT_ID);
			selectStmts.addElement(DomainConstants.SELECT_NAME);

			StringList selectRelStmts = new StringList(1);
			selectRelStmts.addElement(DomainConstants.SELECT_RELATIONSHIP_ID);

			// @formatter:off
		    MapList mlItemsObjects = doToExpand.getRelatedObjects(context,
		    		"Associated Drawing",	// relationship pattern (String)
		    		"CAD Drawing",			// type pattern (String) 
		    		selectStmts,												// objectSelects (StringList)
		    		selectRelStmts,												// relationshipSelects (StringList)
		    		false,														// getTo (bool)
		    		true,														// getFrom (bool)
		    		(short)1, 													// recurseToLevel (int)
		    		"",															// busWhereClause (String)
		    		"",															// relWhereClause (String)
		    		0);															// limit (int)
		    
		    // @formatter:on
		    mlItemsObjects.sort(DomainConstants.SELECT_NAME,"ascending","string");
		    ArrayList<String> preventDoubleDrawingForModelList = new ArrayList<String>();
			for (int d = 0; d < mlItemsObjects.size(); d++) 
			{
				Map doc = (Map) mlItemsObjects.get(d);
				String DID = doc.get("id").toString();
				DomainObject docObj = new DomainObject(DID);
				docObj.open(context);
				
				DomainObject majorCADObj;
				String majorObjID;
				if (isCADObjectVersionedObject(context, DID))
				{
					majorObjID = getMajorObjectForVersionedCADObject(context, DID);
					majorCADObj =  new DomainObject(majorObjID);
					majorCADObj.open(context);
				}
				else
				{
					majorObjID = DID;
					majorCADObj = docObj;
				}
				
				if (preventDoubleDrawingForModelList.contains(majorObjID))
				{
					continue;
				}
				
				TeeDataContent docData = new TeeDataContent();
				//String guid = new Date().getTime()+"";
				//String guid = parentID+String.format("%03d",startIndex+d+1);
				String guid = ""+m_ProjectContentIDCounter++;
				docData.addValue("id", guid);
				docData.addValue("docid", majorObjID);
				docData.addValue("projectID", projectID);
				docData.addValue("SELECT_NAME", majorCADObj.getName());
				docData.addValue("SELECT_TYPE", majorCADObj.getTypeName());
				docData.addValue("TYPE_CLASS", majorCADObj.getTypeName().replaceAll("\\s",""));
				docData.addValue("SELECT_OWNER", majorCADObj.getOwner().getName());
				docData.addValue("SELECT_DESCRIPTION", majorCADObj.getDescription());
				docData.addValue("SELECT_REVISION", majorCADObj.getRevision());
				// <p><b>This text is bold</b>.</p>
				String listStatesStr = "";
				StateList listStates = majorCADObj.getStates(context);
				for (int s = 0; s < listStates.size(); s++) {
					State state = (State) listStates.get(s);
					if (state.isCurrent())
						listStatesStr += state.getName();
				}
				docData.addValue("SELECT_STATE", listStatesStr);
				docData.addValue("parentId", parentID);
				docData.addValue("icon", getIcon(majorCADObj.getTypeName()));
				logger.debug ("Adding DRW: "+docData.getJSONDataObject());
				listDocs.add(docData);
				
				preventDoubleDrawingForModelList.add(majorObjID);
				/*if (!listTempRec.containsKey(majorObjID)) {
					
				}*/
			}
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	private String getIcon(String typename) {
		Map<String, String> enoicons = TeeSettings.getProjectContentSettings().getIcons();
		String image = enoicons.get(typename);
		if (typename.equalsIgnoreCase("Workspace Vault")) {
			image = "/tee/images/" + DBDocument.getFileBaseName(image) + ".png";;
		} else {
			image = "/common/images/" + image;
		}
		
		return m_realUrl + image;
	}

	public ArrayList<TeeDataProject> getAllProjects(Context context) {
		ArrayList<TeeDataProject> retVal = new ArrayList<TeeDataProject>();
		String mqlCommand = constructMqlCommandForGetAllProject();

		try {

			// String mqlRet = MQLCommand.exec(context, mqlCommand, "");
			String mqlRet = MqlUtil.mqlCommand(context, mqlCommand);
			String[] lines = mqlRet.split("\n");
			for (String line : lines) {
				if (line == null || line.trim().equals(""))
					continue;

				String[] tokens = line.split("\\|");

				String type = tokens[0];
				String name = tokens[1];
				String revision = tokens[2];

				TeeDataProject teeProject = new TeeDataProject();

				int strIndex = 3;
				for (String selectable : m_Settings.getProjectSelectablesList()) {
					String selectableName = m_Settings.getProjectSelectableNameForSelectable(selectable);
					String value = tokens[strIndex];
					strIndex++;

					teeProject.addValue(selectableName, value);
				}

				retVal.add(teeProject);
			}
		} catch (FrameworkException fwE) {
			fwE.printStackTrace();
		}

		return (retVal);
	}

	public MapList getProjectMembers(Context context, String projectId) {
		MapList retVal = new MapList();

		HashMap noUserMap = new HashMap();
		noUserMap.put("SELECT_ID", "0");
		noUserMap.put("SELECT_NAME", "none");
		retVal.add(noUserMap);

		String mqlCommand = "expand bus " + projectId + " from relationship 'Workspace Member'  select bus id dump |";

		try {

			String mqlRet = MqlUtil.mqlCommand(context, mqlCommand);
			String[] lines = mqlRet.split("\n");
			for (String line : lines) {
				if (line == null || line.trim().equals(""))
					continue;

				String[] tokens = line.split("\\|");

				String level = tokens[0];
				String linkType = tokens[1];
				String direction = tokens[2];
				String linkedType = tokens[3];
				String linkedName = tokens[4];
				String linkedRev = tokens[5];
				String linkedID = tokens[6];

				HashMap map = new HashMap();
				map.put("SELECT_ID", linkedID);
				map.put("SELECT_NAME", linkedName);

				retVal.add(map);
			}
		} catch (FrameworkException fwE) {
			fwE.printStackTrace();
		}

		return retVal;
	}

	private String constructMqlCommandForGetAllProject() {
		String mqlCommand = "temp query bus Workspace * * where \"from[Route Scope] and attribute[TCETEEManaged].value=='TRUE'\"";

		mqlCommand += " select";

		for (String sel : m_Settings.getProjectSelectablesList()) {
			mqlCommand += " " + sel;
		}

		mqlCommand += " dump |";

		return mqlCommand;
	}
	
	public TeeDataProject create(Context context, HttpSession session, List<TeeDataProject> input) throws Exception {
		TeeDataProject teeproject = input.get(input.size() - 1);
		String name = teeproject.getValue("SELECT_NAME");
		String templatename = "TEEProjectTemplate";
		String language = "en-us";
		String vaultname = context.getVault().getName();
		
		
		try 
		{
			ContextUtil.pushContext(context);
			if(!WorkspaceTemplate.isWorkspaceTemplateExists(context,templatename)) {
				throw new FrameworkException(String.format("%s '%s' does not exist!", WorkspaceTemplate.TYPE_WORKSPACE_TEMPLATE, templatename));
			}
			ContextUtil.popContext(context);
			
			StringList selection=new StringList();
    		selection.add(DomainConstants.SELECT_ID);
			MapList mlTemplates = DomainObject.findObjects(	context,	//Context
					WorkspaceTemplate.TYPE_WORKSPACE_TEMPLATE,			//type
					templatename,									    //name
					"1",												//revision
					"*",												//owner
					"*",												//vault
					null,												//whereExpression
					false,												//expand type
					selection											//objectSelects
					);
            if (mlTemplates.size()==0)
            {
            	throw new FrameworkException(String.format("Failed to retrieve %s '%s'!", WorkspaceTemplate.TYPE_WORKSPACE_TEMPLATE, templatename));
            }
			
            String templateId = (String)((Map<String, String>)mlTemplates.get(0)).get(DomainConstants.SELECT_ID);
            if((templateId == null || "null".equals(templateId)) || (templateId.equals(""))) 
            {
            	throw new FrameworkException(String.format("Failed to retrieve OID for %s '%s'!", WorkspaceTemplate.TYPE_WORKSPACE_TEMPLATE, templatename));
            }

            ContextUtil.startTransaction(context, true);
            
            //Create Workspace using emxWorkspace JPO
            String description = "Workspace '"+name+"' for TEE Product";
            HashMap<String,String> jpoInputMap = new HashMap<String,String>();
            jpoInputMap.put("Name", name);
            jpoInputMap.put("Description", description);
            jpoInputMap.put("TemplateOID", templateId);
            String[] jpoInputArgs = JPO.packArgs(jpoInputMap);
            Map jpoRetVal = JPO.invoke(context, "emxWorkspace", null, "createWorkspaceProcess", jpoInputArgs, Map.class);
            if (jpoRetVal.containsKey("ErrorMessage"))
            {
            	String errMsg = (String)jpoRetVal.get("ErrorMessage");
            	throw new FrameworkException(errMsg);
            }
            
            String projectId = (String)jpoRetVal.get("id");
            Workspace workspace = (Workspace) DomainObject.newInstance(context, projectId, DomainConstants.TEAM);
			workspace.open(context);
            
			
			// set attributes
			String TCETEEManaged = PropertyUtil.getSchemaProperty(context, "attribute_TCETEEManaged");
			workspace.setAttributeValue(context, TCETEEManaged, "TRUE");
			workspace.update(context);

			// activate project
			while (!FrameworkUtil.getCurrentState(context, workspace).getName().equals(Workspace.STATE_PROJECT_SPACE_ACTIVE)) 
			{
				workspace.promote(context);
			}

			
			// create Route
			Route route = (Route) DomainObject.newInstance(context, Route.TYPE_ROUTE);
			String routeId = TeamUtil.autoRevision(context, session, Route.TYPE_ROUTE, "Complete " + name + " Design", Route.POLICY_ROUTE, vaultname);
			route.setId(routeId);

			route.open(context);
			route.setAttributeValue(context, Route.ATTRIBUTE_RESTRICT_MEMBERS, projectId);
			String currentRouteNode = PropertyUtil.getSchemaProperty(context, "attribute_CurrentRouteNode");
			route.setAttributeValue(context, currentRouteNode, "2");
			route.setAttributeValue(context, Route.ATTRIBUTE_ROUTE_STATUS, "Started");
			route.update(context);
			
			//disable triggers for route activation
			//OOTB behavior: route cannot be started when no user is assigned (~= no Route Node connection to user exists)
			//MqlUtil.mqlCommand(context, "trigger off;", true);
			// activate route
			while (!FrameworkUtil.getCurrentState(context, route).getName().equals(Route.STATE_ROUTE_IN_PROCESS)) 
			{
				route.promote(context);
			}

			route.connect(context, new RelationshipType(Route.RELATIONSHIP_PROJECT_ROUTE), true, PersonUtil.getPersonObject(context));
			route.connect(context, new RelationshipType(Route.RELATIONSHIP_ROUTE_SCOPE), false, workspace);

			route.close(context);
			workspace.close(context);

			ContextUtil.commitTransaction(context);

			teeproject.getValues().put("SELECT_ID", projectId);
		} 
		catch (Exception e) 
		{
			teeproject.getValues().put("error", e.getMessage());
			
			ContextUtil.abortTransaction(context);
			e.printStackTrace();
		} 
		/*
		finally 
		{
			MqlUtil.mqlCommand(context, "trigger on;", true);
		}*/
		
		return teeproject;
	}
	
	/*
	public TeeDataProject createDuo(Context context, HttpSession session, List<TeeDataProject> input) throws Exception {
		TeeDataProject teeproject = input.get(input.size() - 1);
		String name = teeproject.getValue("SELECT_NAME");
		GanttTableSettings ganttSettings = (GanttTableSettings)TeeSettings.getSettings(GanttTableSettings.class);
		String templatename = ganttSettings.getTemplateNameForCreateProject();
		String language = "en-us";
		String vaultname = context.getVault().getName();
		
		try {
			ContextUtil.startTransaction(context, true);
			// check workspace exists
			if(Workspace.isWorkspaceExists(context,vaultname,name)) {
				throw new FrameworkException(String.format("%s '%s' already exist!", Workspace.TYPE_PROJECT, name));
			}
			if(!WorkspaceTemplate.isWorkspaceTemplateExists(context,templatename)) {
				throw new FrameworkException(String.format("%s '%s' does not exist!", WorkspaceTemplate.TYPE_WORKSPACE_TEMPLATE, templatename));
			}
			// create project
			Workspace workspace = (Workspace) DomainObject.newInstance(context, Workspace.TYPE_PROJECT, DomainConstants.TEAM);
			
			MqlUtil.mqlCommand(context, "trigger off;", true);

			String projectId = TeamUtil.autoRevision(context, session, Workspace.TYPE_PROJECT, name,
					Workspace.POLICY_PROJECT, vaultname);
			workspace.setId(projectId);
			workspace.open(context);
			// set attributes
			String TCETEEManaged = PropertyUtil.getSchemaProperty(context, "attribute_TCETEEManaged");
			workspace.setAttributeValue(context, TCETEEManaged, "TRUE");
			workspace.setDescription(context, "Demo Workspace for TEE Product");
			workspace.update(context);
			// activate project
			while (!FrameworkUtil.getCurrentState(context, workspace).getName()
					.equals(Workspace.STATE_PROJECT_SPACE_ACTIVE)) {
				workspace.promote(context);
			}
			// search for workspace template
    		StringList selection=new StringList();
    		selection.add(DomainConstants.SELECT_ID);
    		// @formatter:off
			MapList mlTemplates = DomainObject.findObjects(	context,	//Context
					WorkspaceTemplate.TYPE_WORKSPACE_TEMPLATE,			//type
					templatename,									    //name
					"1",												//revision
					"*",												//owner
					"*",												//vault
					null,												//whereExpression
					false,												//expand type
					selection											//objectSelects
					);
			// @formatter:on
            if (mlTemplates.size()==0){
            	throw new FrameworkException(String.format("Failed to retrieve %s '%s'!", WorkspaceTemplate.TYPE_WORKSPACE_TEMPLATE, templatename));
            }
            
            String templateId = (String)((Map<String, String>)mlTemplates.get(0)).get(DomainConstants.SELECT_ID);
            // connect Workspace with Workspace Template Object
            if((templateId != null && !"null".equals(templateId)) && (!templateId.equals(""))) {
               DomainObject workspaceTemplate = DomainObject.newInstance(context,templateId,Workspace.TEAM);
               workspace.connectWorkspaceTemplate(context,workspaceTemplate,true,true,vaultname);
            }
			// link members
			String[] members = new String[] { "mlojka", "ssevcik", "jguran" };
			for (String member : members) {
				String personId = PersonUtil.getPersonObjectID(context, member);
				// to connect each user to Workspace .
				DomainAccess.createObjectOwnership(context, projectId, personId, "Full",
									DomainAccess.COMMENT_MULTIPLE_OWNERSHIP);
				workspace.addRelatedObject(context,
						new RelationshipType(Workspace.RELATIONSHIP_WORKSPACE_MEMBER), false, personId);
			}

			// create Route
			Route route = (Route) DomainObject.newInstance(context, Route.TYPE_ROUTE);
			String routeId = TeamUtil.autoRevision(context, session, Route.TYPE_ROUTE, "Complete " + name + " Design",
					Route.POLICY_ROUTE, vaultname);
			route.setId(routeId);

			route.open(context);
			route.setAttributeValue(context, Route.ATTRIBUTE_RESTRICT_MEMBERS, projectId);
			String currentRouteNode = PropertyUtil.getSchemaProperty(context, "attribute_CurrentRouteNode");
			route.setAttributeValue(context, currentRouteNode, "2");
			route.setAttributeValue(context, Route.ATTRIBUTE_ROUTE_STATUS, "Started");
			route.update(context);
			
			// activate route
			while (!FrameworkUtil.getCurrentState(context, route).getName()
					.equals(Route.STATE_ROUTE_IN_PROCESS)) {
				route.promote(context);
			}

			route.connect(context, new RelationshipType(Route.RELATIONSHIP_PROJECT_ROUTE), true,
					PersonUtil.getPersonObject(context));
			route.connect(context, new RelationshipType(Route.RELATIONSHIP_ROUTE_SCOPE), false, workspace);

			route.close(context);
			workspace.close(context);

			ContextUtil.commitTransaction(context);

			teeproject.getValues().put("SELECT_ID", projectId);
		} catch (Exception e) {
			teeproject.getValues().put("error", e.getMessage());
			
			ContextUtil.abortTransaction(context);
			e.printStackTrace();
		} finally {
			MqlUtil.mqlCommand(context, "trigger on;", true);
		}
		
		return teeproject;
	}
	*/


	protected boolean isObjectTypeComponent(Context context, String objectType) throws MatrixException//CAT Part, CAT Product,...
	{
		return isObjectTypeOfParentType (context, objectType,"MCAD Model");
	}
	
	protected boolean isObjectTypeAssembly(Context context, String objectType) throws MatrixException//ProE Assembly,CAT Product,...
	{
		
		String[] assemblyParentTypes = {"MCAD Assembly", "MCAD Versioned Assembly", "IEF Assembly Family", "IEF Versioned Assembly Family"};
		
		for (String assyParentType:assemblyParentTypes)
		{
			if (isObjectTypeOfParentType (context, objectType,assyParentType))
			{
				return true;
			}
		}
		return false;
	}
	
	
	protected boolean isObjectTypeOfParentType(Context context, String objectType, String parentType) throws MatrixException
	{
		boolean retVal = false;
		StringBuffer mqlCmd = new StringBuffer();
		mqlCmd.append("print type \"");
		mqlCmd.append(objectType);
		mqlCmd.append("\" select kindof[");
		mqlCmd.append(parentType);
		mqlCmd.append("]");

		String mqlResult = MqlUtil.mqlCommand(context, mqlCmd.toString());

		mqlResult = mqlResult.substring(mqlResult.indexOf("=")+2);
		retVal = mqlResult.equalsIgnoreCase("true");
		return retVal;
	}
	
	protected boolean isCADObjectFinalized(Context context, String objectId) throws MatrixException
	{
		boolean retVal = false;
		StringBuffer mqlCmd = new StringBuffer();
		mqlCmd.append("print bus ");
		mqlCmd.append(objectId);
		mqlCmd.append(" select relationship[Finalized] dump");
		
		String mqlResult = MqlUtil.mqlCommand(context, mqlCmd.toString());
		retVal = mqlResult.equalsIgnoreCase("true");
		return retVal;
	}
	
	protected boolean isCADObjectVersionedObject(Context context, String objectId) throws MatrixException
	{
		boolean retVal = false;
		StringBuffer mqlCmd = new StringBuffer();
		mqlCmd.append("print bus ");
		mqlCmd.append(objectId);
		mqlCmd.append(" select attribute[Is Version Object].value dump");
		
		String mqlResult = MqlUtil.mqlCommand(context, mqlCmd.toString());
		retVal = mqlResult.equalsIgnoreCase("true");
		return retVal;
	}
	
	protected String  getVersionedObjectForMajorCADObject(Context context, String objectId) throws MatrixException
	{
		StringBuffer mqlCmd = new StringBuffer();
		mqlCmd.append("print bus ");
		mqlCmd.append(objectId);
		mqlCmd.append(" select from[Active Version].to.id dump");
		
		String mqlResult = MqlUtil.mqlCommand(context, mqlCmd.toString());
		return mqlResult;
	}
	
	protected String  getMajorObjectForVersionedCADObject(Context context, String objectId) throws MatrixException
	{
		StringBuffer mqlCmd = new StringBuffer();
		mqlCmd.append("print bus ");
		mqlCmd.append(objectId);
		mqlCmd.append(" select from[VersionOf].to.id dump");
		
		String mqlResult = MqlUtil.mqlCommand(context, mqlCmd.toString());
		return mqlResult;
	}
	
	protected DomainObject getCorrectDomainObjectToBeSearchedForCADConnections(Context context, DomainObject doObj) throws Exception
	{
		DomainObject correctObjectToExpand;
		String id = doObj.getObjectId(context);
		if (isCADObjectFinalized(context,id))
		{
			if (isCADObjectVersionedObject(context, id))
			{
				
				String majorId = getMajorObjectForVersionedCADObject (context,id);
				correctObjectToExpand = new DomainObject(majorId);
				correctObjectToExpand.open(context);
			}
			else
			{
				correctObjectToExpand = doObj;
			}
		}
		else
		{
			if (isCADObjectVersionedObject(context, id))
			{
				correctObjectToExpand = doObj;
			}
			else
			{
				String versionedId = getVersionedObjectForMajorCADObject(context, id);
				correctObjectToExpand = new DomainObject(versionedId);
				correctObjectToExpand.open(context);
			}
		}
		return correctObjectToExpand;
	}
}
