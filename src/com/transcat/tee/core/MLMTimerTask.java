package com.transcat.tee.core;

import java.util.*;

import com.transcat.tee.utils.LanguageUtils;

public class MLMTimerTask extends  TimerTask
{
	LanguageUtils m_MLMLicense = null;
	  public MLMTimerTask(LanguageUtils mlmLicense)
	  {
		  m_MLMLicense = mlmLicense;
	  }
	  @Override
	  public void run() 
	  {
	    // Your database code here
		  m_MLMLicense.getLanguageTranslate();
	  }
}
