package com.transcat.tee.utils;


import com.matrixone.apps.domain.util.i18nNow;
import com.menang.mnglic.*;
import com.transcat.tee.core.MLMTimerTask;
import matrix.db.Context;

import java.util.*;

public class LanguageUtils
{	
	private static final String DEFAULT_APL_CODE = "TEE";
	private static final String DEFAULT_APL_PRODUCT = "TCE-TEE";	
	private static final String DEFAULT_APL_RELEASE_STRING = "2014";
	private static final String DEFAULT_APL_VERSION_STRING = "2014.1";
	private static final int TIMER_MINUTE = 1;

	private static boolean s_bLicense = false;
	
	private SocketClient s_SocketClient = null;
	private static String m_ClientName = "";
	private static String m_SessionID = "";
	private static boolean m_bAssignedProduct = false;
	private static  boolean m_bRunTimer = false;
	

 public LanguageUtils(Context context)
 {
 	if(m_SessionID.length()==0) //New session
 	{
 		s_SocketClient = null;
 		s_bLicense = false;
 		m_bAssignedProduct = false;
 		m_bRunTimer=false; 		
 		m_ClientName = context.getUser(); 	
 		m_SessionID = context.getSession().getSessionId();
 		try
 		{
 			m_bAssignedProduct = EnoviaUtil.getIsAsssignedPersonToProduct(context, DEFAULT_APL_PRODUCT, m_ClientName);
 		}
 		catch(Exception ex)
 		{
 			m_bAssignedProduct = false;
 		}
 	}
 }
 public static void clearAllLanguages()
 {
	 	m_SessionID = "";
		s_bLicense = false;
		m_bAssignedProduct = false;
		m_bRunTimer=false; 		
 }
 public boolean getAssignedLanguage()
 {
	 return(m_bAssignedProduct);	 
 }
 public static void  setAssignedLanguage(boolean assignedProduct)
 {
	 m_bAssignedProduct = assignedProduct;	 
 }
 public boolean isLanguageSupported()
 {
	return(isLanguageSupported(true));
 }
 public String getSupportMeassge(String sLanguage)
 {
	  	i18nNow locali18nNow = new i18nNow();
	 	String supportMsg = "";
		if(getAssignedLanguage())
		{
			supportMsg = locali18nNow.GetString("MPPStringResource", sLanguage, "TCMPP.ErrorMessage.NoLanguageFound.Label");
		}
		else
		{
			supportMsg = locali18nNow.GetString("MPPStringResource", sLanguage, "TCMPP.ErrorMessage.NoLanguageAssigned.Label");
			supportMsg = String.format( supportMsg,m_ClientName);
		}	 
		return(supportMsg);
 }
 private boolean isLanguageSupported(boolean ob)
 {
	if(!m_bRunTimer)
	{
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new MLMTimerTask(this), TIMER_MINUTE*60*1000, TIMER_MINUTE*60*1000);	
		m_bRunTimer = true;
		return(getLanguageTranslate());
	}
	 if(!s_bLicense || !m_bAssignedProduct)
	 {
		 return(getLanguageTranslate());
	 }
	 return(s_bLicense);
 }
 public boolean getLanguageTranslate( )
 {
	return(getLanguageTranslate(true)); 
 }
 private boolean getLanguageTranslate( boolean ob)
 {
	 if(m_bAssignedProduct)
	 {
	 	 if(s_SocketClient == null)
	     {
		      try
		      {
		        s_SocketClient = new SocketClient(m_ClientName, DEFAULT_APL_CODE, DEFAULT_APL_RELEASE_STRING, DEFAULT_APL_VERSION_STRING);
		      }
		      catch (Exception ex)
		      {
		    	s_bLicense = false;
		        s_SocketClient = null;
		      }
		    }
		    if(s_SocketClient != null)
		    {
		      try
		      {
		        s_SocketClient.confirmLockLicense();
		        int status = s_SocketClient.getStatus();
		        if (status == 0)
		        {
		        	s_bLicense = true;
		        }
		        else
		        {
		          s_bLicense = false;
		          s_SocketClient = new SocketClient( m_ClientName,DEFAULT_APL_CODE, DEFAULT_APL_RELEASE_STRING, DEFAULT_APL_VERSION_STRING);
		          status = s_SocketClient.getStatus();
		          if (status == 0)
		          {
		        	 s_bLicense = true;
		          }
		          else
		          {
				    s_bLicense = false;
		          }
		        }
		      }
		      catch (Exception ex)
		      {
		    	  s_bLicense = false;
		      }
		    }
		    return(s_bLicense);
		 } //not assigned to person
		 else
		 {
			 s_bLicense = false;
			 return(s_bLicense);
		 }
 }
}
