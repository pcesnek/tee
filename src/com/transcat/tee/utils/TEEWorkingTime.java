package com.transcat.tee.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.text.SimpleDateFormat;

public class TEEWorkingTime {

	private Integer m_WorkDayStartHour;
	private Integer m_WorkDayStartMinute;
	private Integer m_WorkDayEndHour;
	private Integer m_WorkDayEndMinute;
	
	private ArrayList<Integer> m_OrderedWeekEndDays;
	
	private static ArrayList<Integer> DEFAULT_WEEKEND_DAYS = new ArrayList<>(Arrays.asList(new Integer(6), new Integer(7)));
	
	public TEEWorkingTime()
	{
		this (8,16);
	}
	
	public TEEWorkingTime(int workDayStartHour, int workDayEndHour)
	{
		this (workDayStartHour,0,workDayEndHour,0);
	}
	
	public TEEWorkingTime(Integer workDayStartHour, Integer workDayStartMinute, Integer workDayEndHour, Integer workDayEndMinute) 
	{
	    m_WorkDayStartHour = workDayStartHour;
	    m_WorkDayStartMinute = workDayStartMinute;
	    m_WorkDayEndHour = workDayEndHour;
	    m_WorkDayEndMinute = workDayEndMinute;
	    
	    //set default weekends
	    m_OrderedWeekEndDays = DEFAULT_WEEKEND_DAYS;
	}
	
	public void setWeekEndDaysList(ArrayList<Integer> weekEndDaysList)
	{
		 m_OrderedWeekEndDays = weekEndDaysList;
	}
	
	
	public GregorianCalendar getNearestWorkingTime(GregorianCalendar date)
	{
		GregorianCalendar retVal = (GregorianCalendar) date.clone();
		ArrayList<Integer> orderedJavaWeekEndDaysList = convertWeekEndDaysListToJavaFormat();
		
		int weekDay = retVal.get(Calendar.DAY_OF_WEEK);
		
		if (orderedJavaWeekEndDaysList.contains(weekDay))
		{
			while (orderedJavaWeekEndDaysList.contains(weekDay))
			{
				retVal.add(Calendar.DATE, 1);
				weekDay = retVal.get(Calendar.DAY_OF_WEEK);
			}
			retVal.set(Calendar.HOUR_OF_DAY, m_WorkDayStartHour);
			retVal.set(Calendar.MINUTE, m_WorkDayStartMinute);
		}
		else
		{
			GregorianCalendar tempEndDate = (GregorianCalendar) retVal.clone();
			tempEndDate.set(Calendar.HOUR_OF_DAY, m_WorkDayEndHour);
			tempEndDate.set(Calendar.MINUTE, m_WorkDayEndMinute);
			
			long currentMillis = retVal.getTimeInMillis();
			long endMillis = tempEndDate.getTimeInMillis();
			
			if (currentMillis > endMillis)
			{
				//time is after working time, go to next day
				retVal.add(Calendar.DATE, 1);
				retVal.set(Calendar.HOUR_OF_DAY, m_WorkDayStartHour);
				retVal.set(Calendar.MINUTE, m_WorkDayStartMinute);
				
				//check whether shifted to weekend
				retVal = getNearestWorkingTime (retVal);
			}
		}
		
		return retVal;
	}
	
	public GregorianCalendar computeWorkingEndDateTime(GregorianCalendar startDate, int durationInSecs)
	{
		ArrayList<Integer> orderedJavaWeekEndDaysList = convertWeekEndDaysListToJavaFormat();
		
		
		GregorianCalendar tempStartDate = (GregorianCalendar) startDate.clone();
		GregorianCalendar tempEndDate = (GregorianCalendar) tempStartDate.clone();
		long tempDurationInMillis = durationInSecs * 1000;
		
		while (tempDurationInMillis > 0)
		{
			tempEndDate.set(Calendar.HOUR_OF_DAY, m_WorkDayEndHour);
			tempEndDate.set(Calendar.MINUTE, m_WorkDayEndMinute);
			
			long startMillis = tempStartDate.getTimeInMillis();
			long endMillis = tempEndDate.getTimeInMillis();
			long diffMillis = endMillis- startMillis;
			
			if (tempDurationInMillis > diffMillis)
			{
				//next day
				tempDurationInMillis -=diffMillis;
				int weekDay = -1;
				do
				{
					tempStartDate.add(Calendar.DATE, 1);
					weekDay = tempStartDate.get(Calendar.DAY_OF_WEEK);
				}
				while (orderedJavaWeekEndDaysList.contains(weekDay));
				
				tempStartDate.set(Calendar.HOUR_OF_DAY, m_WorkDayStartHour);
				tempStartDate.set(Calendar.MINUTE, m_WorkDayStartMinute);
				tempEndDate = (GregorianCalendar) tempStartDate.clone();
			}
			else
			{
				tempEndDate = (GregorianCalendar) tempStartDate.clone();
				tempEndDate.add(Calendar.MILLISECOND,(int)tempDurationInMillis);
				tempDurationInMillis = 0;
			}
		}
		
		GregorianCalendar retVal = (GregorianCalendar) tempEndDate.clone();
		return retVal;
	}
	
	public ArrayList<Integer> convertWeekEndDaysListToJavaFormat()
	{
		ArrayList<Integer> retVal = new ArrayList<Integer>();
		for (Integer weekEndDayIndex: m_OrderedWeekEndDays)
		{
			int javaWeekEndDayIndex = weekEndDayIndex+1;
			if (javaWeekEndDayIndex > 7)
			{
				javaWeekEndDayIndex-=7;
			}
			retVal.add(javaWeekEndDayIndex);
		}
		return retVal;
	}
	
	public static void main(String[] args)
	{
		TEEWorkingTime workingTime = new TEEWorkingTime(8,0, 17,30);
		
		workingTime.setWeekEndDaysList(new ArrayList<>(Arrays.asList(new Integer(6), new Integer(7), new Integer(1))));
		
		GregorianCalendar start = new GregorianCalendar(2015,4,8,9,0);

		
		//SimpleDateFormat formatterTest   = new SimpleDateFormat (eMatrixDateFormat.getInputDateFormat(),Locale.US);
		SimpleDateFormat formatterTest   = new SimpleDateFormat ("dd/MM/yyyy hh:mm:ss a",Locale.US);
		String startDate = formatterTest.format(start.getTime());
		int estimatedHours = 12;
		GregorianCalendar retVal = workingTime.computeWorkingEndDateTime(start,estimatedHours*3600);
		
		String sRealEndDate = formatterTest.format(retVal.getTime());
	}
}
