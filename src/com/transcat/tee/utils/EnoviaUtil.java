package com.transcat.tee.utils;


//matrix
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;

import matrix.db.*;
import matrix.util.MatrixException;
import matrix.util.StringList;

import com.matrixone.apps.domain.DomainConstants;
import com.matrixone.apps.domain.DomainObject;
import com.matrixone.apps.domain.util.ContextUtil;
import com.matrixone.apps.domain.util.FrameworkUtil;
import com.matrixone.apps.domain.util.MapList;
import com.matrixone.apps.domain.util.MqlUtil;
import com.matrixone.apps.domain.util.PropertyUtil;
import com.matrixone.apps.domain.util.eMatrixDateFormat;

import org.apache.log4j.*; 

public class EnoviaUtil 
{
	
	public MapList getAllObjects(Context context, String typeName, String whereCondition) throws Exception
	{
		StringList selectStmts = new StringList(1);
		selectStmts.addElement(DomainConstants.SELECT_ID);

		  
		  //findObjects(context, strType, null,null,strOwnerCondition,DomainConstants.QUERY_WILDCARD,strWhereCondition,true, objectSelects);
		  
		  MapList mlItems = DomainObject.findObjects(context,
				  typeName,
				  "*", 			//name
				  "*", 			//revision
				  "*", 			//owner
				  "*", 			//vault
				  whereCondition,			//where
				  true,			//expand type (find subtypes too)
				  selectStmts	//selects
				  );
	      return mlItems;
	}
	
	public MapList getAllObjects(Context context, String typeName) throws Exception
	{

			  StringList selectStmts = new StringList(1);
			  selectStmts.addElement(DomainConstants.SELECT_ID);
			  
			  //findObjects(context, strType, null,null,strOwnerCondition,DomainConstants.QUERY_WILDCARD,strWhereCondition,true, objectSelects);
			  
			  MapList mlItems = DomainObject.findObjects(context,
					  typeName,
					  "*", 			//name
					  "*", 			//revision
					  "*", 			//owner
					  "*", 			//vault
					  null,			//where
					  true,			//expand type (find subtypes too)
					  selectStmts	//selects
					  );
		      return mlItems;
	  }
	
	  public MapList getRootObjects(Context context, String linkName,String typeName) throws Exception
	  {

			  StringList selectStmts = new StringList(1);
			  selectStmts.addElement(DomainConstants.SELECT_ID);
			  
			  String where = "to["+linkName+"]==False";

			  MapList mlItems = DomainObject.findObjects(context,
					  typeName,
					  "*", //name
					  "*", //revision
					  "*", //owner
					  "*", //vault
					  where,
					  "", 
					  true,
					  selectStmts,
					  (short)0 //sQueryLimit
					  );
		      return mlItems;
	  }
	  
	  public MapList getChildObjects(Context context, String objectID,String linkName,String typeName) throws Exception
	  {
			
		      return getChildObjects (context, objectID, linkName, typeName, null);
		  
	  }
	  
	  public MapList getChildObjects(Context context, String objectID,String linkName,String typeName, ArrayList<String> linkAttrsSelList) throws Exception
	  {
			  DomainObject domObj = new DomainObject(objectID);
			  StringList selectStmts = new StringList(1);
			  selectStmts.addElement(DomainConstants.SELECT_ID);

			  int size = 1;
			  if (linkAttrsSelList != null)
			  {
				  size+=linkAttrsSelList.size();
			  }
			  StringList selectRelStmts = new StringList(size);
			  selectRelStmts.addElement(DomainConstants.SELECT_RELATIONSHIP_ID);
			  if (linkAttrsSelList != null)
			  {
				  for (String linkAttrSel:linkAttrsSelList)
				  {
					  selectRelStmts.addElement(linkAttrSel);
				  }
			  }

		      MapList mlItems = domObj.getRelatedObjects(context,
		    		  linkName,		//String relPattern
		    		  typeName,	//String typePattern
		             selectStmts,		//StringList objectSelects,
		             selectRelStmts,	//StringList relationshipSelects,
		             false,				//boolean getTo,
		             true,				//boolean getFrom,
		             (short)1,			//short recurseToLevel,
		             "",				//String objectWhere,
		             "",				//String relationshipWhere,
		             0);
		      return mlItems;
	  }
	  
	  public MapList getParentObjects(Context context, String objectID,String linkName,String typeName ) throws Exception
	  {
		  return getParentObjects (context, objectID, linkName, typeName, null);
	  }
	  public MapList getParentObjects(Context context, String objectID,String linkName,String typeName, ArrayList<String> linkAttrsSelList) throws Exception
	  {
			  DomainObject domObj = new DomainObject(objectID);
			  StringList selectStmts = new StringList(1);
			  selectStmts.addElement(DomainConstants.SELECT_ID);

			  int size = 1;
			  if (linkAttrsSelList != null)
			  {
				  size+=linkAttrsSelList.size();
			  }
			  StringList selectRelStmts = new StringList(size);
			  selectRelStmts.addElement(DomainConstants.SELECT_RELATIONSHIP_ID);
			  if (linkAttrsSelList != null)
			  {
				  for (String linkAttrSel:linkAttrsSelList)
				  {
					  selectRelStmts.addElement(linkAttrSel);
				  }
			  }


		      MapList mlItems = domObj.getRelatedObjects(context,
		    		  linkName,		//String relPattern
		    		  typeName,	//String typePattern
		             selectStmts,		//StringList objectSelects,
		             selectRelStmts,	//StringList relationshipSelects,
		             true,				//boolean getTo,
		             false,				//boolean getFrom,
		             (short)1,			//short recurseToLevel,
		             "",				//String objectWhere,
		             "",				//String relationshipWhere,
		             0);
		      return mlItems;
		  
	  }
		/*
	  public static boolean addPersonToProduct(Context context,String product,String person) 
		{
			boolean retValue = false;
			Logger logger = TEELogger.getUserLogger(context.getUser());
			String mqlCommand1 = "set context user 'Test Everything'"  ;
			String mqlCommand2 = "mod prod  '"+product+"' add person '" + person + "'"  ;
			try
			{
				logger.debug ("MQL Command " +mqlCommand1);
				MqlUtil.mqlCommand (context, mqlCommand1);
				logger.debug ("MQL Command " +mqlCommand2);
				MqlUtil.mqlCommand (context, mqlCommand2);
			}
			catch(Exception ex)
			{
				logger.error ("Error in addPersonToProduct, product: " +product);
				ex.printStackTrace();
				retValue = false;
			}
			return(retValue);
		}	  
		public static boolean delPersonFromProduct(Context context,String product,String person) 
		{
			boolean retValue = false;
			Logger logger = TEELogger.getUserLogger(context.getUser());
			String mqlCommand1 = "set context user 'Test Everything'" ;
			String mqlCommand2 = "mod prod  '"+product+"' remove person '" + person + "'" ;
			try
			{
				logger.debug ("MQL Command " +mqlCommand1);
				MqlUtil.mqlCommand (context, mqlCommand1);
				logger.debug ("MQL Command " +mqlCommand2);
				MqlUtil.mqlCommand (context, mqlCommand2);				
			}
			catch(Exception ex)
			{
				logger.error ("Error in delPersonFromProduct, product: " +product);
				ex.printStackTrace();
				retValue = false;
			}
			return(retValue);
		}	 */
		public static boolean getIsAsssignedPersonToProduct(Context context,String product,String person) throws Exception
		{
			boolean retValue = false;
			Logger logger = TEELogger.getUserLogger(context.getUser());
													//print prod TCA-MPP select person dump |
			String mqlCommand = "print prod  '"+product+"' select person dump |";
			try
			{
				ContextUtil.pushContext(context);
				String mqlRet = MqlUtil.mqlCommand (context, mqlCommand);
				ContextUtil.popContext(context);
				String[]lines = mqlRet.split("\n");
				for (String line: lines)
				{
					if (line == null || line.trim().equals(""))
						continue;
					String[] tokens = line.split("\\|");
					for(int i =0;i<tokens.length;i++)
					{
						String personInProd = tokens[i];
						if(personInProd.equals(person))
						{
							retValue = true;
							break;
						}
					}
				}
			}
			catch(Exception ex)
			{
				logger.error ("Error in getIsAsssignedPersonToProduct, product: " +product);
				ex.printStackTrace();
				throw ex;
			}
			return(retValue);
		}

	  public String getXMLForStructureBrowserCreate(String newObjectID, String parentObjectID, String relationship)
	  {
		  String xmlStr = "<mxRoot><action>add</action><data status=\"pending\">";
		  xmlStr += "<item oid=\""+newObjectID+"\"";
		  xmlStr += " pid=\""+parentObjectID+"\"";
		  xmlStr += " relType=\""+relationship+"\">";
		  xmlStr += "</item></data></mxRoot>";
		  return xmlStr;
	  }
	  
	  public static String getKindOfForBO (Context context, String objectID)
	  {
		  String retValue = "";
		  String mqlCommand = "print bus  "+objectID+" select type.kindof dump";
		  Logger logger = TEELogger.getUserLogger(context.getUser());
		  try
		  {
			  retValue = MqlUtil.mqlCommand (context, mqlCommand);
		  }
		  catch(Exception ex)
		  {
			  logger.error ("Error in getKindOfForBO, oid: " +objectID);
			  ex.printStackTrace();
		  }
		  return(retValue);
	  }
	  
	  public static String getEnoviaDate(String date_fromDB)  throws Exception
 	  {
		  	String sEnoviaDateString="";
		  	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
			
			if ((!"".equals(date_fromDB))&& (date_fromDB!=null) )
			{
				String dateInString = date_fromDB;
			 
				Date date = formatter.parse(dateInString);
				SimpleDateFormat dateFormat = new SimpleDateFormat(eMatrixDateFormat.getEMatrixDateFormat());
				sEnoviaDateString = dateFormat.format(date);
			}
			
			return sEnoviaDateString;
	  }
	  public static ArrayList<String> generateAutoNames(Context ctx, String enoviaSymbolicTypeName, String name, int count) throws MatrixException
	  {
	    ArrayList<String>retVal = new ArrayList<String>();
	    /*String convertedType = "";
	    if(type.equalsIgnoreCase("caddrawing"))
	    {
	      convertedType = EnoviaDefines.SchemaNames.Types.CAD_DRAWING;
	    }
	    if(type.equalsIgnoreCase("cadmodel"))
	    {
	      convertedType = EnoviaDefines.SchemaNames.Types.CAD_MODEL;
	    }*/

	    //example: execute program eServicecommonNumberGenerator.tcl "type_CADModel" "A Size" "" "NULL" "" "_" "" "Yes"
	    String mqlCommand = "execute program eServicecommonNumberGenerator.tcl ";
	    mqlCommand += "\""+enoviaSymbolicTypeName+"\" ";//Generator Object Name
	    mqlCommand += "\""+name+"\" ";//Generator Object Revision
	    mqlCommand += "\"\" ";//Policy
	    mqlCommand += "\"NULL\" ";//Create Additional
	    mqlCommand += "\"\" ";//Vault
	    mqlCommand += "\"_\" ";//CustomRevLevel
	    mqlCommand += "\"\" ";//ObjectType
	    mqlCommand += "\"Yes\" ";//User SuperUser

	    for (int i = 0 ; i < count; i++)
	    {
	    	String result = "";
	    	MQLCommand mqlContextCommand = new MQLCommand();

	    	boolean success = mqlContextCommand.executeCommand(ctx, mqlCommand);	 
	    	if(success)
	    	{
	    		result = mqlContextCommand.getResult().trim();
	    	}
	    	
	      String tokens[] = result.split("\\|");
	      if(tokens.length==2);
	      {
	        String generatedName = tokens[1];
	        retVal.add(generatedName);
	      }
	    }
	    return retVal;
	  }	  
	  public static StringList getAutoNameSeries(Context context,  String typeName) throws MatrixException
	  {
	    	StringList autoName = new StringList();
	    	
	    	ArrayList<String> topTypeNames = new ArrayList<String>();
	    	ArrayList<String> typeNames = new ArrayList<String> ();
	    	topTypeNames.add("type_DOCUMENTS");
	    	
	    	String typeSymbName = FrameworkUtil.getAliasForAdmin(context, "type", typeName, true);
	    	typeNames.add (typeSymbName);
	    	String loopTypeName = typeName;
	    	do
	    	{
	    		BusinessType boType = new BusinessType (loopTypeName,context.getVault());
	    		StringList typeParents = boType.getParents(context);
	    		loopTypeName = (String)typeParents.get(0);
	    		typeSymbName = FrameworkUtil.getAliasForAdmin(context, "type", loopTypeName, true);
	    		typeNames.add (typeSymbName);
	    	}while (topTypeNames.contains(typeSymbName) == false);
	    	
	    	
	    	String objectGeneratorType = PropertyUtil.getSchemaProperty(context, "type_eServiceObjectGenerator");
	    	for (String searchTypeName : typeNames)
	    	{
	    		Query queryObjType = new Query("");
	    		queryObjType.open(context);
	    		queryObjType.setBusinessObjectType(objectGeneratorType);
	    		queryObjType.setBusinessObjectName(searchTypeName);
	    		queryObjType.setBusinessObjectRevision("*");
	    		queryObjType.setVaultPattern("*");
	    		queryObjType.setOwnerPattern("*");
	    		queryObjType.setWhereExpression("");
	    		queryObjType.setExpandType(false);

	    		BusinessObjectList typeObjectsList	= queryObjType.evaluate(context);
	    		BusinessObjectItr typeObjectsItr	= new BusinessObjectItr(typeObjectsList);
	    		while(typeObjectsItr.next())
	    		{
	    			BusinessObject objGenObj = typeObjectsItr.obj();
	    			//autoName.addElement(objGenObj.getName());
	    			autoName.addElement(objGenObj.getName() + "-" + objGenObj.getRevision());
	    		}
	    		queryObjType.close(context);
	    		
	    	}
	    	autoName.sort();
	    	
	    	return autoName;
	    }
	    	  
}
