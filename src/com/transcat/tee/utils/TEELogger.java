package com.transcat.tee.utils;

import java.io.*;
import java.util.*;
import org.apache.log4j.*; 

public class TEELogger 
{
	private static String s_LogDirectory = "C:\\Temp";
	private static String s_LogLevel = "fatal";	
	private static String s_LogPattern = "%d{yyyy-MM-dd HH:mm:ss,SSS} %-5p - %m%n";		
    private static HashMap<String,Logger> s_UserLogger = new HashMap<String,Logger>();
    
    static
    {
		ResourceBundle teeBundle = ResourceBundle.getBundle("tee");
		if (teeBundle.containsKey("TEESuite.System.Log.Directory"))
			s_LogDirectory = teeBundle.getString("TEESuite.System.Log.Directory").trim();
		
		if (teeBundle.containsKey("TEESuite.System.Log.Directory")) 
			s_LogLevel = teeBundle.getString("TEESuite.System.Log.Level").trim();
		
		if (teeBundle.containsKey("TEESuite.System.Log.Pattern")) 
			s_LogPattern = teeBundle.getString("TEESuite.System.Log.Pattern").trim();
    }
    
    public static Logger getUserLogger(String user)
    {
 		Logger logger = null;
 		if(!s_UserLogger.containsKey(user))
 		{
 			String userLevel = s_LogLevel;
 			ResourceBundle teeBundle = ResourceBundle.getBundle("tee"); 
 			if (teeBundle.containsKey("TEESuite.System.Log.User."+user+".Level"))
 				userLevel = teeBundle.getString("TEESuite.System.Log.User."+user+".Level").trim(); 			
 					
 			Layout layout = new PatternLayout(s_LogPattern);  
 		    
 		    String fileTolog = s_LogDirectory + "\\tee_"+user+".log";  
 		    logger = Logger.getLogger(user);  

 		    if(userLevel.equalsIgnoreCase("DEBUG"))
 		    	logger.setLevel(Level.DEBUG); 
 		    if(userLevel.equalsIgnoreCase("INFO"))
 		    	logger.setLevel(Level.INFO);  
 		    if(userLevel.equalsIgnoreCase("WARN"))
 		    	logger.setLevel(Level.WARN); 
 		    if(userLevel.equalsIgnoreCase("ERROR"))
 		    	logger.setLevel(Level.ERROR);  
 		    if(userLevel.equalsIgnoreCase("FATAL"))
 		    	logger.setLevel(Level.FATAL);   		    
 		
			try
			{
	       		File file = new File(fileTolog);  
	       		FileAppender appender = new FileAppender(layout, file.getAbsolutePath(), false);  
	       		logger.addAppender(appender); 
			}
			catch(Exception ex)
			{
				System.out.println(ex.getMessage());
			}
			s_UserLogger.put(user,logger);
 		}
 		return(s_UserLogger.get(user)); 
    }
}
