package com.transcat.tee.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import com.matrixone.apps.domain.util.eMatrixDateFormat;
import com.transcat.tee.settings.GanttChartSettings;
import com.transcat.tee.settings.TeeSettings;

public class ReportData
{
	protected static SimpleDateFormat m_DateFormat = new SimpleDateFormat (eMatrixDateFormat.getInputDateFormat(), Locale.US);
	protected static SimpleDateFormat m_DayFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
	GanttChartSettings m_GanttSettings = (GanttChartSettings)TeeSettings.getSettings(GanttChartSettings.class);
	GregorianCalendar m_Now;
	String m_sNow;
	TEEWorkingTime m_TeeWorkTime = null;
	
	HashMap<String, TaskReportData> tasksData;
	
	public ReportData ()
	{
		m_TeeWorkTime = new TEEWorkingTime(m_GanttSettings.getStartWorkDayHour(), m_GanttSettings.getStartWorkDayMinute(), 
				m_GanttSettings.getEndWorkDayHour(), m_GanttSettings.getEndWorkDayMinute());
		m_TeeWorkTime.setWeekEndDaysList(m_GanttSettings.getWeekendDaysList());
		m_Now = new GregorianCalendar();
		m_sNow = m_DateFormat.format(m_Now.getTimeInMillis());
		tasksData = new HashMap<String, TaskReportData>();
	}
	
	public void startWorkOnTask(String taskID, String taskName, String taskProjectName, String user, String time)
	{
		TaskReportData newTaskWork = new TaskReportData(taskID,taskName,taskProjectName);
		newTaskWork.startWork(user, time);
		tasksData.put(taskID, newTaskWork);
	}
	
	public void reStartWorkOnTask(String taskID, String user, String time)
	{
		TaskReportData taskWork = tasksData.get(taskID);
		if (taskWork != null)
		{
			taskWork.startWork(user, time);
		}
		else
		{
			//System.out.println ("!!! Got null task when re-starting work on task with id "+taskID);
		}
	}
	
	public void stopWorkOnTask(String taskID, String time)
	{
		TaskReportData taskWork = tasksData.get(taskID);
		if (taskWork != null)
		{
			taskWork.stopWork(time);
		}
		else
		{
			//System.out.println ("!!! Got null task when stopping work on task with id "+taskID);
		}
	}
	
	public ArrayList<HashMap<String,String>> getAllTasksRecords()
	{
		ArrayList<HashMap<String,String>> retVal= new ArrayList<HashMap<String,String>>();
		
		for (String taskID:tasksData.keySet())
		{
			TaskReportData taskRep = tasksData.get(taskID);
			retVal.addAll(taskRep.getTaskRecords());
		}
		return retVal;
	}
	
	protected class TaskReportData
	{
		
		String id, name, projectName;
		String currentUser = "";
		boolean isStarted = false;
		GregorianCalendar lastStartTime;
		LinkedHashMap<String,HashMap<String, Long>> dayUserWork;
		
		protected TaskReportData(String taskID, String taskName, String taskProjectName)
		{
			id = taskID;
			name = taskName;
			projectName = taskProjectName;
			dayUserWork = new LinkedHashMap<String,HashMap<String, Long>>();
		}
		
		protected void startWork (String user, String time)
		{
			currentUser = user;
			isStarted = true;
			lastStartTime = parseStringDate(time);
			
			lastStartTime = getNearestStartTime(lastStartTime);
			
		}
		
		protected void stopWork(String time)
		{
			isStarted = false;
			GregorianCalendar endDate = parseStringDate(time);
			GregorianCalendar nearestEndDate = getNearestEndTime(endDate);
			
			long startDateInMillis = lastStartTime.getTimeInMillis();
			long endDataInMillis = nearestEndDate.getTimeInMillis();
			
			if (startDateInMillis > endDataInMillis)
			{
				//can happen if started out of working hours
				//nothing to do
			}
			else
			{
				int startDay = lastStartTime.get(Calendar.DAY_OF_YEAR);
				int endDay = nearestEndDate.get(Calendar.DAY_OF_YEAR);
				
				if (startDay != endDay)
				{
					GregorianCalendar tempStartDate = (GregorianCalendar)lastStartTime.clone();
					while (startDay != endDay)
					{
						GregorianCalendar tempEndDate = (GregorianCalendar)tempStartDate.clone();
						tempEndDate.set(Calendar.HOUR_OF_DAY, m_GanttSettings.getEndWorkDayHour());
						tempEndDate.set(Calendar.MINUTE, m_GanttSettings.getEndWorkDayMinute());
						tempEndDate.set(Calendar.SECOND,0);
						tempEndDate.set(Calendar.MILLISECOND,0);
						
						startDateInMillis = tempStartDate.getTimeInMillis();
						endDataInMillis = tempEndDate.getTimeInMillis();
						long duration = endDataInMillis - startDateInMillis;
						storeRecordToData(tempStartDate,duration);
						
						tempStartDate.add(Calendar.DATE, 1);
						tempStartDate.set(Calendar.HOUR_OF_DAY, m_GanttSettings.getStartWorkDayHour());
						tempStartDate.set(Calendar.MINUTE, m_GanttSettings.getStartWorkDayMinute());
						tempStartDate.set(Calendar.SECOND,0);
						tempStartDate.set(Calendar.MILLISECOND,0);
						
						startDay = tempStartDate.get(Calendar.DAY_OF_YEAR);
					}
					
					startDateInMillis = tempStartDate.getTimeInMillis();
					endDataInMillis = nearestEndDate.getTimeInMillis();
					
					long duration = endDataInMillis - startDateInMillis;
					storeRecordToData(tempStartDate,duration);
				}
				else
				{
					long duration = endDataInMillis - startDateInMillis;
					storeRecordToData(lastStartTime,duration);
				}
			}
			
		}
		
		
		protected GregorianCalendar getNearestStartTime(GregorianCalendar startTime)
		{
			GregorianCalendar retVal = (GregorianCalendar)startTime.clone();
			
			ArrayList<Integer> orderedJavaWeekEndDaysList = m_TeeWorkTime.convertWeekEndDaysListToJavaFormat();
			
			int weekDay = retVal.get(Calendar.DAY_OF_WEEK);
			
			if (orderedJavaWeekEndDaysList.contains(weekDay))
			{
				while (orderedJavaWeekEndDaysList.contains(weekDay))
				{
					retVal.add(Calendar.DATE, 1);
					weekDay = retVal.get(Calendar.DAY_OF_WEEK);
				}
				retVal.set(Calendar.HOUR_OF_DAY, m_GanttSettings.getStartWorkDayHour());
				retVal.set(Calendar.MINUTE, m_GanttSettings.getStartWorkDayMinute());
				retVal.set(Calendar.SECOND,0);
				retVal.set(Calendar.MILLISECOND,0);
			}
			else
			{
				GregorianCalendar tempStartDate = (GregorianCalendar) retVal.clone();
				tempStartDate.set(Calendar.HOUR_OF_DAY,  m_GanttSettings.getStartWorkDayHour());
				tempStartDate.set(Calendar.MINUTE, m_GanttSettings.getStartWorkDayMinute());
				tempStartDate.set(Calendar.SECOND,0);
				tempStartDate.set(Calendar.MILLISECOND,0);
				
				long currentMillis = retVal.getTimeInMillis();
				long startMillis = tempStartDate.getTimeInMillis();
				
				if (startMillis > currentMillis)
				{
					//time is after working time, 
					retVal = tempStartDate;
				}
				else
				{
					GregorianCalendar tempEndDate = (GregorianCalendar) retVal.clone();
					tempEndDate.set(Calendar.HOUR_OF_DAY,  m_GanttSettings.getEndWorkDayHour());
					tempEndDate.set(Calendar.MINUTE, m_GanttSettings.getEndWorkDayMinute());
					tempEndDate.set(Calendar.SECOND,0);
					tempEndDate.set(Calendar.MILLISECOND,0);
					long endMillis = tempEndDate.getTimeInMillis();
					if (currentMillis > endMillis)
					{ //work started after working hours - set start work to next day (start work hour of that day)
						tempStartDate.add (Calendar.DATE, 1);
						retVal = tempStartDate;
					}
				}
			}

			return retVal;
		}
		
		protected GregorianCalendar getNearestEndTime(GregorianCalendar endTime)
		{
			GregorianCalendar retVal = (GregorianCalendar)endTime.clone();
			
			ArrayList<Integer> orderedJavaWeekEndDaysList = m_TeeWorkTime.convertWeekEndDaysListToJavaFormat();
			
			int weekDay = retVal.get(Calendar.DAY_OF_WEEK);
			
			if (orderedJavaWeekEndDaysList.contains(weekDay))
			{
				while (orderedJavaWeekEndDaysList.contains(weekDay))
				{
					retVal.add(Calendar.DATE, -1);
					weekDay = retVal.get(Calendar.DAY_OF_WEEK);
				}
				retVal.set(Calendar.HOUR_OF_DAY, m_GanttSettings.getEndWorkDayHour());
				retVal.set(Calendar.MINUTE, m_GanttSettings.getEndWorkDayMinute());
				retVal.set(Calendar.SECOND,0);
				retVal.set(Calendar.MILLISECOND,0);
			}
			else
			{
				GregorianCalendar tempEndDate = (GregorianCalendar) retVal.clone();
				tempEndDate.set(Calendar.HOUR_OF_DAY,  m_GanttSettings.getEndWorkDayHour());
				tempEndDate.set(Calendar.MINUTE, m_GanttSettings.getEndWorkDayMinute());
				tempEndDate.set(Calendar.SECOND,0);
				tempEndDate.set(Calendar.MILLISECOND,0);
				
				long currentMillis = retVal.getTimeInMillis();
				long endMillis = tempEndDate.getTimeInMillis();
				
				if (currentMillis > endMillis)
				{
					//time is after working time, set end work to previous day (end of working hours of that day)
					retVal = tempEndDate;
				}
				else
				{
					GregorianCalendar tempStartDate = (GregorianCalendar) retVal.clone();
					tempStartDate.set(Calendar.HOUR_OF_DAY,  m_GanttSettings.getStartWorkDayHour());
					tempStartDate.set(Calendar.MINUTE, m_GanttSettings.getStartWorkDayMinute());
					tempStartDate.set(Calendar.SECOND,0);
					tempStartDate.set(Calendar.MILLISECOND,0);
					long startMillis = tempStartDate.getTimeInMillis();
					if (startMillis > currentMillis)
					{//work ended before working hours
						tempEndDate.add (Calendar.DATE, -1);
						retVal = tempEndDate;
					}
				}
			}

			return retVal;
		}
		
		protected void storeRecordToData(GregorianCalendar startDate, long duration)
		{
			String dayString = m_DayFormat.format(startDate.getTimeInMillis());
			
			HashMap<String, Long> hoursForUserPerDayMap = dayUserWork.get(dayString);
			if (hoursForUserPerDayMap == null)
			{
				hoursForUserPerDayMap = new HashMap<String, Long>();
				dayUserWork.put (dayString,hoursForUserPerDayMap);
			}
			
			
			Long storedDurationForUserAndDay = hoursForUserPerDayMap.get(currentUser);
			if (storedDurationForUserAndDay == null)
			{
				Long newDurationForDay = new Long (duration);
				hoursForUserPerDayMap.put (currentUser,newDurationForDay);
			}
			else
			{
				long storedDuration = storedDurationForUserAndDay.longValue();
				long sumDuration = storedDuration+duration;
				Long newDurationForDay = new Long (sumDuration);
				hoursForUserPerDayMap.put (currentUser,newDurationForDay);
			}
			
		}
		
		public ArrayList<HashMap<String,String>> getTaskRecords()
		{
			
			if (isStarted)
			{
				stopWork(m_sNow);
			}
			
			ArrayList<HashMap<String,String>> retVal = new ArrayList<HashMap<String,String>>();
			for (String dayString: dayUserWork.keySet())
			{
				HashMap<String,Long> hoursForUserPerDayMap = dayUserWork.get(dayString);
				for (String user: hoursForUserPerDayMap.keySet())
				{
					Long durationObj = hoursForUserPerDayMap.get(user);
					long duration = durationObj.longValue();
					
					String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(duration),
						    TimeUnit.MILLISECONDS.toMinutes(duration) % TimeUnit.HOURS.toMinutes(1),
						    TimeUnit.MILLISECONDS.toSeconds(duration) % TimeUnit.MINUTES.toSeconds(1));
					
					HashMap<String,String> record = new HashMap<String,String>();
					
					record.put("taskName",name);
					record.put("taskID",id);
					record.put("taskProject",projectName);
					record.put("user",user);
					record.put ("day", dayString);
					record.put ("duration",hms);
					retVal.add(record);
				}
			}
			return retVal;
			
		}
		
		public GregorianCalendar parseStringDate(String strDate)
		{
			GregorianCalendar retVal = new GregorianCalendar();
			try
			{
				retVal.setTime(m_DateFormat.parse(strDate));
			}
			catch (ParseException pe)
			{
				System.err.println ("ParseException "+pe+" when parsing string date "+strDate+" using format "+eMatrixDateFormat.getInputDateFormat());
				retVal = null;
			}
			catch (Exception ex)
			{
				System.err.println ("UnexpectedException "+ex+" when parsing string date "+strDate+" using format "+eMatrixDateFormat.getInputDateFormat());
				ex.printStackTrace();
				throw ex;
			}
			return retVal;
		}
	}
	
	public static void main(String[] args)
	{
		ReportData rd = new ReportData();
		
		//rd.startWorkOnTask("1", "first", "project", "mlojka", "6/29/2015 07:00:00 AM");
		//rd.stopWorkOnTask("1", "6/29/2015 05:20:00 PM");
		//rd.reStartWorkOnTask("1", "mlojka", "6/29/2015 11:30:00 AM");
		//rd.stopWorkOnTask("1", "6/30/2015 11:50:00 AM");

		rd.startWorkOnTask("1", "vstreky", "project", "mlojka", "6/23/2015 4:24:02 PM");
		rd.stopWorkOnTask("1", "6/24/2015 9:08:07 AM");
		rd.reStartWorkOnTask("1", "mlojka", "6/24/2015 9:08:22 AM");
		rd.stopWorkOnTask("1", "6/24/2015 9:11:10 AM");
		
		ArrayList<HashMap<String,String>> test = rd.getAllTasksRecords();
	}
	
}
