package com.transcat.tee.settings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;


public class ProjectSelectableSettings extends AbstractSelectableSettings
{
	protected ArrayList<String> m_ProjectSelectableNamesList;
	protected ArrayList<String> m_ProjectSelectablesList;
	
	protected HashMap<String,String> m_ProjectSelectableNamesToSelectablesMap;
	protected HashMap<String,String> m_ProjectSelectablesToSelectableNamesMap;
	
	
	public ProjectSelectableSettings()
	{
		m_ProjectSelectableNamesList = new ArrayList<String>();
		m_ProjectSelectablesList = new ArrayList<String>();
		m_ProjectSelectableNamesToSelectablesMap = new HashMap<String,String>();
		m_ProjectSelectablesToSelectableNamesMap = new HashMap<String,String>();
		
		ResourceBundle teeBundle = ResourceBundle.getBundle("tee");
		if (teeBundle.containsKey("TEESuite.DataSchema.Project.Selectables"))
		{
			String projectSelectablesStr = teeBundle.getString("TEESuite.DataSchema.Project.Selectables").trim();
			parseSettingsInputLine (projectSelectablesStr, m_ProjectSelectableNamesList, m_ProjectSelectablesList, m_ProjectSelectableNamesToSelectablesMap, m_ProjectSelectablesToSelectableNamesMap);
		}
	}
	
	public ArrayList<String> getProjectSelectableNamesList()
	{
		return m_ProjectSelectableNamesList;
	}
	
	public ArrayList<String> getProjectSelectablesList()
	{
		return m_ProjectSelectablesList;
	}
	
	public HashMap<String,String> getProjectSelectablesToSelectableNamesMap()
	{
		return m_ProjectSelectablesToSelectableNamesMap;
	}
	
	public HashMap<String,String> getProjectSelectableNamesToSelectablesMap()
	{
		return m_ProjectSelectableNamesToSelectablesMap;
	}
	
	public String getProjectSelectForSelectableName(String selectableName)
	{
		String retVal = m_ProjectSelectableNamesToSelectablesMap.get(selectableName); 
		return retVal;
	}
	
	public String getProjectSelectableNameForSelectable(String selectable)
	{
		String retVal = m_ProjectSelectablesToSelectableNamesMap.get(selectable); 
		return retVal;
	}
	
	
}
