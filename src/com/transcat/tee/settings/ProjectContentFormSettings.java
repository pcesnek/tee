package com.transcat.tee.settings;

public class ProjectContentFormSettings extends AbstractSettings
{
	public ProjectContentFormSettings()
	{
		readTableSettings ("TEESuite.ProjectContent.Form.Fields", "TEESuite.ProjectContent.Form.Field");
	}
	
	public static void main(String[] args)
	{
		AbstractSettings.main(new ProjectContentFormSettings());
	}
}
