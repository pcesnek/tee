package com.transcat.tee.settings;

import java.util.ArrayList;

public class BaseSettings 
{
	protected ArrayList<String> parseLine (String line)
	{
		return parseLine(line, ",");
	}
	protected ArrayList<String> parseLine (String line, String delims)
	{
		ArrayList<String> retVal  =new ArrayList<String>();
		String []selectTokens = line.split(delims);
		for (String token : selectTokens)
		{
			if (token.equals(""))
				continue;
			
			retVal.add (token.trim());
		}
		return retVal;
	}
	
	public String trimSelectFromAttributePrefix(String select)
	{
		String retVal = select.trim();
		
		if (retVal.startsWith("attribute[") )
		{
			String tempStr =retVal.substring("attribute[".length());
			if (tempStr.endsWith("]"))
			{
				retVal=tempStr.substring(0,tempStr.length()-1);
			}
			else if (tempStr.endsWith("].value"))
			{
				retVal=tempStr.substring(0,tempStr.length()-"].value".length());
			}
		}
		
		return retVal;
	}
	
	public String getJSONString() {
		return "";
	}
	
	public static void  main(String[] args)
	{
		BaseSettings bs = new BaseSettings();
		String select = "attribute[Route Node Instructions]";
		String select2 = "attribute[Scheduled Completion Date].value";
		String select3 = "attribute[Title]";

		
	}
}
