package com.transcat.tee.settings;

import java.util.ResourceBundle;

public class GanttTableSettings extends AbstractSettings
{
	String m_TemplateName = "";
	public GanttTableSettings()
	{
		readTableSettings ("TEESuite.GanttChart.Table.Task.Columns", "TEESuite.GanttChart.Table.Task.Column");
		
		ResourceBundle teeBundle = ResourceBundle.getBundle("tee");
		m_TemplateName = teeBundle.getString("TEESuite.GanttChart.CreateProject.TemplateName");
		if (m_TemplateName != null)
		{
			m_TemplateName = m_TemplateName.trim();
		}
	}
	
	public String getTemplateNameForCreateProject()
	{
		return m_TemplateName;
	}

	public static void main(String[] args)
	{
		AbstractSettings.main(new GanttTableSettings());
			
	}
}
