package com.transcat.tee.settings;

import java.io.FileInputStream;
import java.util.*;
import java.util.Map.Entry;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.matrixone.apps.domain.util.FrameworkUtil;


public class KanbanSettings extends BaseSettings
{
	protected ArrayList<String> m_ProjectColorList = new ArrayList<String>();
	protected ArrayList<KanbanColumnSetting> m_ColumnsList = new ArrayList<KanbanColumnSetting>();
	protected String m_TaskVisual = "";
	
	public HashMap <String,ArrayList<String>> getPolicyStatesMapForKanbanColumn(String kanbanColumnName)
	{
		HashMap <String,ArrayList<String>> retVal = new HashMap <String,ArrayList<String>>();
		for (KanbanColumnSetting colSet: m_ColumnsList)
		{
			if (colSet.getIdent().equals(kanbanColumnName))
			{
				HashMap<String, KanbanSettings.KanbanColumnSetting.KanbanPolicySetting> policySettingsForColumnMap = colSet.getPolicySetting();
				
				for (String policyName: policySettingsForColumnMap.keySet())
				{
					KanbanSettings.KanbanColumnSetting.KanbanPolicySetting policySettings = policySettingsForColumnMap.get(policyName);
					ArrayList<String> statesList = policySettings.getStates();
					retVal.put(policyName, statesList);
				}
			}
		}
		return retVal;
	}
	public String getTargetStateForKanbanColumnRelatedObject(int column,String policy,String object)
	{
		String retState = "";
		KanbanSettings.KanbanColumnSetting columnSettings = m_ColumnsList.get(column);
		if(columnSettings != null)
		{
			KanbanSettings.KanbanColumnSetting.KanbanPolicySetting policySettings = columnSettings.getPolicySetting().get(policy);
			if(policySettings != null)
				retState = policySettings.getRelatedStates().get(object);
		}
		if(retState == null)
			retState = "";
		return retState;
	}	
	
	public KanbanSettings ()
	{		
		//For test purpose
		FileInputStream fis = null;
		ResourceBundle teeBundle = null;
		/*try
		{
		  fis = new FileInputStream("C:\\TEE\\workspace\\TEE\\WebContent\\WEB-INF\\classes\\tee.properties");
		  teeBundle  = new PropertyResourceBundle(fis);
		}
		catch(Exception ex)
		{
			
		}*/
		
		teeBundle = ResourceBundle.getBundle("tee");
		String policiesString = "";
		if (teeBundle.containsKey("TEESuite.DataSchema.Task.Policies"))
		{
			policiesString = teeBundle.getString("TEESuite.DataSchema.Task.Policies").trim();
		}
		ArrayList<String> policies = parseLine(policiesString);
		
		String columnsString = "";
		if (teeBundle.containsKey("TEESuite.Kanban.Table.Columns"))
		{
			columnsString = teeBundle.getString("TEESuite.Kanban.Table.Columns").trim();
		}
		ArrayList<String> columns = parseLine(columnsString);
		
		for(String column: columns)
		{
			KanbanColumnSetting columnSetting = new KanbanColumnSetting(column);	
			String label = teeBundle.getString("TEESuite.Kanban.Table.Column."+column+".Label");
			columnSetting.setLabel(label);
			for (String policy:policies)
			{
				String state = teeBundle.getString("TEESuite.Kanban.Table.Column."+column+"."+policy+".States");
				String promote = teeBundle.getString("TEESuite.Kanban.Table.Column."+column+"."+policy+".Promote");
				String demote = teeBundle.getString("TEESuite.Kanban.Table.Column."+column+"."+policy+".Demote");
				
				KanbanColumnSetting.KanbanPolicySetting kanbanPolicySetting =  columnSetting.new KanbanPolicySetting(policy);
				kanbanPolicySetting.parseStates(state);
				kanbanPolicySetting.parsePromotes(promote);
				kanbanPolicySetting.parseDemotes(demote);
				//Content Document Parts Others Promote Demote state
				Enumeration<String> keys = teeBundle.getKeys();
			    while (keys.hasMoreElements()) 
			    {
			         String key = "" + keys.nextElement();
			         //TEESuite.Kanban.Table.Column.ToDo.policy_TCETEETask.Related.Document=IN WORK
			         if(key.startsWith("TEESuite.Kanban.Table.Column."+column+"."+policy+".Related"))
			         {			        	
			        	 String relObject[] = key.split("\\.");
			        	 if(relObject.length == 8)
			        	 {
			        		 String newKey = "TEESuite.Kanban.Table.Column."+column+"."+policy+".Related."+relObject[7];
			        		 String relState = teeBundle.getString(newKey);
			        		 String relObjectType = relObject[7];
			        		 kanbanPolicySetting.addRelatedState(relObjectType,relState);
			        	 }
			         }
			    }		
				columnSetting.addPolicySetting(kanbanPolicySetting);
			}			
			this.addColumn(columnSetting);
		}

		//GUI  Settings
		m_ProjectColorList = parseLine(teeBundle.getString("TEESuite.Kanban.Project.Colors").trim());
		m_TaskVisual =  teeBundle.getString("TEESuite.Kanban.Task.Visual").trim();
	}
	
	public void addColumn(KanbanColumnSetting columnName)
	{
		m_ColumnsList.add (columnName);
	}
	public ArrayList<KanbanColumnSetting> getColumnsList()
	{
		return m_ColumnsList;
	}
	
	public int getColumnPositionByColumnID(String columnID)
	{
		int retVal = -1;
		for (int i = 0; i < m_ColumnsList.size(); i++)
		{
			KanbanColumnSetting col = m_ColumnsList.get (i);
					
			if (col.getIdent().equals(columnID))
			{
				retVal = i;
				break;
			}
		}
		return retVal;
	}
	
	public ArrayList<String> getProjectColorList()
	{
		return m_ProjectColorList;
	}
	class KanbanColumnSetting 
	{
		private String m_Label = "";	
		private String m_Ident = "";
		private HashMap<String,KanbanPolicySetting> m_ListPolicies = new HashMap<String,KanbanPolicySetting>();
		
		public KanbanColumnSetting(String ident){m_Ident=ident;}
		
		public void setLabel(String label){m_Label = label;}
		public String getLabel( ){return(m_Label);}	
		public String getIdent( ){return(m_Ident);}	
		
		public void addPolicySetting(KanbanPolicySetting policySettings){m_ListPolicies.put(policySettings.getName(), policySettings);}
		public HashMap<String,KanbanPolicySetting> getPolicySetting(){return(m_ListPolicies);}
		
		
		class KanbanPolicySetting 
		{
			private String m_Name = "";
			private ArrayList<String> m_States = new ArrayList<String>();
			private ArrayList<String> m_Promotes = new ArrayList<String>();
			private ArrayList<String> m_Demotes = new ArrayList<String>();
			private HashMap<String,String> m_RelatedStates = new HashMap<String,String>();
			
			public KanbanPolicySetting(String name)
			{
				m_Name = name;
			}
			public void parseStates(String states)
			{
				m_States = parseLine(states);
			}
			public void parsePromotes(String promotes)
			{
				m_Promotes = parseLine(promotes);
			}
			public void parseDemotes(String demotes)
			{
				m_Demotes = parseLine(demotes);
			}	
			public void addRelatedState(String object,String state)
			{
				m_RelatedStates.put(object, state);
			}
			public String getName() {return(m_Name);}
			public ArrayList<String> getStates() {return(m_States);}
			public ArrayList<String> getPromotes() {return(m_Promotes);}
			public ArrayList<String> getDemotes() {return(m_Demotes);}
			public HashMap<String,String> getRelatedStates() {return(m_RelatedStates);}
			
			public JSONObject getJSONDataObject()
			{
				JSONObject retVal = new JSONObject();
				retVal.put("Name", m_Name);
				retVal.put("States",m_States);
				retVal.put("Promotes",m_Promotes);
				retVal.put("Demotes",m_Demotes);

				return retVal;
			}			
		}
		public JSONObject getJSONDataObject()
		{
			JSONObject retVal = new JSONObject();
			JSONArray arrayPolicies = new JSONArray();
			retVal.put("Label", m_Label);
			retVal.put("Ident", m_Ident);
			for (Entry<String, KanbanPolicySetting> entry  : m_ListPolicies.entrySet()) 
			{
				String policy = entry.getKey();
				KanbanPolicySetting policySetting =  entry.getValue();
				arrayPolicies.add(policySetting.getJSONDataObject());
			}			
			retVal.put("Policies", arrayPolicies);
			return retVal;
		}		
	}
	public JSONObject getJSONDataObject()
	{
		JSONObject retVal = new JSONObject();
		ArrayList<KanbanColumnSetting> listColumns = this.getColumnsList();
		for(int i=0;i<listColumns.size();i++)
		{
			KanbanColumnSetting columnSettings = listColumns.get(i);
			retVal.put("Columns"+i,columnSettings.getJSONDataObject());
		}
		JSONArray arrayColors = new JSONArray();
		ArrayList<String> listProjectColors = this.getProjectColorList();
		for(int i=0;i<listProjectColors.size();i++)
		{
			String color = listProjectColors.get(i);
			arrayColors.add(color);
		}	
		retVal.put("ProjectColors",arrayColors);
		retVal.put("TaskVisual",m_TaskVisual);
		return retVal;
	}	
	public static void main(String[] args)
	{
		KanbanSettings kanbanSett = new KanbanSettings();
		
		 System.out.println(kanbanSett.getJSONDataObject());
		
		ArrayList<KanbanColumnSetting> listColumns = kanbanSett.getColumnsList();
		for(int i=0;i<listColumns.size();i++)
		{
			KanbanColumnSetting columnSettings = listColumns.get(i);
			System.out.println("Column label:" + columnSettings.getLabel());
			HashMap<String,KanbanColumnSetting.KanbanPolicySetting>  mapPolicySettings = columnSettings.getPolicySetting();
			for (Entry<String, KanbanColumnSetting.KanbanPolicySetting> entry  : mapPolicySettings.entrySet()) 
			{
				String policy = entry.getKey();
				KanbanColumnSetting.KanbanPolicySetting policySetting =  entry.getValue();
			    System.out.println("Policy settings: " + policy);
			    System.out.println("	States" + policySetting.getStates());
			    System.out.println("	Promotes" + policySetting.getPromotes());
			    System.out.println("	Demotes" + policySetting.getDemotes());
			    HashMap<String,String> listRelStates = policySetting.getRelatedStates();
			    for (Map.Entry<String,String> entryRel : listRelStates.entrySet()) 
			    {
			      String key = entryRel.getKey();
			      String value = entryRel.getValue();
			      System.out.println("	related State for" + key + "=" + value);
			    }
			}
		}
	}
	public String getTaskVisual(){return(m_TaskVisual);}
}
