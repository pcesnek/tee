package com.transcat.tee.settings;

public class KanbanFormSettings extends AbstractSettings
{
	public KanbanFormSettings()
	{
		readTableSettings ("TEESuite.Kanban.TaskForm.Fields", "TEESuite.Kanban.TaskForm.Field");
	}

	public static void main(String[] args)
	{
		AbstractSettings.main(new KanbanFormSettings());
	}
}
