package com.transcat.tee.settings;

import java.util.*;

public abstract class AbstractSelectableSettings extends BaseSettings 
{
	protected void parseSettingsInputLine(String line, 
			ArrayList<String> selectableNamesList, ArrayList<String> selectablesList,
			HashMap<String,String> selectableNamesToSelectablesMap, HashMap<String,String> selectablesToSelectableNamesMap)
	{
		ArrayList<String> lineTokensList = parseLine(line);
		
		for (String token:lineTokensList)
		{
			ArrayList<String> selectTokensList = parseLine(token,":");
			if (selectTokensList.size() != 2)
			{
				System.err.println ("Wrong select input: '"+token+"'. This select input is omitted!");
				continue;
			}
			
			String selectableName = selectTokensList.get(0);
			String selectable = selectTokensList.get(1);
			
			selectableNamesList.add(selectableName);
			selectablesList.add(selectable);
			
			selectableNamesToSelectablesMap.put(selectableName,selectable);
			selectablesToSelectableNamesMap.put(selectable,selectableName);
		}
	}
}
