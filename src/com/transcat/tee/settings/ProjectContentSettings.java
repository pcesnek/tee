package com.transcat.tee.settings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.transcat.tee.settings.KanbanSettings.KanbanColumnSetting;

public class ProjectContentSettings extends AbstractSettings {
//	private static String[][] enoarrays = new String[][] { { "Workspace", "emxFramework.smallIcon.type_Project" },
//			{ "Workspace Vault", "emxFramework.smallIcon.type_ProjectVault" },
//			{ "CATPart For Team", "emxFramework.smallIcon.type_CATPart" },
//			{ "CATProduct For Team", "emxFramework.smallIcon.type_CATProduct" },
//			{ "CATDrawing For Team", "emxFramework.smallIcon.type_CATDrawing" },
//			{ "Document", "emxFramework.smallIcon.type_MSWordDocument" } };

	private HashMap<String, String> enotypes = new HashMap<String, String>();
	private ArrayList<String> dragSupportedTypes = new ArrayList<String>();

	private String relationships;
	private String types;
	private String dragTransportDir;

	public ProjectContentSettings() {
		ResourceBundle teeBundle = ResourceBundle.getBundle("tee");
		if (teeBundle.containsKey("TEESuite.ProjectContentTree.Relationships")) {
			relationships = teeBundle.getString("TEESuite.ProjectContentTree.Relationships").trim();
		}
		if (teeBundle.containsKey("TEESuite.ProjectContentTree.Types")) {
			types = teeBundle.getString("TEESuite.ProjectContentTree.Types").trim();
		}
		
		readTableSettings ("TEESuite.ProjectContent.Mappings", "TEESuite.ProjectContent.Mapping");

		ResourceBundle enoBundle = ResourceBundle.getBundle("emxSystem");
		
		for(String type : m_ColumnsList)
		{
			String select = getColumnSetting (type,SELECT_SETTING);
			String label = getColumnSetting (type,LABEL_SETTING);
			if (enoBundle.containsKey(select)) {
				enotypes.put(label, enoBundle.getString(select).trim());
			}
		}
		
		
		String supportedTypesString = "";
		if (teeBundle.containsKey("TEESuite.ProjectContent.DragSupportedTypes"))
		{
			supportedTypesString = teeBundle.getString("TEESuite.ProjectContent.DragSupportedTypes").trim();
		}
		ArrayList<String> suppTypes = parseLine(supportedTypesString);
		for(String suppType: suppTypes)
		{
			dragSupportedTypes.add(suppType);
		}
		
		dragTransportDir = "";
		if (teeBundle.containsKey("TEESuite.ProjectContent.DragTransportDirectory"))
		{
			dragTransportDir = teeBundle.getString("TEESuite.ProjectContent.DragTransportDirectory").trim();
		}
		else
		{
			dragTransportDir = "C:\\Temp";
		}

		
//		for (String[] enoarray : enoarrays) {
//			if (enoBundle.containsKey(enoarray[1])) {
//				enotypes.put(enoarray[0], enoBundle.getString(enoarray[1]).trim());
//			}
//		}
		
	}
	public ArrayList<String> getDragSupportedtypes() {
		return(dragSupportedTypes);
	}
	
	public String getDragTransportDir() {
		return this.dragTransportDir;
	}	

	public String getRelationships() {
		return this.relationships;
	}

	public String getTypes() {
		return this.types;
	}

	public HashMap<String, String> getIcons() {
		return this.enotypes;
	}

	@Override
	public String getJSONString() {
		JSONArray retValArr = new JSONArray();
		for (Map.Entry<String, String> enotype : enotypes.entrySet()) {
			JSONObject jsonEntry = new JSONObject();
			jsonEntry.put("type", enotype.getKey());
			jsonEntry.put("icon", enotype.getValue());

			retValArr.add(jsonEntry);
		}
		return JSONArray.toJSONString(retValArr);
	}

	public static void main(String[] args) {
		ProjectContentSettings.main(new ProjectContentSettings());
	}

	public static void main(ProjectContentSettings settings) {
		for (Map.Entry<String, String> enotype : settings.getIcons().entrySet()) {
		}
	}
}
