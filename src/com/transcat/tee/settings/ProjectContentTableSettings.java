package com.transcat.tee.settings;

public class ProjectContentTableSettings extends AbstractSettings
{
	public ProjectContentTableSettings()
	{
		readTableSettings ("TEESuite.ProjectContent.Table.Columns", "TEESuite.ProjectContent.Table.Column");
	}

	public static void main(String[] args)
	{
		AbstractSettings.main(new ProjectContentTableSettings());
	}
}
