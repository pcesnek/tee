package com.transcat.tee.settings;

import java.util.*;

import org.json.simple.*;

public abstract class AbstractSettings extends BaseSettings
{
	protected String m_BaseKey;
	protected ArrayList<String> m_ColumnsList = new ArrayList<String>();
	protected HashMap<String,HashMap<String,String>> m_ColumnsSettings = new HashMap<String,HashMap<String,String>>();
	
	public static final String LABEL_SETTING = "Label";
	public static final String SELECT_SETTING = "Select";
	public static final String EDITABLE_SETTING = "Editable";
	public static final String WIDTH_SETTING = "Width";
	
	protected static final String[] EXPECTED_SETTINGS  = {LABEL_SETTING, SELECT_SETTING, EDITABLE_SETTING, WIDTH_SETTING};
	
	public Set<String> getSettingsNamesForColumn(String columnName)
	{
		HashMap<String,String> columnSettings = m_ColumnsSettings.get(columnName);
		if (columnSettings == null)
		{
			return null;
		}
	
		Set<String> retVal =columnSettings.keySet();
		return retVal;
	}
	
	public String getColumnSetting(String columnName, String settingName)
	{
		HashMap<String,String> columnSettings = m_ColumnsSettings.get(columnName);
		if (columnSettings == null)
		{
			return null;
		}
		String retVal = columnSettings.get(settingName);
		return retVal;
	}
	
	public ArrayList<String> getColumns()
	{
		return m_ColumnsList;
	}
	
	protected void readTableSettings (String keyForColumnsList, String baseKey)
	{
		ResourceBundle teeBundle = ResourceBundle.getBundle("tee");
		if (teeBundle.containsKey(keyForColumnsList) == false)
		{
			return;
		}
		String columnsListLine = teeBundle.getString(keyForColumnsList).trim();
		m_ColumnsList = parseLine(columnsListLine);
		
		for (String columnName: m_ColumnsList)
		{
			HashMap<String,String> settingsMapForColumn = new HashMap<String,String>();
			m_ColumnsSettings.put(columnName, settingsMapForColumn);
			
			for (String settingName: EXPECTED_SETTINGS)
			{
				String keyForSetting = baseKey+"."+columnName+"."+settingName;
				if (teeBundle.containsKey(keyForSetting))
				{
					String settingValue = teeBundle.getString(keyForSetting).trim();
					settingsMapForColumn.put (settingName, settingValue);
				}
			}
		}
	}
	
	@Override
	public String getJSONString()
	{
		JSONArray retValArr = new JSONArray();
		for(String columnName: m_ColumnsList)
		{
			JSONObject jsonColumn = new JSONObject();
			String select = getColumnSetting (columnName,SELECT_SETTING);
			String label = getColumnSetting (columnName,LABEL_SETTING);
			jsonColumn.put("field",select);	
			jsonColumn.put("title",label);
			
			String editable = getColumnSetting (columnName,EDITABLE_SETTING);
			if (editable != null)
			{
				jsonColumn.put("editable",Boolean.parseBoolean(editable));
			}
			String width = getColumnSetting (columnName,WIDTH_SETTING);
			if (width != null)
			{
				jsonColumn.put("width",Integer.parseInt(width));
			}
			retValArr.add(jsonColumn);
		}
		return JSONArray.toJSONString(retValArr);
	}
	
	public static void main(AbstractSettings settings)
	{
		ArrayList<String> listColumns = settings.getColumns();
		for (String colName: listColumns)
		{
			Set<String> colSets = settings.getSettingsNamesForColumn(colName);
			
			for (String setName: colSets)
			{
				String setValue = settings.getColumnSetting(colName, setName);
			}
		}
	}
}
