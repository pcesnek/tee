package com.transcat.tee.settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;

import org.json.simple.JSONObject;

public class GanttChartSettings extends BaseSettings
{
	protected static final String DEFAULT_WEEKEND_DAYS_LIST 	= "6,7";
	protected static final String DEFAULT_SHOW_WEEKEND_DAYS 	= "true";
	protected static final String DEFAULT_WORKDAY_START 		= "8:00";
	protected static final String DEFAULT_WORKDAY_END 			= "16:00";
	protected static final String DEAFULTSHOW_NON_WORKING_HOURS	= "false";
	
	private ArrayList<Integer> m_WeekendDaysList;
	private boolean m_ShowWeekendDays = true;
	private int m_StartWorkDayHour = 8;
	private int m_StartWorkDayMinute = 0;
	private int m_EndWorkDayHour = 16;
	private int m_EndWorkDayMinute = 0;
	private boolean m_ShowNonWorkingHours = false;
	private int m_WorkDayDurationInMillis = 8*3600*1000;
	
	public GanttChartSettings ()
	{
		ResourceBundle teeBundle = ResourceBundle.getBundle("tee");
		
		//evaluate week end days
		try
		{
			String sWeekEndDays = DEFAULT_WEEKEND_DAYS_LIST;
			if (teeBundle.containsKey("TEESuite.GanttChart.WeekendDays.List"))
			{
				sWeekEndDays = teeBundle.getString("TEESuite.GanttChart.WeekendDays.List");
			}
			ArrayList<String> parsedWeekendDaysList = parseLine(sWeekEndDays, ",");
			m_WeekendDaysList = new ArrayList<Integer>();
			for (String strWeekendDayIndex:parsedWeekendDaysList)
			{
				Integer iWeekEndDayIndex = Integer.parseInt(strWeekendDayIndex);
				m_WeekendDaysList.add(iWeekEndDayIndex);
			}
		}
		catch (Exception ex)
		{
			m_WeekendDaysList = new ArrayList<>(Arrays.asList(new Integer(6), new Integer(7)));
		}
		
		//evaluate whether to show weekend days
		try
		{
			String sShowWeekendDays = DEFAULT_SHOW_WEEKEND_DAYS;
			if (teeBundle.containsKey("TEESuite.GanttChart.ShowWeekendDays"))
			{
				sShowWeekendDays = teeBundle.getString("TEESuite.GanttChart.ShowWeekendDays");
			}
			m_ShowWeekendDays = Boolean.parseBoolean(sShowWeekendDays);
		}
		catch (Exception ex)
		{
			m_ShowWeekendDays = true;
		}
		
		//evaluate start time of working day
		try
		{
			String sWorkDayStart = DEFAULT_WORKDAY_START;
			if (teeBundle.containsKey("TEESuite.GanttChart.WorkDay.StartTime"))
			{
				sWorkDayStart = teeBundle.getString("TEESuite.GanttChart.WorkDay.StartTime");
			}
			ArrayList<String> parsedTime = parseLine(sWorkDayStart, ":");
			m_StartWorkDayHour = Integer.parseInt(parsedTime.get(0));
			m_StartWorkDayMinute = Integer.parseInt(parsedTime.get(1));
		}
		catch (Exception ex)
		{
			m_StartWorkDayHour = 8;
			m_StartWorkDayMinute = 0;
		}
		
		//evaluate end time of working day
		try
		{
			String sWorkDayEnd = DEFAULT_WORKDAY_END;
			if (teeBundle.containsKey("TEESuite.GanttChart.WorkDay.EndTime"))
			{
				sWorkDayEnd = teeBundle.getString("TEESuite.GanttChart.WorkDay.EndTime");
			}
			ArrayList<String> parsedTime = parseLine(sWorkDayEnd, ":");
			m_EndWorkDayHour = Integer.parseInt(parsedTime.get(0));
			m_EndWorkDayMinute = Integer.parseInt(parsedTime.get(1));
		}
		catch (Exception ex)
		{
			m_EndWorkDayHour = 16;
			m_EndWorkDayMinute = 0;
		}
		
		//evaluate whether to show non-working hours
		try
		{
			String sShowNonWorkingHours = DEAFULTSHOW_NON_WORKING_HOURS;
			if (teeBundle.containsKey("TEESuite.GanttChart.ShowNonWorkingHours"))
			{
				sShowNonWorkingHours = teeBundle.getString("TEESuite.GanttChart.ShowNonWorkingHours");
			}
			m_ShowNonWorkingHours = Boolean.parseBoolean(sShowNonWorkingHours);
		}
		catch (Exception ex)
		{
			m_ShowNonWorkingHours = false;
		}
		
		//evaluated working day duration
		try
		{
			GregorianCalendar startTime = new GregorianCalendar(2015, 1, 1, m_StartWorkDayHour, m_StartWorkDayMinute);
			GregorianCalendar endTime = new GregorianCalendar(2015, 1, 1, m_EndWorkDayHour, m_EndWorkDayMinute);
			
			m_WorkDayDurationInMillis = (int)(endTime.getTimeInMillis() - startTime.getTimeInMillis());
		}
		catch (Exception ex)
		{
			m_WorkDayDurationInMillis = 8*3600*1000;
		}
	}
	
	
	public ArrayList<Integer> getWeekendDaysList ()
	{
		return m_WeekendDaysList;
	}
	
	public int getStartWorkDayHour()
	{
		return m_StartWorkDayHour;
	}
	public int getStartWorkDayMinute()
	{
		return m_StartWorkDayMinute;
	}
	public int getEndWorkDayHour()
	{
		return m_EndWorkDayHour;
	}
	public int getEndWorkDayMinute()
	{
		return m_EndWorkDayMinute;
	}
	
	public int getWorkingDayDurationInMillis()
	{
		return m_WorkDayDurationInMillis;
	}
	
	@Override
	public String getJSONString()
	{
		
		JSONObject jsonGanttChartSettings = new JSONObject();
		
		jsonGanttChartSettings.put ("showWorkDays",!m_ShowWeekendDays);
		jsonGanttChartSettings.put ("showWorkHours",!m_ShowNonWorkingHours);
		
		int weekEndStart = m_WeekendDaysList.get(0);
		int weekEndEnd = m_WeekendDaysList.get(m_WeekendDaysList.size()-1);
		
		int workWeekStart = weekEndEnd+1;
		if (workWeekStart>=7)
		{
			workWeekStart-=7;
		}
		
		int workWeekEnd = weekEndStart-1;
		//commented - apparently workWeekStart should be zero, if started by sunday
		/*if (workWeekEnd == 0)
		{
			workWeekEnd = 7;
		}*/
		
		jsonGanttChartSettings.put ("workWeekStart",workWeekStart);
		jsonGanttChartSettings.put ("workWeekEnd",workWeekEnd);
		
		
		jsonGanttChartSettings.put ("workDayStart","2015/1/1 "+m_StartWorkDayHour+":"+m_StartWorkDayMinute);
		jsonGanttChartSettings.put ("workDayEnd","2015/1/1 "+m_EndWorkDayHour+":"+m_EndWorkDayMinute);
		
		
		String retVal = jsonGanttChartSettings.toJSONString();
		return retVal;
	}
	
	public static void main(String[] args)
	{
		GanttChartSettings  settings = new GanttChartSettings(); 
	}
}
