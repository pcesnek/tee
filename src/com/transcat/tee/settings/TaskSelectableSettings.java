package com.transcat.tee.settings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;


public class TaskSelectableSettings  extends AbstractSelectableSettings
{
	protected ArrayList<String> m_RouteNodeSelectableNamesList;
	protected ArrayList<String> m_RouteNodeSelectablesList;
	
	protected HashMap<String,String> m_RouteNodeSelectableNamesToSelectablesMap;
	protected HashMap<String,String> m_RouteNodeSelectablesToSelectableNamesMap;

	protected ArrayList<String> m_TaskSelectableNamesList;
	protected ArrayList<String> m_TaskSelectablesList;
	
	protected HashMap<String,String> m_TaskSelectableNamesToSelectablesMap;
	protected HashMap<String,String> m_TaskSelectablesToSelectableNamesMap;
	
	
	public TaskSelectableSettings()
	{
		m_RouteNodeSelectableNamesList = new ArrayList<String>();
		m_RouteNodeSelectablesList = new ArrayList<String>();
		m_RouteNodeSelectableNamesToSelectablesMap = new HashMap<String,String>();
		m_RouteNodeSelectablesToSelectableNamesMap = new HashMap<String,String>();
		m_TaskSelectableNamesList = new ArrayList<String>();
		m_TaskSelectablesList = new ArrayList<String>();
		m_TaskSelectableNamesToSelectablesMap = new HashMap<String,String>();
		m_TaskSelectablesToSelectableNamesMap = new HashMap<String,String>();
		
		ResourceBundle teeBundle = ResourceBundle.getBundle("tee");
		if (teeBundle.containsKey("TEESuite.DataSchema.RouteNode.Selectables"))
		{
			String routeNodeSelectablesStr = teeBundle.getString("TEESuite.DataSchema.RouteNode.Selectables").trim();
			parseSettingsInputLine (routeNodeSelectablesStr, m_RouteNodeSelectableNamesList, m_RouteNodeSelectablesList, m_RouteNodeSelectableNamesToSelectablesMap, m_RouteNodeSelectablesToSelectableNamesMap);

		}
		
		if (teeBundle.containsKey("TEESuite.DataSchema.Task.Selectables"))
		{
			String taskSelectablesStr = teeBundle.getString("TEESuite.DataSchema.Task.Selectables").trim();
			parseSettingsInputLine (taskSelectablesStr, m_TaskSelectableNamesList, m_TaskSelectablesList, m_TaskSelectableNamesToSelectablesMap, m_TaskSelectablesToSelectableNamesMap);	
		}
		

		
	}
	
	public ArrayList<String> getRouteNodeSelectableNamesList()
	{
		return m_RouteNodeSelectableNamesList;
	}
	
	public ArrayList<String> getRouteNodeSelectablesList()
	{
		return m_RouteNodeSelectablesList;
	}
	
	public String getRouteNodeSelectForSelectableName(String selectableName)
	{
		String retVal = m_RouteNodeSelectableNamesToSelectablesMap.get(selectableName); 
		return retVal;
	}
	
	public String getRouteNodeSelectableNameForSelectable(String selectable)
	{
		String retVal = m_RouteNodeSelectablesToSelectableNamesMap.get(selectable); 
		return retVal;
	}
	
	public ArrayList<String> getTaskSelectableNamesList()
	{
		return m_TaskSelectableNamesList;
	}
	
	public ArrayList<String> getTaskSelectablesList()
	{
		return m_TaskSelectablesList;
	}
	
	public HashMap<String,String> getRouteNodeSelectablesToSelectableNamesMap()
	{
		return m_RouteNodeSelectablesToSelectableNamesMap;
	}
	
	public HashMap<String,String> getRouteNodeSelectableNamesToSelectablesMap()
	{
		return m_RouteNodeSelectableNamesToSelectablesMap;
	}
	
	public HashMap<String,String> getTaskSelectablesToSelectableNamesMap()
	{
		return m_TaskSelectablesToSelectableNamesMap;
	}
	
	public HashMap<String,String> getTaskSelectableNamesToSelectablesMap()
	{
		return m_TaskSelectableNamesToSelectablesMap;
	}
	
	public String getTaskSelectForSelectableName(String selectableName)
	{
		String retVal = m_TaskSelectableNamesToSelectablesMap.get(selectableName); 
		return retVal;
	}
	
	public String getTaskSelectableNameForSelectable(String selectable)
	{
		String retVal = m_TaskSelectablesToSelectableNamesMap.get(selectable); 
		return retVal;
	}
}

