package com.transcat.tee.settings;

public class TeeSettings 
{
	private static ProjectSelectableSettings m_ProjectSelectableSettings = null;
	private static TaskSelectableSettings m_TaskSelectableSettings = null;
	private static KanbanSettings m_KanBanSettings = null;
	private static GanttTableSettings m_GanttTableSettings = null;
	private static GanttChartSettings m_GanttChartSettings = null;
	private static KanbanFormSettings m_KanbanFormSettings = null;
	private static ProjectContentTableSettings m_ProjectContentTableSettings = null;
	private static ProjectContentFormSettings m_ProjectContentFormSettings = null;
	private static ProjectContentSettings m_ProjectContentSettings = null;
	
	
	static
	{
		m_ProjectSelectableSettings = new ProjectSelectableSettings();
		m_TaskSelectableSettings = new TaskSelectableSettings();	
		m_KanBanSettings = new KanbanSettings();
		m_GanttTableSettings = new GanttTableSettings();
		m_GanttChartSettings = new GanttChartSettings();
		m_KanbanFormSettings = new KanbanFormSettings();
		m_ProjectContentTableSettings = new ProjectContentTableSettings();
		m_ProjectContentFormSettings = new ProjectContentFormSettings();
		m_ProjectContentSettings = new ProjectContentSettings();
	}
	public static ProjectSelectableSettings getProjectSelectableSettings()
	{
		return(m_ProjectSelectableSettings);
	}
	public static TaskSelectableSettings getTaskSelectableSettings()
	{
		return(m_TaskSelectableSettings);
	}	
	
	public static KanbanSettings getKanbanSettings()
	{
		return (m_KanBanSettings);
	}
	
	public static <T> BaseSettings getSettings(Class<T> clazz) {
		if (GanttTableSettings.class == clazz)
			return m_GanttTableSettings;
		else if (GanttChartSettings.class == clazz)
			return m_GanttChartSettings;
		else if (KanbanFormSettings.class == clazz)
			return m_KanbanFormSettings;
		else if (ProjectContentTableSettings.class == clazz)
			return m_ProjectContentTableSettings;
		else if (ProjectContentFormSettings.class == clazz)
			return m_ProjectContentFormSettings;
		else
			return null;
	}
	
	public static ProjectContentSettings getProjectContentSettings()
	{
		return (m_ProjectContentSettings);
	}
	
}
